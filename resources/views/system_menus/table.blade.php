<table class="table table-responsive" id="systemMenus-table">
    <thead>
        <tr>
            <th>Label</th>
        <th>Component</th>
        <th>Classbutton</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($systemMenus as $systemMenu)
        <tr>
            <td>{!! $systemMenu->label !!}</td>
            <td>{!! $systemMenu->component !!}</td>
            <td>{!! $systemMenu->classButton !!}</td>
            <td>
                {!! Form::open(['route' => ['systemMenus.destroy', $systemMenu->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('systemMenus.show', [$systemMenu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('systemMenus.edit', [$systemMenu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>