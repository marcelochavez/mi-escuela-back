<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $systemMenu->id !!}</p>
</div>

<!-- Label Field -->
<div class="form-group">
    {!! Form::label('label', 'Label:') !!}
    <p>{!! $systemMenu->label !!}</p>
</div>

<!-- Component Field -->
<div class="form-group">
    {!! Form::label('component', 'Component:') !!}
    <p>{!! $systemMenu->component !!}</p>
</div>

<!-- Classbutton Field -->
<div class="form-group">
    {!! Form::label('classButton', 'Classbutton:') !!}
    <p>{!! $systemMenu->classButton !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $systemMenu->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $systemMenu->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $systemMenu->deleted_at !!}</p>
</div>

