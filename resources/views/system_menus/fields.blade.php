<!-- Label Field -->
<div class="form-group col-sm-6">
    {!! Form::label('label', 'Label:') !!}
    {!! Form::text('label', null, ['class' => 'form-control']) !!}
</div>

<!-- Component Field -->
<div class="form-group col-sm-6">
    {!! Form::label('component', 'Component:') !!}
    {!! Form::text('component', null, ['class' => 'form-control']) !!}
</div>

<!-- Classbutton Field -->
<div class="form-group col-sm-6">
    {!! Form::label('classButton', 'Classbutton:') !!}
    {!! Form::text('classButton', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('systemMenus.index') !!}" class="btn btn-default">Cancel</a>
</div>
