<table class="table table-responsive" id="courseInstanceHasEvents-table">
    <thead>
        <tr>
            <th>Course Instance Id</th>
        <th>Event Id</th>
        <th>Description</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($courseInstanceHasEvents as $courseInstanceHasEvent)
        <tr>
            <td>{!! $courseInstanceHasEvent->course_instance_id !!}</td>
            <td>{!! $courseInstanceHasEvent->event_id !!}</td>
            <td>{!! $courseInstanceHasEvent->description !!}</td>
            <td>
                {!! Form::open(['route' => ['courseInstanceHasEvents.destroy', $courseInstanceHasEvent->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('courseInstanceHasEvents.show', [$courseInstanceHasEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('courseInstanceHasEvents.edit', [$courseInstanceHasEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>