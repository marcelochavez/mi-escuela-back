<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $courseInstanceHasEvent->id !!}</p>
</div>

<!-- Course Instance Id Field -->
<div class="form-group">
    {!! Form::label('course_instance_id', 'Course Instance Id:') !!}
    <p>{!! $courseInstanceHasEvent->course_instance_id !!}</p>
</div>

<!-- Event Id Field -->
<div class="form-group">
    {!! Form::label('event_id', 'Event Id:') !!}
    <p>{!! $courseInstanceHasEvent->event_id !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $courseInstanceHasEvent->description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $courseInstanceHasEvent->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $courseInstanceHasEvent->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $courseInstanceHasEvent->deleted_at !!}</p>
</div>

