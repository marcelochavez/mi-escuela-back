@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Course Instance Has Event
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($courseInstanceHasEvent, ['route' => ['courseInstanceHasEvents.update', $courseInstanceHasEvent->id], 'method' => 'patch']) !!}

                        @include('course_instance_has_events.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection