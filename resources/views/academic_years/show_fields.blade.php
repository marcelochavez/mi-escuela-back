<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $academicYear->id !!}</p>
</div>

<!-- Year Field -->
<div class="form-group">
    {!! Form::label('year', 'Year:') !!}
    <p>{!! $academicYear->year !!}</p>
</div>

<!-- Start First Semester Field -->
<div class="form-group">
    {!! Form::label('start_first_semester', 'Start First Semester:') !!}
    <p>{!! $academicYear->start_first_semester !!}</p>
</div>

<!-- Start Second Semester Field -->
<div class="form-group">
    {!! Form::label('start_second_semester', 'Start Second Semester:') !!}
    <p>{!! $academicYear->start_second_semester !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $academicYear->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $academicYear->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $academicYear->deleted_at !!}</p>
</div>

