@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Academic Year
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($academicYear, ['route' => ['academicYears.update', $academicYear->id], 'method' => 'patch']) !!}

                        @include('academic_years.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection