<table class="table table-responsive" id="academicYears-table">
    <thead>
        <tr>
            <th>Year</th>
        <th>Start First Semester</th>
        <th>Start Second Semester</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($academicYears as $academicYear)
        <tr>
            <td>{!! $academicYear->year !!}</td>
            <td>{!! $academicYear->start_first_semester !!}</td>
            <td>{!! $academicYear->start_second_semester !!}</td>
            <td>
                {!! Form::open(['route' => ['academicYears.destroy', $academicYear->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('academicYears.show', [$academicYear->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('academicYears.edit', [$academicYear->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>