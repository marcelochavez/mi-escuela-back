<!-- Year Field -->
<div class="form-group col-sm-6">
    {!! Form::label('year', 'Year:') !!}
    {!! Form::text('year', null, ['class' => 'form-control']) !!}
</div>

<!-- Start First Semester Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_first_semester', 'Start First Semester:') !!}
    {!! Form::date('start_first_semester', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Second Semester Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_second_semester', 'Start Second Semester:') !!}
    {!! Form::date('start_second_semester', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('academicYears.index') !!}" class="btn btn-default">Cancel</a>
</div>
