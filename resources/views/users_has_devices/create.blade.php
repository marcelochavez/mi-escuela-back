@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Users Has Device
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'usersHasDevices.store']) !!}

                        @include('users_has_devices.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
