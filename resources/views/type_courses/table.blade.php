<table class="table table-responsive" id="typeCourses-table">
    <thead>
        <tr>
            <th>Name</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($typeCourses as $typeCourse)
        <tr>
            <td>{!! $typeCourse->name !!}</td>
            <td>
                {!! Form::open(['route' => ['typeCourses.destroy', $typeCourse->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('typeCourses.show', [$typeCourse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('typeCourses.edit', [$typeCourse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>