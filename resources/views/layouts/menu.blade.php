
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>

<li class="{{ Request::is('systemMenus*') ? 'active' : '' }}">
    <a href="{!! route('systemMenus.index') !!}"><i class="fa fa-edit"></i><span>System Menus</span></a>
</li>

<li class="{{ Request::is('studentHasCourseInstances*') ? 'active' : '' }}">
    <a href="{!! route('studentHasCourseInstances.index') !!}"><i class="fa fa-edit"></i><span>Student Has Course Instances</span></a>
</li>

<li class="{{ Request::is('courses*') ? 'active' : '' }}">
    <a href="{!! route('courses.index') !!}"><i class="fa fa-edit"></i><span>Courses</span></a>
</li>

<li class="{{ Request::is('roleHasSystemMenus*') ? 'active' : '' }}">
    <a href="{!! route('roleHasSystemMenus.index') !!}"><i class="fa fa-edit"></i><span>Role Has System Menus</span></a>
</li>

<li class="{{ Request::is('usersHasEvents*') ? 'active' : '' }}">
    <a href="{!! route('usersHasEvents.index') !!}"><i class="fa fa-edit"></i><span>Users Has Events</span></a>
</li>

<li class="{{ Request::is('courseInstances*') ? 'active' : '' }}">
    <a href="{!! route('courseInstances.index') !!}"><i class="fa fa-edit"></i><span>Course Instances</span></a>
</li>

<li class="{{ Request::is('semesters*') ? 'active' : '' }}">
    <a href="{!! route('semesters.index') !!}"><i class="fa fa-edit"></i><span>Semesters</span></a>
</li>

<li class="{{ Request::is('typeCourses*') ? 'active' : '' }}">
    <a href="{!! route('typeCourses.index') !!}"><i class="fa fa-edit"></i><span>Type Courses</span></a>
</li>

<li class="{{ Request::is('years*') ? 'active' : '' }}">
    <a href="{!! route('years.index') !!}"><i class="fa fa-edit"></i><span>Years</span></a>
</li>

<li class="{{ Request::is('priorities*') ? 'active' : '' }}">
    <a href="{!! route('priorities.index') !!}"><i class="fa fa-edit"></i><span>Priorities</span></a>
</li>

<li class="{{ Request::is('eventsTypes*') ? 'active' : '' }}">
    <a href="{!! route('eventsTypes.index') !!}"><i class="fa fa-edit"></i><span>Events Types</span></a>
</li>

<li class="{{ Request::is('events*') ? 'active' : '' }}">
    <a href="{!! route('events.index') !!}"><i class="fa fa-edit"></i><span>Events</span></a>
</li>

<li class="{{ Request::is('globalEvents*') ? 'active' : '' }}">
    <a href="{!! route('globalEvents.index') !!}"><i class="fa fa-edit"></i><span>Global Events</span></a>
</li>

<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>

<li class="{{ Request::is('academicYears*') ? 'active' : '' }}">
    <a href="{!! route('academicYears.index') !!}"><i class="fa fa-edit"></i><span>Academic Years</span></a>
</li>

<li class="{{ Request::is('courseInstanceHasEvents*') ? 'active' : '' }}">
    <a href="{!! route('courseInstanceHasEvents.index') !!}"><i class="fa fa-edit"></i><span>Course Instance Has Events</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('usersHasDevices*') ? 'active' : '' }}">
    <a href="{!! route('usersHasDevices.index') !!}"><i class="fa fa-edit"></i><span>Users Has Devices</span></a>
</li>

