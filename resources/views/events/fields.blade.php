<!-- Priority Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority_id', 'Priority Id:') !!}
    {!! Form::number('priority_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_type_id', 'Event Type Id:') !!}
    {!! Form::number('event_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Event Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_date', 'Event Date:') !!}
    {!! Form::date('event_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('events.index') !!}" class="btn btn-default">Cancel</a>
</div>
