<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $event->id !!}</p>
</div>

<!-- Priority Id Field -->
<div class="form-group">
    {!! Form::label('priority_id', 'Priority Id:') !!}
    <p>{!! $event->priority_id !!}</p>
</div>

<!-- Event Type Id Field -->
<div class="form-group">
    {!! Form::label('event_type_id', 'Event Type Id:') !!}
    <p>{!! $event->event_type_id !!}</p>
</div>

<!-- Event Date Field -->
<div class="form-group">
    {!! Form::label('event_date', 'Event Date:') !!}
    <p>{!! $event->event_date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $event->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $event->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $event->deleted_at !!}</p>
</div>

