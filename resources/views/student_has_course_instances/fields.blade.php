<!-- Student Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('student_id', 'Student Id:') !!}
    {!! Form::number('student_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Instance Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_instance_id', 'Course Instance Id:') !!}
    {!! Form::number('course_instance_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('studentHasCourseInstances.index') !!}" class="btn btn-default">Cancel</a>
</div>
