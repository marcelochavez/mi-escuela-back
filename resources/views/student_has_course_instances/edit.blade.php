@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Student Has Course Instance
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($studentHasCourseInstance, ['route' => ['studentHasCourseInstances.update', $studentHasCourseInstance->id], 'method' => 'patch']) !!}

                        @include('student_has_course_instances.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection