<table class="table table-responsive" id="studentHasCourseInstances-table">
    <thead>
        <tr>
            <th>Student Id</th>
        <th>Course Instance Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($studentHasCourseInstances as $studentHasCourseInstance)
        <tr>
            <td>{!! $studentHasCourseInstance->student_id !!}</td>
            <td>{!! $studentHasCourseInstance->course_instance_id !!}</td>
            <td>
                {!! Form::open(['route' => ['studentHasCourseInstances.destroy', $studentHasCourseInstance->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('studentHasCourseInstances.show', [$studentHasCourseInstance->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('studentHasCourseInstances.edit', [$studentHasCourseInstance->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>