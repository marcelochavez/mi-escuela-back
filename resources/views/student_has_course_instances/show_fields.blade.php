<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $studentHasCourseInstance->id !!}</p>
</div>

<!-- Student Id Field -->
<div class="form-group">
    {!! Form::label('student_id', 'Student Id:') !!}
    <p>{!! $studentHasCourseInstance->student_id !!}</p>
</div>

<!-- Course Instance Id Field -->
<div class="form-group">
    {!! Form::label('course_instance_id', 'Course Instance Id:') !!}
    <p>{!! $studentHasCourseInstance->course_instance_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $studentHasCourseInstance->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $studentHasCourseInstance->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $studentHasCourseInstance->deleted_at !!}</p>
</div>

