@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Events Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($eventsType, ['route' => ['eventsTypes.update', $eventsType->id], 'method' => 'patch']) !!}

                        @include('events_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection