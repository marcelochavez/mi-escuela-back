<table class="table table-responsive" id="usersHasEvents-table">
    <thead>
        <tr>
            <th>Student Id</th>
        <th>Event Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($usersHasEvents as $usersHasEvent)
        <tr>
            <td>{!! $usersHasEvent->student_id !!}</td>
            <td>{!! $usersHasEvent->event_id !!}</td>
            <td>
                {!! Form::open(['route' => ['usersHasEvents.destroy', $usersHasEvent->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('usersHasEvents.show', [$usersHasEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('usersHasEvents.edit', [$usersHasEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>