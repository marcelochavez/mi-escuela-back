@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Users Has Event
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($usersHasEvent, ['route' => ['usersHasEvents.update', $usersHasEvent->id], 'method' => 'patch']) !!}

                        @include('users_has_events.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection