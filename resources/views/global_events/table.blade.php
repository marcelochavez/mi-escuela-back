<table class="table table-responsive" id="globalEvents-table">
    <thead>
        <tr>
            <th>Event Id</th>
        <th>Description</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($globalEvents as $globalEvent)
        <tr>
            <td>{!! $globalEvent->event_id !!}</td>
            <td>{!! $globalEvent->description !!}</td>
            <td>
                {!! Form::open(['route' => ['globalEvents.destroy', $globalEvent->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('globalEvents.show', [$globalEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('globalEvents.edit', [$globalEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>