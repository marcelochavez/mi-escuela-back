<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $roleHasSystemMenu->id !!}</p>
</div>

<!-- Role Id Field -->
<div class="form-group">
    {!! Form::label('role_id', 'Role Id:') !!}
    <p>{!! $roleHasSystemMenu->role_id !!}</p>
</div>

<!-- System Menu Id Field -->
<div class="form-group">
    {!! Form::label('system_menu_id', 'System Menu Id:') !!}
    <p>{!! $roleHasSystemMenu->system_menu_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $roleHasSystemMenu->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $roleHasSystemMenu->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $roleHasSystemMenu->deleted_at !!}</p>
</div>

