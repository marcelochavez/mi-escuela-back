<table class="table table-responsive" id="roleHasSystemMenus-table">
    <thead>
        <tr>
            <th>Role Id</th>
        <th>System Menu Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($roleHasSystemMenus as $roleHasSystemMenu)
        <tr>
            <td>{!! $roleHasSystemMenu->role_id !!}</td>
            <td>{!! $roleHasSystemMenu->system_menu_id !!}</td>
            <td>
                {!! Form::open(['route' => ['roleHasSystemMenus.destroy', $roleHasSystemMenu->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('roleHasSystemMenus.show', [$roleHasSystemMenu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('roleHasSystemMenus.edit', [$roleHasSystemMenu->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>