@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Course Instance
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($courseInstance, ['route' => ['courseInstances.update', $courseInstance->id], 'method' => 'patch']) !!}

                        @include('course_instances.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection