<table class="table table-responsive" id="courseInstances-table">
    <thead>
        <tr>
            <th>Course Id</th>
        <th>Semester Id</th>
        <th>Year Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($courseInstances as $courseInstance)
        <tr>
            <td>{!! $courseInstance->course_id !!}</td>
            <td>{!! $courseInstance->semester_id !!}</td>
            <td>{!! $courseInstance->year_id !!}</td>
            <td>
                {!! Form::open(['route' => ['courseInstances.destroy', $courseInstance->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('courseInstances.show', [$courseInstance->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('courseInstances.edit', [$courseInstance->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>