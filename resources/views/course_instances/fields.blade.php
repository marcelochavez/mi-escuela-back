<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Semester Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('semester_id', 'Semester Id:') !!}
    {!! Form::number('semester_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('year_id', 'Year Id:') !!}
    {!! Form::number('year_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('courseInstances.index') !!}" class="btn btn-default">Cancel</a>
</div>
