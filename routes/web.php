<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');


Route::resource('roles', 'RoleController');

Route::resource('systemMenus', 'SystemMenuController');

Route::resource('studentHasCourseInstances', 'StudentHasCourseInstanceController');

Route::resource('courses', 'CourseController');

Route::resource('roleHasSystemMenus', 'RoleHasSystemMenuController');

Route::resource('usersHasEvents', 'UsersHasEventController');

Route::resource('courseInstances', 'CourseInstanceController');

Route::resource('semesters', 'SemesterController');

Route::resource('typeCourses', 'TypeCourseController');

Route::resource('years', 'YearController');

Route::resource('priorities', 'PriorityController');

Route::resource('eventsTypes', 'EventsTypeController');

Route::resource('events', 'EventController');

Route::resource('globalEvents', 'GlobalEventController');

Route::resource('categories', 'CategoryController');

Route::resource('academicYears', 'AcademicYearController');

Route::resource('courseInstanceHasEvents', 'CourseInstanceHasEventController');

Route::resource('users', 'UserController');

Route::resource('usersHasDevices', 'UsersHasDeviceController');