<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('auth/studentLogin', 'UserAPIController@studentLogin');
Route::post('auth/generalLogin', 'UserAPIController@generalLogin');

Route::post('studentRegister','UserAPIController@studentRegister');


Route::group(['middleware' => 'jwt.auth'], function () {

    Route::resource('courses', 'CourseAPIController');

    Route::resource('users', 'UserAPIController');

    Route::resource('studentHasCourseInstances', 'StudentHasCourseInstanceAPIController');

    Route::resource('roles', 'RoleAPIController');

    Route::resource('courseInstanceHasEvents', 'CourseInstanceHasEventAPIController');

    Route::resource('courseInstances', 'CourseInstanceAPIController');

    Route::resource('priorities', 'PriorityAPIController');

    Route::resource('semesters', 'SemesterAPIController');

    Route::resource('years', 'YearAPIController');

    Route::resource('globalEvents', 'GlobalEventAPIController');

    Route::resource('events', 'EventAPIController');

    Route::resource('eventsTypes', 'EventsTypeAPIController');

    Route::resource('categories', 'CategoryAPIController');

    Route::post('/generalRegister', 'UserAPIController@generalRegister');

    Route::get('dataUser', 'UserAPIController@dataUser');

    Route::get('findSystemMenu', 'RoleHasSystemMenuAPIController@findSystemMenu');

    Route::post('usersByRole','UserAPIController@usersByRole');

    Route::get('studentsList', 'UserAPIController@studentsList');

    Route::get('studentById/{id}','UserAPIController@studentById');

    Route::get('studentById2/{id}','UserAPIController@studentById2');

    Route::resource('usersHasEvents', 'UsersHasEventAPIController');

    Route::resource('role_has_system_menus', 'RoleHasSystemMenuAPIController');

    Route::resource('system_menus', 'SystemMenuAPIController');

    Route::resource('typeCourses', 'TypeCourseAPIController');

    Route::resource('academicYears', 'AcademicYearAPIController');

    Route::put('studentUpdate/{id}', 'UserAPIController@studentUpdate');

    Route::get('allCourse', 'CourseAPIController@allCourse');

    Route::get('currentInstancesCourses', 'CourseInstanceAPIController@currentInstancesCourses');

    Route::post('findCourseInstanceByInstance', 'CourseInstanceAPIController@findCourseInstanceByInstance');

    route::post('findCourseInstanceByInstancex', 'CourseInstanceAPIController@findCourseInstanceByInstancex');

    Route::get('/getYearInstance', 'YearAPIController@getYearInstance');

    Route::get('getIntanceCourseByStudent', 'StudentHasCourseInstanceAPIController@getIntanceCourseByStudent');

    Route::put('/changePassword','UserAPIController@changePassword');

    Route::put('/reset', 'UserAPIController@reset');

    //Eventos del estudiante Mobile
    Route::get('getStudentEvents', 'EventAPIController@obtenerEventosDeEstudiante');

    Route::put('studentEdit', 'UserAPIController@studentEdit');

    Route::delete('studentDestroy/{id}', 'UserAPIController@studentDestroy');


    Route::resource('usersHasDevices', 'UsersHasDeviceAPIController');

});
Route::put('reset2', 'UserAPIController@reset');

