# MiEscuelaApp Backend

## Requerimientos

* Mismos requerimientos de Laravel 5.4
* PHP 7.1+
* MySQL 5.6+

## Pasos de instalación
* Ejecutar los siguientes comandos
    ```
    git clone <link repo git ssh>
    cd <carpeta_proyecto>
    composer install
    ```
* Configurar `.env` file
    * Crear DB vacia en MySQL 
    * Copiar `.env.example` a `.env`
    * Configurar los datos de acceso de la base de datos en `.env`
* Ejecutar los siguientes comandos
    ```
    php artisan key:generate
    chown -R www-data:www-data .
    chmod -R 777 .
    ```
* Migrar y Cargar datos
    ```
    php artisan migrate
    
    ```
* Ejecutar
    ```
    php artisan serve
    ```
    El server correrá en `http://localhost:8000`
    
