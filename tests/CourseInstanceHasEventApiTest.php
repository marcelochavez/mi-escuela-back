<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseInstanceHasEventApiTest extends TestCase
{
    use MakeCourseInstanceHasEventTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->fakeCourseInstanceHasEventData();
        $this->json('POST', '/api/v1/courseInstanceHasEvents', $courseInstanceHasEvent);

        $this->assertApiResponse($courseInstanceHasEvent);
    }

    /**
     * @test
     */
    public function testReadCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->makeCourseInstanceHasEvent();
        $this->json('GET', '/api/v1/courseInstanceHasEvents/'.$courseInstanceHasEvent->id);

        $this->assertApiResponse($courseInstanceHasEvent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->makeCourseInstanceHasEvent();
        $editedCourseInstanceHasEvent = $this->fakeCourseInstanceHasEventData();

        $this->json('PUT', '/api/v1/courseInstanceHasEvents/'.$courseInstanceHasEvent->id, $editedCourseInstanceHasEvent);

        $this->assertApiResponse($editedCourseInstanceHasEvent);
    }

    /**
     * @test
     */
    public function testDeleteCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->makeCourseInstanceHasEvent();
        $this->json('DELETE', '/api/v1/courseInstanceHasEvents/'.$courseInstanceHasEvent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/courseInstanceHasEvents/'.$courseInstanceHasEvent->id);

        $this->assertResponseStatus(404);
    }
}
