<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventsTypeApiTest extends TestCase
{
    use MakeEventsTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEventsType()
    {
        $eventsType = $this->fakeEventsTypeData();
        $this->json('POST', '/api/v1/eventsTypes', $eventsType);

        $this->assertApiResponse($eventsType);
    }

    /**
     * @test
     */
    public function testReadEventsType()
    {
        $eventsType = $this->makeEventsType();
        $this->json('GET', '/api/v1/eventsTypes/'.$eventsType->id);

        $this->assertApiResponse($eventsType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEventsType()
    {
        $eventsType = $this->makeEventsType();
        $editedEventsType = $this->fakeEventsTypeData();

        $this->json('PUT', '/api/v1/eventsTypes/'.$eventsType->id, $editedEventsType);

        $this->assertApiResponse($editedEventsType);
    }

    /**
     * @test
     */
    public function testDeleteEventsType()
    {
        $eventsType = $this->makeEventsType();
        $this->json('DELETE', '/api/v1/eventsTypes/'.$eventsType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/eventsTypes/'.$eventsType->id);

        $this->assertResponseStatus(404);
    }
}
