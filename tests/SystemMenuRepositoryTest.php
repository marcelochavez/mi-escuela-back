<?php

use App\Models\SystemMenu;
use App\Repositories\SystemMenuRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SystemMenuRepositoryTest extends TestCase
{
    use MakeSystemMenuTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SystemMenuRepository
     */
    protected $systemMenuRepo;

    public function setUp()
    {
        parent::setUp();
        $this->systemMenuRepo = App::make(SystemMenuRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSystemMenu()
    {
        $systemMenu = $this->fakeSystemMenuData();
        $createdSystemMenu = $this->systemMenuRepo->create($systemMenu);
        $createdSystemMenu = $createdSystemMenu->toArray();
        $this->assertArrayHasKey('id', $createdSystemMenu);
        $this->assertNotNull($createdSystemMenu['id'], 'Created SystemMenu must have id specified');
        $this->assertNotNull(SystemMenu::find($createdSystemMenu['id']), 'SystemMenu with given id must be in DB');
        $this->assertModelData($systemMenu, $createdSystemMenu);
    }

    /**
     * @test read
     */
    public function testReadSystemMenu()
    {
        $systemMenu = $this->makeSystemMenu();
        $dbSystemMenu = $this->systemMenuRepo->find($systemMenu->id);
        $dbSystemMenu = $dbSystemMenu->toArray();
        $this->assertModelData($systemMenu->toArray(), $dbSystemMenu);
    }

    /**
     * @test update
     */
    public function testUpdateSystemMenu()
    {
        $systemMenu = $this->makeSystemMenu();
        $fakeSystemMenu = $this->fakeSystemMenuData();
        $updatedSystemMenu = $this->systemMenuRepo->update($fakeSystemMenu, $systemMenu->id);
        $this->assertModelData($fakeSystemMenu, $updatedSystemMenu->toArray());
        $dbSystemMenu = $this->systemMenuRepo->find($systemMenu->id);
        $this->assertModelData($fakeSystemMenu, $dbSystemMenu->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSystemMenu()
    {
        $systemMenu = $this->makeSystemMenu();
        $resp = $this->systemMenuRepo->delete($systemMenu->id);
        $this->assertTrue($resp);
        $this->assertNull(SystemMenu::find($systemMenu->id), 'SystemMenu should not exist in DB');
    }
}
