<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentHasCourseInstanceApiTest extends TestCase
{
    use MakeStudentHasCourseInstanceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->fakeStudentHasCourseInstanceData();
        $this->json('POST', '/api/v1/studentHasCourseInstances', $studentHasCourseInstance);

        $this->assertApiResponse($studentHasCourseInstance);
    }

    /**
     * @test
     */
    public function testReadStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->makeStudentHasCourseInstance();
        $this->json('GET', '/api/v1/studentHasCourseInstances/'.$studentHasCourseInstance->id);

        $this->assertApiResponse($studentHasCourseInstance->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->makeStudentHasCourseInstance();
        $editedStudentHasCourseInstance = $this->fakeStudentHasCourseInstanceData();

        $this->json('PUT', '/api/v1/studentHasCourseInstances/'.$studentHasCourseInstance->id, $editedStudentHasCourseInstance);

        $this->assertApiResponse($editedStudentHasCourseInstance);
    }

    /**
     * @test
     */
    public function testDeleteStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->makeStudentHasCourseInstance();
        $this->json('DELETE', '/api/v1/studentHasCourseInstances/'.$studentHasCourseInstance->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/studentHasCourseInstances/'.$studentHasCourseInstance->id);

        $this->assertResponseStatus(404);
    }
}
