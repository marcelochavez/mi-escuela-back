<?php

use App\Models\CourseInstanceHasEvent;
use App\Repositories\CourseInstanceHasEventRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseInstanceHasEventRepositoryTest extends TestCase
{
    use MakeCourseInstanceHasEventTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CourseInstanceHasEventRepository
     */
    protected $courseInstanceHasEventRepo;

    public function setUp()
    {
        parent::setUp();
        $this->courseInstanceHasEventRepo = App::make(CourseInstanceHasEventRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->fakeCourseInstanceHasEventData();
        $createdCourseInstanceHasEvent = $this->courseInstanceHasEventRepo->create($courseInstanceHasEvent);
        $createdCourseInstanceHasEvent = $createdCourseInstanceHasEvent->toArray();
        $this->assertArrayHasKey('id', $createdCourseInstanceHasEvent);
        $this->assertNotNull($createdCourseInstanceHasEvent['id'], 'Created CourseInstanceHasEvent must have id specified');
        $this->assertNotNull(CourseInstanceHasEvent::find($createdCourseInstanceHasEvent['id']), 'CourseInstanceHasEvent with given id must be in DB');
        $this->assertModelData($courseInstanceHasEvent, $createdCourseInstanceHasEvent);
    }

    /**
     * @test read
     */
    public function testReadCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->makeCourseInstanceHasEvent();
        $dbCourseInstanceHasEvent = $this->courseInstanceHasEventRepo->find($courseInstanceHasEvent->id);
        $dbCourseInstanceHasEvent = $dbCourseInstanceHasEvent->toArray();
        $this->assertModelData($courseInstanceHasEvent->toArray(), $dbCourseInstanceHasEvent);
    }

    /**
     * @test update
     */
    public function testUpdateCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->makeCourseInstanceHasEvent();
        $fakeCourseInstanceHasEvent = $this->fakeCourseInstanceHasEventData();
        $updatedCourseInstanceHasEvent = $this->courseInstanceHasEventRepo->update($fakeCourseInstanceHasEvent, $courseInstanceHasEvent->id);
        $this->assertModelData($fakeCourseInstanceHasEvent, $updatedCourseInstanceHasEvent->toArray());
        $dbCourseInstanceHasEvent = $this->courseInstanceHasEventRepo->find($courseInstanceHasEvent->id);
        $this->assertModelData($fakeCourseInstanceHasEvent, $dbCourseInstanceHasEvent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCourseInstanceHasEvent()
    {
        $courseInstanceHasEvent = $this->makeCourseInstanceHasEvent();
        $resp = $this->courseInstanceHasEventRepo->delete($courseInstanceHasEvent->id);
        $this->assertTrue($resp);
        $this->assertNull(CourseInstanceHasEvent::find($courseInstanceHasEvent->id), 'CourseInstanceHasEvent should not exist in DB');
    }
}
