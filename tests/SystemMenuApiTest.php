<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SystemMenuApiTest extends TestCase
{
    use MakeSystemMenuTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSystemMenu()
    {
        $systemMenu = $this->fakeSystemMenuData();
        $this->json('POST', '/api/v1/systemMenus', $systemMenu);

        $this->assertApiResponse($systemMenu);
    }

    /**
     * @test
     */
    public function testReadSystemMenu()
    {
        $systemMenu = $this->makeSystemMenu();
        $this->json('GET', '/api/v1/systemMenus/'.$systemMenu->id);

        $this->assertApiResponse($systemMenu->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSystemMenu()
    {
        $systemMenu = $this->makeSystemMenu();
        $editedSystemMenu = $this->fakeSystemMenuData();

        $this->json('PUT', '/api/v1/systemMenus/'.$systemMenu->id, $editedSystemMenu);

        $this->assertApiResponse($editedSystemMenu);
    }

    /**
     * @test
     */
    public function testDeleteSystemMenu()
    {
        $systemMenu = $this->makeSystemMenu();
        $this->json('DELETE', '/api/v1/systemMenus/'.$systemMenu->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/systemMenus/'.$systemMenu->id);

        $this->assertResponseStatus(404);
    }
}
