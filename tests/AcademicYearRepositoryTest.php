<?php

use App\Models\AcademicYear;
use App\Repositories\AcademicYearRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AcademicYearRepositoryTest extends TestCase
{
    use MakeAcademicYearTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AcademicYearRepository
     */
    protected $academicYearRepo;

    public function setUp()
    {
        parent::setUp();
        $this->academicYearRepo = App::make(AcademicYearRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAcademicYear()
    {
        $academicYear = $this->fakeAcademicYearData();
        $createdAcademicYear = $this->academicYearRepo->create($academicYear);
        $createdAcademicYear = $createdAcademicYear->toArray();
        $this->assertArrayHasKey('id', $createdAcademicYear);
        $this->assertNotNull($createdAcademicYear['id'], 'Created AcademicYear must have id specified');
        $this->assertNotNull(AcademicYear::find($createdAcademicYear['id']), 'AcademicYear with given id must be in DB');
        $this->assertModelData($academicYear, $createdAcademicYear);
    }

    /**
     * @test read
     */
    public function testReadAcademicYear()
    {
        $academicYear = $this->makeAcademicYear();
        $dbAcademicYear = $this->academicYearRepo->find($academicYear->id);
        $dbAcademicYear = $dbAcademicYear->toArray();
        $this->assertModelData($academicYear->toArray(), $dbAcademicYear);
    }

    /**
     * @test update
     */
    public function testUpdateAcademicYear()
    {
        $academicYear = $this->makeAcademicYear();
        $fakeAcademicYear = $this->fakeAcademicYearData();
        $updatedAcademicYear = $this->academicYearRepo->update($fakeAcademicYear, $academicYear->id);
        $this->assertModelData($fakeAcademicYear, $updatedAcademicYear->toArray());
        $dbAcademicYear = $this->academicYearRepo->find($academicYear->id);
        $this->assertModelData($fakeAcademicYear, $dbAcademicYear->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAcademicYear()
    {
        $academicYear = $this->makeAcademicYear();
        $resp = $this->academicYearRepo->delete($academicYear->id);
        $this->assertTrue($resp);
        $this->assertNull(AcademicYear::find($academicYear->id), 'AcademicYear should not exist in DB');
    }
}
