<?php

use App\Models\UsersHasEvent;
use App\Repositories\UsersHasEventRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersHasEventRepositoryTest extends TestCase
{
    use MakeUsersHasEventTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UsersHasEventRepository
     */
    protected $usersHasEventRepo;

    public function setUp()
    {
        parent::setUp();
        $this->usersHasEventRepo = App::make(UsersHasEventRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUsersHasEvent()
    {
        $usersHasEvent = $this->fakeUsersHasEventData();
        $createdUsersHasEvent = $this->usersHasEventRepo->create($usersHasEvent);
        $createdUsersHasEvent = $createdUsersHasEvent->toArray();
        $this->assertArrayHasKey('id', $createdUsersHasEvent);
        $this->assertNotNull($createdUsersHasEvent['id'], 'Created UsersHasEvent must have id specified');
        $this->assertNotNull(UsersHasEvent::find($createdUsersHasEvent['id']), 'UsersHasEvent with given id must be in DB');
        $this->assertModelData($usersHasEvent, $createdUsersHasEvent);
    }

    /**
     * @test read
     */
    public function testReadUsersHasEvent()
    {
        $usersHasEvent = $this->makeUsersHasEvent();
        $dbUsersHasEvent = $this->usersHasEventRepo->find($usersHasEvent->id);
        $dbUsersHasEvent = $dbUsersHasEvent->toArray();
        $this->assertModelData($usersHasEvent->toArray(), $dbUsersHasEvent);
    }

    /**
     * @test update
     */
    public function testUpdateUsersHasEvent()
    {
        $usersHasEvent = $this->makeUsersHasEvent();
        $fakeUsersHasEvent = $this->fakeUsersHasEventData();
        $updatedUsersHasEvent = $this->usersHasEventRepo->update($fakeUsersHasEvent, $usersHasEvent->id);
        $this->assertModelData($fakeUsersHasEvent, $updatedUsersHasEvent->toArray());
        $dbUsersHasEvent = $this->usersHasEventRepo->find($usersHasEvent->id);
        $this->assertModelData($fakeUsersHasEvent, $dbUsersHasEvent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUsersHasEvent()
    {
        $usersHasEvent = $this->makeUsersHasEvent();
        $resp = $this->usersHasEventRepo->delete($usersHasEvent->id);
        $this->assertTrue($resp);
        $this->assertNull(UsersHasEvent::find($usersHasEvent->id), 'UsersHasEvent should not exist in DB');
    }
}
