<?php

use App\Models\StudentHasCourseInstance;
use App\Repositories\StudentHasCourseInstanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentHasCourseInstanceRepositoryTest extends TestCase
{
    use MakeStudentHasCourseInstanceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StudentHasCourseInstanceRepository
     */
    protected $studentHasCourseInstanceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->studentHasCourseInstanceRepo = App::make(StudentHasCourseInstanceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->fakeStudentHasCourseInstanceData();
        $createdStudentHasCourseInstance = $this->studentHasCourseInstanceRepo->create($studentHasCourseInstance);
        $createdStudentHasCourseInstance = $createdStudentHasCourseInstance->toArray();
        $this->assertArrayHasKey('id', $createdStudentHasCourseInstance);
        $this->assertNotNull($createdStudentHasCourseInstance['id'], 'Created StudentHasCourseInstance must have id specified');
        $this->assertNotNull(StudentHasCourseInstance::find($createdStudentHasCourseInstance['id']), 'StudentHasCourseInstance with given id must be in DB');
        $this->assertModelData($studentHasCourseInstance, $createdStudentHasCourseInstance);
    }

    /**
     * @test read
     */
    public function testReadStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->makeStudentHasCourseInstance();
        $dbStudentHasCourseInstance = $this->studentHasCourseInstanceRepo->find($studentHasCourseInstance->id);
        $dbStudentHasCourseInstance = $dbStudentHasCourseInstance->toArray();
        $this->assertModelData($studentHasCourseInstance->toArray(), $dbStudentHasCourseInstance);
    }

    /**
     * @test update
     */
    public function testUpdateStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->makeStudentHasCourseInstance();
        $fakeStudentHasCourseInstance = $this->fakeStudentHasCourseInstanceData();
        $updatedStudentHasCourseInstance = $this->studentHasCourseInstanceRepo->update($fakeStudentHasCourseInstance, $studentHasCourseInstance->id);
        $this->assertModelData($fakeStudentHasCourseInstance, $updatedStudentHasCourseInstance->toArray());
        $dbStudentHasCourseInstance = $this->studentHasCourseInstanceRepo->find($studentHasCourseInstance->id);
        $this->assertModelData($fakeStudentHasCourseInstance, $dbStudentHasCourseInstance->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStudentHasCourseInstance()
    {
        $studentHasCourseInstance = $this->makeStudentHasCourseInstance();
        $resp = $this->studentHasCourseInstanceRepo->delete($studentHasCourseInstance->id);
        $this->assertTrue($resp);
        $this->assertNull(StudentHasCourseInstance::find($studentHasCourseInstance->id), 'StudentHasCourseInstance should not exist in DB');
    }
}
