<?php

use App\Models\TypeCourse;
use App\Repositories\TypeCourseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeCourseRepositoryTest extends TestCase
{
    use MakeTypeCourseTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeCourseRepository
     */
    protected $typeCourseRepo;

    public function setUp()
    {
        parent::setUp();
        $this->typeCourseRepo = App::make(TypeCourseRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTypeCourse()
    {
        $typeCourse = $this->fakeTypeCourseData();
        $createdTypeCourse = $this->typeCourseRepo->create($typeCourse);
        $createdTypeCourse = $createdTypeCourse->toArray();
        $this->assertArrayHasKey('id', $createdTypeCourse);
        $this->assertNotNull($createdTypeCourse['id'], 'Created TypeCourse must have id specified');
        $this->assertNotNull(TypeCourse::find($createdTypeCourse['id']), 'TypeCourse with given id must be in DB');
        $this->assertModelData($typeCourse, $createdTypeCourse);
    }

    /**
     * @test read
     */
    public function testReadTypeCourse()
    {
        $typeCourse = $this->makeTypeCourse();
        $dbTypeCourse = $this->typeCourseRepo->find($typeCourse->id);
        $dbTypeCourse = $dbTypeCourse->toArray();
        $this->assertModelData($typeCourse->toArray(), $dbTypeCourse);
    }

    /**
     * @test update
     */
    public function testUpdateTypeCourse()
    {
        $typeCourse = $this->makeTypeCourse();
        $fakeTypeCourse = $this->fakeTypeCourseData();
        $updatedTypeCourse = $this->typeCourseRepo->update($fakeTypeCourse, $typeCourse->id);
        $this->assertModelData($fakeTypeCourse, $updatedTypeCourse->toArray());
        $dbTypeCourse = $this->typeCourseRepo->find($typeCourse->id);
        $this->assertModelData($fakeTypeCourse, $dbTypeCourse->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTypeCourse()
    {
        $typeCourse = $this->makeTypeCourse();
        $resp = $this->typeCourseRepo->delete($typeCourse->id);
        $this->assertTrue($resp);
        $this->assertNull(TypeCourse::find($typeCourse->id), 'TypeCourse should not exist in DB');
    }
}
