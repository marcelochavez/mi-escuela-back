<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GlobalEventApiTest extends TestCase
{
    use MakeGlobalEventTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGlobalEvent()
    {
        $globalEvent = $this->fakeGlobalEventData();
        $this->json('POST', '/api/v1/globalEvents', $globalEvent);

        $this->assertApiResponse($globalEvent);
    }

    /**
     * @test
     */
    public function testReadGlobalEvent()
    {
        $globalEvent = $this->makeGlobalEvent();
        $this->json('GET', '/api/v1/globalEvents/'.$globalEvent->id);

        $this->assertApiResponse($globalEvent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGlobalEvent()
    {
        $globalEvent = $this->makeGlobalEvent();
        $editedGlobalEvent = $this->fakeGlobalEventData();

        $this->json('PUT', '/api/v1/globalEvents/'.$globalEvent->id, $editedGlobalEvent);

        $this->assertApiResponse($editedGlobalEvent);
    }

    /**
     * @test
     */
    public function testDeleteGlobalEvent()
    {
        $globalEvent = $this->makeGlobalEvent();
        $this->json('DELETE', '/api/v1/globalEvents/'.$globalEvent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/globalEvents/'.$globalEvent->id);

        $this->assertResponseStatus(404);
    }
}
