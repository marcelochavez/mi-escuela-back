<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeCourseApiTest extends TestCase
{
    use MakeTypeCourseTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTypeCourse()
    {
        $typeCourse = $this->fakeTypeCourseData();
        $this->json('POST', '/api/v1/typeCourses', $typeCourse);

        $this->assertApiResponse($typeCourse);
    }

    /**
     * @test
     */
    public function testReadTypeCourse()
    {
        $typeCourse = $this->makeTypeCourse();
        $this->json('GET', '/api/v1/typeCourses/'.$typeCourse->id);

        $this->assertApiResponse($typeCourse->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTypeCourse()
    {
        $typeCourse = $this->makeTypeCourse();
        $editedTypeCourse = $this->fakeTypeCourseData();

        $this->json('PUT', '/api/v1/typeCourses/'.$typeCourse->id, $editedTypeCourse);

        $this->assertApiResponse($editedTypeCourse);
    }

    /**
     * @test
     */
    public function testDeleteTypeCourse()
    {
        $typeCourse = $this->makeTypeCourse();
        $this->json('DELETE', '/api/v1/typeCourses/'.$typeCourse->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/typeCourses/'.$typeCourse->id);

        $this->assertResponseStatus(404);
    }
}
