<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoleHasSystemMenuApiTest extends TestCase
{
    use MakeRoleHasSystemMenuTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->fakeRoleHasSystemMenuData();
        $this->json('POST', '/api/v1/roleHasSystemMenus', $roleHasSystemMenu);

        $this->assertApiResponse($roleHasSystemMenu);
    }

    /**
     * @test
     */
    public function testReadRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->makeRoleHasSystemMenu();
        $this->json('GET', '/api/v1/roleHasSystemMenus/'.$roleHasSystemMenu->id);

        $this->assertApiResponse($roleHasSystemMenu->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->makeRoleHasSystemMenu();
        $editedRoleHasSystemMenu = $this->fakeRoleHasSystemMenuData();

        $this->json('PUT', '/api/v1/roleHasSystemMenus/'.$roleHasSystemMenu->id, $editedRoleHasSystemMenu);

        $this->assertApiResponse($editedRoleHasSystemMenu);
    }

    /**
     * @test
     */
    public function testDeleteRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->makeRoleHasSystemMenu();
        $this->json('DELETE', '/api/v1/roleHasSystemMenus/'.$roleHasSystemMenu->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roleHasSystemMenus/'.$roleHasSystemMenu->id);

        $this->assertResponseStatus(404);
    }
}
