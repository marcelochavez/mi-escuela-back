<?php

use App\Models\GlobalEvent;
use App\Repositories\GlobalEventRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GlobalEventRepositoryTest extends TestCase
{
    use MakeGlobalEventTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GlobalEventRepository
     */
    protected $globalEventRepo;

    public function setUp()
    {
        parent::setUp();
        $this->globalEventRepo = App::make(GlobalEventRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGlobalEvent()
    {
        $globalEvent = $this->fakeGlobalEventData();
        $createdGlobalEvent = $this->globalEventRepo->create($globalEvent);
        $createdGlobalEvent = $createdGlobalEvent->toArray();
        $this->assertArrayHasKey('id', $createdGlobalEvent);
        $this->assertNotNull($createdGlobalEvent['id'], 'Created GlobalEvent must have id specified');
        $this->assertNotNull(GlobalEvent::find($createdGlobalEvent['id']), 'GlobalEvent with given id must be in DB');
        $this->assertModelData($globalEvent, $createdGlobalEvent);
    }

    /**
     * @test read
     */
    public function testReadGlobalEvent()
    {
        $globalEvent = $this->makeGlobalEvent();
        $dbGlobalEvent = $this->globalEventRepo->find($globalEvent->id);
        $dbGlobalEvent = $dbGlobalEvent->toArray();
        $this->assertModelData($globalEvent->toArray(), $dbGlobalEvent);
    }

    /**
     * @test update
     */
    public function testUpdateGlobalEvent()
    {
        $globalEvent = $this->makeGlobalEvent();
        $fakeGlobalEvent = $this->fakeGlobalEventData();
        $updatedGlobalEvent = $this->globalEventRepo->update($fakeGlobalEvent, $globalEvent->id);
        $this->assertModelData($fakeGlobalEvent, $updatedGlobalEvent->toArray());
        $dbGlobalEvent = $this->globalEventRepo->find($globalEvent->id);
        $this->assertModelData($fakeGlobalEvent, $dbGlobalEvent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGlobalEvent()
    {
        $globalEvent = $this->makeGlobalEvent();
        $resp = $this->globalEventRepo->delete($globalEvent->id);
        $this->assertTrue($resp);
        $this->assertNull(GlobalEvent::find($globalEvent->id), 'GlobalEvent should not exist in DB');
    }
}
