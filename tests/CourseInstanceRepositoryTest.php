<?php

use App\Models\CourseInstance;
use App\Repositories\CourseInstanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseInstanceRepositoryTest extends TestCase
{
    use MakeCourseInstanceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CourseInstanceRepository
     */
    protected $courseInstanceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->courseInstanceRepo = App::make(CourseInstanceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCourseInstance()
    {
        $courseInstance = $this->fakeCourseInstanceData();
        $createdCourseInstance = $this->courseInstanceRepo->create($courseInstance);
        $createdCourseInstance = $createdCourseInstance->toArray();
        $this->assertArrayHasKey('id', $createdCourseInstance);
        $this->assertNotNull($createdCourseInstance['id'], 'Created CourseInstance must have id specified');
        $this->assertNotNull(CourseInstance::find($createdCourseInstance['id']), 'CourseInstance with given id must be in DB');
        $this->assertModelData($courseInstance, $createdCourseInstance);
    }

    /**
     * @test read
     */
    public function testReadCourseInstance()
    {
        $courseInstance = $this->makeCourseInstance();
        $dbCourseInstance = $this->courseInstanceRepo->find($courseInstance->id);
        $dbCourseInstance = $dbCourseInstance->toArray();
        $this->assertModelData($courseInstance->toArray(), $dbCourseInstance);
    }

    /**
     * @test update
     */
    public function testUpdateCourseInstance()
    {
        $courseInstance = $this->makeCourseInstance();
        $fakeCourseInstance = $this->fakeCourseInstanceData();
        $updatedCourseInstance = $this->courseInstanceRepo->update($fakeCourseInstance, $courseInstance->id);
        $this->assertModelData($fakeCourseInstance, $updatedCourseInstance->toArray());
        $dbCourseInstance = $this->courseInstanceRepo->find($courseInstance->id);
        $this->assertModelData($fakeCourseInstance, $dbCourseInstance->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCourseInstance()
    {
        $courseInstance = $this->makeCourseInstance();
        $resp = $this->courseInstanceRepo->delete($courseInstance->id);
        $this->assertTrue($resp);
        $this->assertNull(CourseInstance::find($courseInstance->id), 'CourseInstance should not exist in DB');
    }
}
