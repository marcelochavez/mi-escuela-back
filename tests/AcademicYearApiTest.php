<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AcademicYearApiTest extends TestCase
{
    use MakeAcademicYearTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAcademicYear()
    {
        $academicYear = $this->fakeAcademicYearData();
        $this->json('POST', '/api/v1/academicYears', $academicYear);

        $this->assertApiResponse($academicYear);
    }

    /**
     * @test
     */
    public function testReadAcademicYear()
    {
        $academicYear = $this->makeAcademicYear();
        $this->json('GET', '/api/v1/academicYears/'.$academicYear->id);

        $this->assertApiResponse($academicYear->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAcademicYear()
    {
        $academicYear = $this->makeAcademicYear();
        $editedAcademicYear = $this->fakeAcademicYearData();

        $this->json('PUT', '/api/v1/academicYears/'.$academicYear->id, $editedAcademicYear);

        $this->assertApiResponse($editedAcademicYear);
    }

    /**
     * @test
     */
    public function testDeleteAcademicYear()
    {
        $academicYear = $this->makeAcademicYear();
        $this->json('DELETE', '/api/v1/academicYears/'.$academicYear->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/academicYears/'.$academicYear->id);

        $this->assertResponseStatus(404);
    }
}
