<?php

use Faker\Factory as Faker;
use App\Models\CourseInstanceHasEvent;
use App\Repositories\CourseInstanceHasEventRepository;

trait MakeCourseInstanceHasEventTrait
{
    /**
     * Create fake instance of CourseInstanceHasEvent and save it in database
     *
     * @param array $courseInstanceHasEventFields
     * @return CourseInstanceHasEvent
     */
    public function makeCourseInstanceHasEvent($courseInstanceHasEventFields = [])
    {
        /** @var CourseInstanceHasEventRepository $courseInstanceHasEventRepo */
        $courseInstanceHasEventRepo = App::make(CourseInstanceHasEventRepository::class);
        $theme = $this->fakeCourseInstanceHasEventData($courseInstanceHasEventFields);
        return $courseInstanceHasEventRepo->create($theme);
    }

    /**
     * Get fake instance of CourseInstanceHasEvent
     *
     * @param array $courseInstanceHasEventFields
     * @return CourseInstanceHasEvent
     */
    public function fakeCourseInstanceHasEvent($courseInstanceHasEventFields = [])
    {
        return new CourseInstanceHasEvent($this->fakeCourseInstanceHasEventData($courseInstanceHasEventFields));
    }

    /**
     * Get fake data of CourseInstanceHasEvent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCourseInstanceHasEventData($courseInstanceHasEventFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'course_instance_id' => $fake->randomDigitNotNull,
            'event_id' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $courseInstanceHasEventFields);
    }
}
