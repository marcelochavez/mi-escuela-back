<?php

use Faker\Factory as Faker;
use App\Models\AcademicYear;
use App\Repositories\AcademicYearRepository;

trait MakeAcademicYearTrait
{
    /**
     * Create fake instance of AcademicYear and save it in database
     *
     * @param array $academicYearFields
     * @return AcademicYear
     */
    public function makeAcademicYear($academicYearFields = [])
    {
        /** @var AcademicYearRepository $academicYearRepo */
        $academicYearRepo = App::make(AcademicYearRepository::class);
        $theme = $this->fakeAcademicYearData($academicYearFields);
        return $academicYearRepo->create($theme);
    }

    /**
     * Get fake instance of AcademicYear
     *
     * @param array $academicYearFields
     * @return AcademicYear
     */
    public function fakeAcademicYear($academicYearFields = [])
    {
        return new AcademicYear($this->fakeAcademicYearData($academicYearFields));
    }

    /**
     * Get fake data of AcademicYear
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAcademicYearData($academicYearFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'year' => $fake->word,
            'start_first_semester' => $fake->date('Y-m-d H:i:s'),
            'start_second_semester' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $academicYearFields);
    }
}
