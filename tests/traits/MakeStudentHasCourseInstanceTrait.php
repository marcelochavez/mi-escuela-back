<?php

use Faker\Factory as Faker;
use App\Models\StudentHasCourseInstance;
use App\Repositories\StudentHasCourseInstanceRepository;

trait MakeStudentHasCourseInstanceTrait
{
    /**
     * Create fake instance of StudentHasCourseInstance and save it in database
     *
     * @param array $studentHasCourseInstanceFields
     * @return StudentHasCourseInstance
     */
    public function makeStudentHasCourseInstance($studentHasCourseInstanceFields = [])
    {
        /** @var StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo */
        $studentHasCourseInstanceRepo = App::make(StudentHasCourseInstanceRepository::class);
        $theme = $this->fakeStudentHasCourseInstanceData($studentHasCourseInstanceFields);
        return $studentHasCourseInstanceRepo->create($theme);
    }

    /**
     * Get fake instance of StudentHasCourseInstance
     *
     * @param array $studentHasCourseInstanceFields
     * @return StudentHasCourseInstance
     */
    public function fakeStudentHasCourseInstance($studentHasCourseInstanceFields = [])
    {
        return new StudentHasCourseInstance($this->fakeStudentHasCourseInstanceData($studentHasCourseInstanceFields));
    }

    /**
     * Get fake data of StudentHasCourseInstance
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStudentHasCourseInstanceData($studentHasCourseInstanceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'student_id' => $fake->randomDigitNotNull,
            'course_instance_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $studentHasCourseInstanceFields);
    }
}
