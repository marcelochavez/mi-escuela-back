<?php

use Faker\Factory as Faker;
use App\Models\UsersHasEvent;
use App\Repositories\UsersHasEventRepository;

trait MakeUsersHasEventTrait
{
    /**
     * Create fake instance of UsersHasEvent and save it in database
     *
     * @param array $usersHasEventFields
     * @return UsersHasEvent
     */
    public function makeUsersHasEvent($usersHasEventFields = [])
    {
        /** @var UsersHasEventRepository $usersHasEventRepo */
        $usersHasEventRepo = App::make(UsersHasEventRepository::class);
        $theme = $this->fakeUsersHasEventData($usersHasEventFields);
        return $usersHasEventRepo->create($theme);
    }

    /**
     * Get fake instance of UsersHasEvent
     *
     * @param array $usersHasEventFields
     * @return UsersHasEvent
     */
    public function fakeUsersHasEvent($usersHasEventFields = [])
    {
        return new UsersHasEvent($this->fakeUsersHasEventData($usersHasEventFields));
    }

    /**
     * Get fake data of UsersHasEvent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUsersHasEventData($usersHasEventFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'student_id' => $fake->randomDigitNotNull,
            'event_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $usersHasEventFields);
    }
}
