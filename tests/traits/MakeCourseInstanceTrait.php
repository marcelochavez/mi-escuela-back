<?php

use Faker\Factory as Faker;
use App\Models\CourseInstance;
use App\Repositories\CourseInstanceRepository;

trait MakeCourseInstanceTrait
{
    /**
     * Create fake instance of CourseInstance and save it in database
     *
     * @param array $courseInstanceFields
     * @return CourseInstance
     */
    public function makeCourseInstance($courseInstanceFields = [])
    {
        /** @var CourseInstanceRepository $courseInstanceRepo */
        $courseInstanceRepo = App::make(CourseInstanceRepository::class);
        $theme = $this->fakeCourseInstanceData($courseInstanceFields);
        return $courseInstanceRepo->create($theme);
    }

    /**
     * Get fake instance of CourseInstance
     *
     * @param array $courseInstanceFields
     * @return CourseInstance
     */
    public function fakeCourseInstance($courseInstanceFields = [])
    {
        return new CourseInstance($this->fakeCourseInstanceData($courseInstanceFields));
    }

    /**
     * Get fake data of CourseInstance
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCourseInstanceData($courseInstanceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'course_id' => $fake->randomDigitNotNull,
            'semester_id' => $fake->randomDigitNotNull,
            'year_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $courseInstanceFields);
    }
}
