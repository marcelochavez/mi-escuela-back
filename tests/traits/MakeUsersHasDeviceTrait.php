<?php

use Faker\Factory as Faker;
use App\Models\UsersHasDevice;
use App\Repositories\UsersHasDeviceRepository;

trait MakeUsersHasDeviceTrait
{
    /**
     * Create fake instance of UsersHasDevice and save it in database
     *
     * @param array $usersHasDeviceFields
     * @return UsersHasDevice
     */
    public function makeUsersHasDevice($usersHasDeviceFields = [])
    {
        /** @var UsersHasDeviceRepository $usersHasDeviceRepo */
        $usersHasDeviceRepo = App::make(UsersHasDeviceRepository::class);
        $theme = $this->fakeUsersHasDeviceData($usersHasDeviceFields);
        return $usersHasDeviceRepo->create($theme);
    }

    /**
     * Get fake instance of UsersHasDevice
     *
     * @param array $usersHasDeviceFields
     * @return UsersHasDevice
     */
    public function fakeUsersHasDevice($usersHasDeviceFields = [])
    {
        return new UsersHasDevice($this->fakeUsersHasDeviceData($usersHasDeviceFields));
    }

    /**
     * Get fake data of UsersHasDevice
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUsersHasDeviceData($usersHasDeviceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'device' => $fake->word,
            'users_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $usersHasDeviceFields);
    }
}
