<?php

use Faker\Factory as Faker;
use App\Models\GlobalEvent;
use App\Repositories\GlobalEventRepository;

trait MakeGlobalEventTrait
{
    /**
     * Create fake instance of GlobalEvent and save it in database
     *
     * @param array $globalEventFields
     * @return GlobalEvent
     */
    public function makeGlobalEvent($globalEventFields = [])
    {
        /** @var GlobalEventRepository $globalEventRepo */
        $globalEventRepo = App::make(GlobalEventRepository::class);
        $theme = $this->fakeGlobalEventData($globalEventFields);
        return $globalEventRepo->create($theme);
    }

    /**
     * Get fake instance of GlobalEvent
     *
     * @param array $globalEventFields
     * @return GlobalEvent
     */
    public function fakeGlobalEvent($globalEventFields = [])
    {
        return new GlobalEvent($this->fakeGlobalEventData($globalEventFields));
    }

    /**
     * Get fake data of GlobalEvent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGlobalEventData($globalEventFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'event_id' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $globalEventFields);
    }
}
