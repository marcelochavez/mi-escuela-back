<?php

use Faker\Factory as Faker;
use App\Models\RoleHasSystemMenu;
use App\Repositories\RoleHasSystemMenuRepository;

trait MakeRoleHasSystemMenuTrait
{
    /**
     * Create fake instance of RoleHasSystemMenu and save it in database
     *
     * @param array $roleHasSystemMenuFields
     * @return RoleHasSystemMenu
     */
    public function makeRoleHasSystemMenu($roleHasSystemMenuFields = [])
    {
        /** @var RoleHasSystemMenuRepository $roleHasSystemMenuRepo */
        $roleHasSystemMenuRepo = App::make(RoleHasSystemMenuRepository::class);
        $theme = $this->fakeRoleHasSystemMenuData($roleHasSystemMenuFields);
        return $roleHasSystemMenuRepo->create($theme);
    }

    /**
     * Get fake instance of RoleHasSystemMenu
     *
     * @param array $roleHasSystemMenuFields
     * @return RoleHasSystemMenu
     */
    public function fakeRoleHasSystemMenu($roleHasSystemMenuFields = [])
    {
        return new RoleHasSystemMenu($this->fakeRoleHasSystemMenuData($roleHasSystemMenuFields));
    }

    /**
     * Get fake data of RoleHasSystemMenu
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoleHasSystemMenuData($roleHasSystemMenuFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'role_id' => $fake->randomDigitNotNull,
            'system_menu_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $roleHasSystemMenuFields);
    }
}
