<?php

use Faker\Factory as Faker;
use App\Models\EventsType;
use App\Repositories\EventsTypeRepository;

trait MakeEventsTypeTrait
{
    /**
     * Create fake instance of EventsType and save it in database
     *
     * @param array $eventsTypeFields
     * @return EventsType
     */
    public function makeEventsType($eventsTypeFields = [])
    {
        /** @var EventsTypeRepository $eventsTypeRepo */
        $eventsTypeRepo = App::make(EventsTypeRepository::class);
        $theme = $this->fakeEventsTypeData($eventsTypeFields);
        return $eventsTypeRepo->create($theme);
    }

    /**
     * Get fake instance of EventsType
     *
     * @param array $eventsTypeFields
     * @return EventsType
     */
    public function fakeEventsType($eventsTypeFields = [])
    {
        return new EventsType($this->fakeEventsTypeData($eventsTypeFields));
    }

    /**
     * Get fake data of EventsType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEventsTypeData($eventsTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'category_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $eventsTypeFields);
    }
}
