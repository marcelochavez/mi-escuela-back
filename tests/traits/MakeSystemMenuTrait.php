<?php

use Faker\Factory as Faker;
use App\Models\SystemMenu;
use App\Repositories\SystemMenuRepository;

trait MakeSystemMenuTrait
{
    /**
     * Create fake instance of SystemMenu and save it in database
     *
     * @param array $systemMenuFields
     * @return SystemMenu
     */
    public function makeSystemMenu($systemMenuFields = [])
    {
        /** @var SystemMenuRepository $systemMenuRepo */
        $systemMenuRepo = App::make(SystemMenuRepository::class);
        $theme = $this->fakeSystemMenuData($systemMenuFields);
        return $systemMenuRepo->create($theme);
    }

    /**
     * Get fake instance of SystemMenu
     *
     * @param array $systemMenuFields
     * @return SystemMenu
     */
    public function fakeSystemMenu($systemMenuFields = [])
    {
        return new SystemMenu($this->fakeSystemMenuData($systemMenuFields));
    }

    /**
     * Get fake data of SystemMenu
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSystemMenuData($systemMenuFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'label' => $fake->word,
            'component' => $fake->word,
            'classButton' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $systemMenuFields);
    }
}
