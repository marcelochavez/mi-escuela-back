<?php

use Faker\Factory as Faker;
use App\Models\TypeCourse;
use App\Repositories\TypeCourseRepository;

trait MakeTypeCourseTrait
{
    /**
     * Create fake instance of TypeCourse and save it in database
     *
     * @param array $typeCourseFields
     * @return TypeCourse
     */
    public function makeTypeCourse($typeCourseFields = [])
    {
        /** @var TypeCourseRepository $typeCourseRepo */
        $typeCourseRepo = App::make(TypeCourseRepository::class);
        $theme = $this->fakeTypeCourseData($typeCourseFields);
        return $typeCourseRepo->create($theme);
    }

    /**
     * Get fake instance of TypeCourse
     *
     * @param array $typeCourseFields
     * @return TypeCourse
     */
    public function fakeTypeCourse($typeCourseFields = [])
    {
        return new TypeCourse($this->fakeTypeCourseData($typeCourseFields));
    }

    /**
     * Get fake data of TypeCourse
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTypeCourseData($typeCourseFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $typeCourseFields);
    }
}
