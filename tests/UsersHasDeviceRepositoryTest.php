<?php

use App\Models\UsersHasDevice;
use App\Repositories\UsersHasDeviceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersHasDeviceRepositoryTest extends TestCase
{
    use MakeUsersHasDeviceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UsersHasDeviceRepository
     */
    protected $usersHasDeviceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->usersHasDeviceRepo = App::make(UsersHasDeviceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUsersHasDevice()
    {
        $usersHasDevice = $this->fakeUsersHasDeviceData();
        $createdUsersHasDevice = $this->usersHasDeviceRepo->create($usersHasDevice);
        $createdUsersHasDevice = $createdUsersHasDevice->toArray();
        $this->assertArrayHasKey('id', $createdUsersHasDevice);
        $this->assertNotNull($createdUsersHasDevice['id'], 'Created UsersHasDevice must have id specified');
        $this->assertNotNull(UsersHasDevice::find($createdUsersHasDevice['id']), 'UsersHasDevice with given id must be in DB');
        $this->assertModelData($usersHasDevice, $createdUsersHasDevice);
    }

    /**
     * @test read
     */
    public function testReadUsersHasDevice()
    {
        $usersHasDevice = $this->makeUsersHasDevice();
        $dbUsersHasDevice = $this->usersHasDeviceRepo->find($usersHasDevice->id);
        $dbUsersHasDevice = $dbUsersHasDevice->toArray();
        $this->assertModelData($usersHasDevice->toArray(), $dbUsersHasDevice);
    }

    /**
     * @test update
     */
    public function testUpdateUsersHasDevice()
    {
        $usersHasDevice = $this->makeUsersHasDevice();
        $fakeUsersHasDevice = $this->fakeUsersHasDeviceData();
        $updatedUsersHasDevice = $this->usersHasDeviceRepo->update($fakeUsersHasDevice, $usersHasDevice->id);
        $this->assertModelData($fakeUsersHasDevice, $updatedUsersHasDevice->toArray());
        $dbUsersHasDevice = $this->usersHasDeviceRepo->find($usersHasDevice->id);
        $this->assertModelData($fakeUsersHasDevice, $dbUsersHasDevice->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUsersHasDevice()
    {
        $usersHasDevice = $this->makeUsersHasDevice();
        $resp = $this->usersHasDeviceRepo->delete($usersHasDevice->id);
        $this->assertTrue($resp);
        $this->assertNull(UsersHasDevice::find($usersHasDevice->id), 'UsersHasDevice should not exist in DB');
    }
}
