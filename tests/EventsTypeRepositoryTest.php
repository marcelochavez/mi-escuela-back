<?php

use App\Models\EventsType;
use App\Repositories\EventsTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventsTypeRepositoryTest extends TestCase
{
    use MakeEventsTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EventsTypeRepository
     */
    protected $eventsTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->eventsTypeRepo = App::make(EventsTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEventsType()
    {
        $eventsType = $this->fakeEventsTypeData();
        $createdEventsType = $this->eventsTypeRepo->create($eventsType);
        $createdEventsType = $createdEventsType->toArray();
        $this->assertArrayHasKey('id', $createdEventsType);
        $this->assertNotNull($createdEventsType['id'], 'Created EventsType must have id specified');
        $this->assertNotNull(EventsType::find($createdEventsType['id']), 'EventsType with given id must be in DB');
        $this->assertModelData($eventsType, $createdEventsType);
    }

    /**
     * @test read
     */
    public function testReadEventsType()
    {
        $eventsType = $this->makeEventsType();
        $dbEventsType = $this->eventsTypeRepo->find($eventsType->id);
        $dbEventsType = $dbEventsType->toArray();
        $this->assertModelData($eventsType->toArray(), $dbEventsType);
    }

    /**
     * @test update
     */
    public function testUpdateEventsType()
    {
        $eventsType = $this->makeEventsType();
        $fakeEventsType = $this->fakeEventsTypeData();
        $updatedEventsType = $this->eventsTypeRepo->update($fakeEventsType, $eventsType->id);
        $this->assertModelData($fakeEventsType, $updatedEventsType->toArray());
        $dbEventsType = $this->eventsTypeRepo->find($eventsType->id);
        $this->assertModelData($fakeEventsType, $dbEventsType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEventsType()
    {
        $eventsType = $this->makeEventsType();
        $resp = $this->eventsTypeRepo->delete($eventsType->id);
        $this->assertTrue($resp);
        $this->assertNull(EventsType::find($eventsType->id), 'EventsType should not exist in DB');
    }
}
