<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseInstanceApiTest extends TestCase
{
    use MakeCourseInstanceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCourseInstance()
    {
        $courseInstance = $this->fakeCourseInstanceData();
        $this->json('POST', '/api/v1/courseInstances', $courseInstance);

        $this->assertApiResponse($courseInstance);
    }

    /**
     * @test
     */
    public function testReadCourseInstance()
    {
        $courseInstance = $this->makeCourseInstance();
        $this->json('GET', '/api/v1/courseInstances/'.$courseInstance->id);

        $this->assertApiResponse($courseInstance->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCourseInstance()
    {
        $courseInstance = $this->makeCourseInstance();
        $editedCourseInstance = $this->fakeCourseInstanceData();

        $this->json('PUT', '/api/v1/courseInstances/'.$courseInstance->id, $editedCourseInstance);

        $this->assertApiResponse($editedCourseInstance);
    }

    /**
     * @test
     */
    public function testDeleteCourseInstance()
    {
        $courseInstance = $this->makeCourseInstance();
        $this->json('DELETE', '/api/v1/courseInstances/'.$courseInstance->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/courseInstances/'.$courseInstance->id);

        $this->assertResponseStatus(404);
    }
}
