<?php

use App\Models\RoleHasSystemMenu;
use App\Repositories\RoleHasSystemMenuRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoleHasSystemMenuRepositoryTest extends TestCase
{
    use MakeRoleHasSystemMenuTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoleHasSystemMenuRepository
     */
    protected $roleHasSystemMenuRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roleHasSystemMenuRepo = App::make(RoleHasSystemMenuRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->fakeRoleHasSystemMenuData();
        $createdRoleHasSystemMenu = $this->roleHasSystemMenuRepo->create($roleHasSystemMenu);
        $createdRoleHasSystemMenu = $createdRoleHasSystemMenu->toArray();
        $this->assertArrayHasKey('id', $createdRoleHasSystemMenu);
        $this->assertNotNull($createdRoleHasSystemMenu['id'], 'Created RoleHasSystemMenu must have id specified');
        $this->assertNotNull(RoleHasSystemMenu::find($createdRoleHasSystemMenu['id']), 'RoleHasSystemMenu with given id must be in DB');
        $this->assertModelData($roleHasSystemMenu, $createdRoleHasSystemMenu);
    }

    /**
     * @test read
     */
    public function testReadRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->makeRoleHasSystemMenu();
        $dbRoleHasSystemMenu = $this->roleHasSystemMenuRepo->find($roleHasSystemMenu->id);
        $dbRoleHasSystemMenu = $dbRoleHasSystemMenu->toArray();
        $this->assertModelData($roleHasSystemMenu->toArray(), $dbRoleHasSystemMenu);
    }

    /**
     * @test update
     */
    public function testUpdateRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->makeRoleHasSystemMenu();
        $fakeRoleHasSystemMenu = $this->fakeRoleHasSystemMenuData();
        $updatedRoleHasSystemMenu = $this->roleHasSystemMenuRepo->update($fakeRoleHasSystemMenu, $roleHasSystemMenu->id);
        $this->assertModelData($fakeRoleHasSystemMenu, $updatedRoleHasSystemMenu->toArray());
        $dbRoleHasSystemMenu = $this->roleHasSystemMenuRepo->find($roleHasSystemMenu->id);
        $this->assertModelData($fakeRoleHasSystemMenu, $dbRoleHasSystemMenu->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoleHasSystemMenu()
    {
        $roleHasSystemMenu = $this->makeRoleHasSystemMenu();
        $resp = $this->roleHasSystemMenuRepo->delete($roleHasSystemMenu->id);
        $this->assertTrue($resp);
        $this->assertNull(RoleHasSystemMenu::find($roleHasSystemMenu->id), 'RoleHasSystemMenu should not exist in DB');
    }
}
