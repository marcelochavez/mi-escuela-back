<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersHasDeviceApiTest extends TestCase
{
    use MakeUsersHasDeviceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUsersHasDevice()
    {
        $usersHasDevice = $this->fakeUsersHasDeviceData();
        $this->json('POST', '/api/v1/usersHasDevices', $usersHasDevice);

        $this->assertApiResponse($usersHasDevice);
    }

    /**
     * @test
     */
    public function testReadUsersHasDevice()
    {
        $usersHasDevice = $this->makeUsersHasDevice();
        $this->json('GET', '/api/v1/usersHasDevices/'.$usersHasDevice->id);

        $this->assertApiResponse($usersHasDevice->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUsersHasDevice()
    {
        $usersHasDevice = $this->makeUsersHasDevice();
        $editedUsersHasDevice = $this->fakeUsersHasDeviceData();

        $this->json('PUT', '/api/v1/usersHasDevices/'.$usersHasDevice->id, $editedUsersHasDevice);

        $this->assertApiResponse($editedUsersHasDevice);
    }

    /**
     * @test
     */
    public function testDeleteUsersHasDevice()
    {
        $usersHasDevice = $this->makeUsersHasDevice();
        $this->json('DELETE', '/api/v1/usersHasDevices/'.$usersHasDevice->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/usersHasDevices/'.$usersHasDevice->id);

        $this->assertResponseStatus(404);
    }
}
