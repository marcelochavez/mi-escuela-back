<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersHasEventApiTest extends TestCase
{
    use MakeUsersHasEventTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUsersHasEvent()
    {
        $usersHasEvent = $this->fakeUsersHasEventData();
        $this->json('POST', '/api/v1/usersHasEvents', $usersHasEvent);

        $this->assertApiResponse($usersHasEvent);
    }

    /**
     * @test
     */
    public function testReadUsersHasEvent()
    {
        $usersHasEvent = $this->makeUsersHasEvent();
        $this->json('GET', '/api/v1/usersHasEvents/'.$usersHasEvent->id);

        $this->assertApiResponse($usersHasEvent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUsersHasEvent()
    {
        $usersHasEvent = $this->makeUsersHasEvent();
        $editedUsersHasEvent = $this->fakeUsersHasEventData();

        $this->json('PUT', '/api/v1/usersHasEvents/'.$usersHasEvent->id, $editedUsersHasEvent);

        $this->assertApiResponse($editedUsersHasEvent);
    }

    /**
     * @test
     */
    public function testDeleteUsersHasEvent()
    {
        $usersHasEvent = $this->makeUsersHasEvent();
        $this->json('DELETE', '/api/v1/usersHasEvents/'.$usersHasEvent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/usersHasEvents/'.$usersHasEvent->id);

        $this->assertResponseStatus(404);
    }
}
