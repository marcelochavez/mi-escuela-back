<?php

namespace App\Facades;

use App\Exceptions\NegocioException;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

/**
 * @property User $user Usuario actual
 */
class IdentityFacade extends Facade
{

    private $userRepository;
    private $roleRepository;


    /** @var User */
    public $user = null;

    private $log;

    public static function instance(){return app()->make(IdentityFacade::class);}
    protected static function getFacadeAccessor() { return 'identityFacade'; }

    public function __construct(UserRepository $userRepository,
                                RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;



    }
    public function isLoggedIn(){
        return $this->user != null;
    }

    public function getRole(){
        $user = $this->user;

        return $user->role_id;
    }
    public function getUser(){
        $user = $this->userRepository->findWhere([
            'email'=>$this->user->email
        ])->first();
        return $user;
    }

}

