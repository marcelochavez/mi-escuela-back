<?php

namespace App\Constants;


use MabeEnum\Enum;

class SemesterEnum extends Enum
{
    const NULL = 0;
    const PRIMER = 1;
    const SEGUNDO = 2;
}