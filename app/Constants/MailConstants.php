<?php

namespace App\Constants;



class MailConstants
{
    Const MAIL_TO_RECOVER_PASSWPRD = "Estimad@ @user_name, \n  Hemos generado una contraseña temporal para ti, con duración de 24 horas. Deberás modificarla cuando ingreses a tu cuenta. \n
    CONTRASEÑA: @newPassword \n\n\n Se despide afectuosamente, \n MiEscuelaApp"  ;

    public  static function recoveryPasswordMail ($user, $password){
        $desc = str_replace('@user_name', $user, self::MAIL_TO_RECOVER_PASSWPRD);
        $desc = str_replace('@newPassword', $password, $desc);

        return $desc;
    }
}