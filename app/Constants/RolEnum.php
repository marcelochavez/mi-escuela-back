<?php

namespace App\Constants;


use MabeEnum\Enum;

class RolEnum extends Enum
{
    const NULL = 0;
    const ADMIN = 1;
    const EXTENSION = 2;
    const DOCENCIA = 3;
    const ESTUDIANTE = 4;
}