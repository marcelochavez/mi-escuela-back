<?php

namespace App\Constants;


class Messages
{

    // ERRORES
    const ERROR_GENERAL             = "Ha ocurrido un error interno";
    const ERROR_UNAUTHORIZED        = "Permisos insuficientes";
    const ERROR_IVALID_CREDENTIALS  = "Credenciales inválidas";
    const ERROR_BAD_INPUTS          = "Parametros incorrectos";
    const ERROR_MISSING_INPUTS      = "Faltan parámetros";
    const ERROR_REFRESH_TOKEN       = "Error al refrescar token";
    const ERROR_INVALID_TOKEN       = "Token inválido";
    const ERROR_MISSING_TOKEN       = "Token es requerido";
    const ERROR_GENERAL_TOKEN       = "Problema al intentar crear token";
    const ERROR_BAD_REQUEST         = "La solicitud contiene sintaxis errónea y no debería repetirse";
    const ERROR_NOT_FOUND           = "Recurso no encontrado";
    const ERROR_ACTION_INVALID      = "Esta acción no existe";
    const ERROR_ACTION_NOT_EXECUTED = "La acción no se pudo realizar";
    const ERROR_MAIL_REGISTERED     = "El Email ya está registrado";
    const ERROR_CODE_REGISTERED     = "El código del curso ya existe";

    // EXITO
    const SUCCESS                   = "La acción se ha realizado correctamente";
    const MAIL_MESSAGE              = "Se ha enviado una constraseña de recuperación a su correo. Tienes 24 hrs para actualizar tu contraseña.";
    const REGISTER_SUCCESS_ACTIVATE_MAIL             = "Para finalizar su registro, se ha enviado un email con una contraseña temporal";
    const REGISTER_SUCCESS_WITHOUT_MAIL      = "Registro exitoso. La contraseña no pudo ser evnviada por mail, ya que el uso de mail está desactivado en el servidor.";
    // ACTIONS

    public static function successAction($action){
        return "Acción '$action->name' realizada correctamente";
    }

    public static function errorAction($action){
        return "No se pudo realizar la acción '$action->name'";
    }



}