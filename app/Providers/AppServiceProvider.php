<?php

namespace App\Providers;

use App\Facades\IdentityFacade;
use App\Models\Identity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        app()->singleton(IdentityFacade::class);
        DB::enableQueryLog();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Configure logging to include files and line numbers
        $monolog = \Log::getMonolog();
        $introspection = new \Monolog\Processor\IntrospectionProcessor (
            \Monolog\Logger::DEBUG, // whatever level you want this processor to handle
            [
                'Monolog\\',
                'Illuminate\\',
            ]
        );
        $monolog->pushProcessor($introspection);

        $monolog->pushHandler(new \Monolog\Handler\ErrorLogHandler());
    }
}
