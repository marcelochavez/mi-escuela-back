<?php


namespace App\Exceptions;
use Exception;

class NegocioException extends Exception
{

    /**
     * Create a new authentication exception.
     *
     * @param  string  $message
     * @param  array  $guards
     * @return void
     */
    public function __construct($message = 'Error interno.')
    {
        parent::__construct($message);
    }
}