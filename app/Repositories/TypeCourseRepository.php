<?php

namespace App\Repositories;

use App\Models\TypeCourse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeCourseRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:34 am -03
 *
 * @method TypeCourse findWithoutFail($id, $columns = ['*'])
 * @method TypeCourse find($id, $columns = ['*'])
 * @method TypeCourse first($columns = ['*'])
*/
class TypeCourseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeCourse::class;
    }
}
