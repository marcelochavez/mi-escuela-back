<?php

namespace App\Repositories;

use App\Models\AcademicYear;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AcademicYearRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:39 am -03
 *
 * @method AcademicYear findWithoutFail($id, $columns = ['*'])
 * @method AcademicYear find($id, $columns = ['*'])
 * @method AcademicYear first($columns = ['*'])
*/
class AcademicYearRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'year',
        'start_first_semester',
        'start_second_semester'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AcademicYear::class;
    }
}
