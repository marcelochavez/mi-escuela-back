<?php

namespace App\Repositories;

use App\Models\CourseInstance;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CourseInstanceRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:33 am -03
 *
 * @method CourseInstance findWithoutFail($id, $columns = ['*'])
 * @method CourseInstance find($id, $columns = ['*'])
 * @method CourseInstance first($columns = ['*'])
*/
class CourseInstanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'semester_id',
        'year_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseInstance::class;
    }
}
