<?php

namespace App\Repositories;

use App\Models\CourseInstanceHasEvent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CourseInstanceHasEventRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:42 am -03
 *
 * @method CourseInstanceHasEvent findWithoutFail($id, $columns = ['*'])
 * @method CourseInstanceHasEvent find($id, $columns = ['*'])
 * @method CourseInstanceHasEvent first($columns = ['*'])
*/
class CourseInstanceHasEventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_instance_id',
        'event_id',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseInstanceHasEvent::class;
    }
}
