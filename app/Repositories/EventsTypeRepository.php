<?php

namespace App\Repositories;

use App\Models\EventsType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EventsTypeRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:36 am -03
 *
 * @method EventsType findWithoutFail($id, $columns = ['*'])
 * @method EventsType find($id, $columns = ['*'])
 * @method EventsType first($columns = ['*'])
*/
class EventsTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EventsType::class;
    }
}
