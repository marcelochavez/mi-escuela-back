<?php

namespace App\Repositories;

use App\Models\Priority;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PriorityRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:35 am -03
 *
 * @method Priority findWithoutFail($id, $columns = ['*'])
 * @method Priority find($id, $columns = ['*'])
 * @method Priority first($columns = ['*'])
*/
class PriorityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Priority::class;
    }
}
