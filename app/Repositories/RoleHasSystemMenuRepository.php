<?php

namespace App\Repositories;

use App\Models\RoleHasSystemMenu;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoleHasSystemMenuRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:31 am -03
 *
 * @method RoleHasSystemMenu findWithoutFail($id, $columns = ['*'])
 * @method RoleHasSystemMenu find($id, $columns = ['*'])
 * @method RoleHasSystemMenu first($columns = ['*'])
*/
class RoleHasSystemMenuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'role_id',
        'system_menu_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoleHasSystemMenu::class;
    }
}
