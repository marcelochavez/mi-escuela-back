<?php

namespace App\Repositories;

use App\Models\SystemMenu;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SystemMenuRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:27 am -03
 *
 * @method SystemMenu findWithoutFail($id, $columns = ['*'])
 * @method SystemMenu find($id, $columns = ['*'])
 * @method SystemMenu first($columns = ['*'])
*/
class SystemMenuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'label',
        'component',
        'classButton'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SystemMenu::class;
    }
}
