<?php

namespace App\Repositories\Criterias;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WhereFields implements CriteriaInterface
{

    /**
     * @var array
     */
    protected $where;

    /**
     * WhereFields constructor.
     * @param $where
     */
    public function __construct($where)
    {
        $this->where = $where;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        foreach ($this->where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $model = $model->where($field, $condition, $val);
            } else {
                $model = $model->where($field, '=', $value);
            }
        }

        return $model;
    }
}