<?php

namespace App\Repositories;

use App\Models\GlobalEvent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GlobalEventRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:38 am -03
 *
 * @method GlobalEvent findWithoutFail($id, $columns = ['*'])
 * @method GlobalEvent find($id, $columns = ['*'])
 * @method GlobalEvent first($columns = ['*'])
*/
class GlobalEventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'event_id',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GlobalEvent::class;
    }
}
