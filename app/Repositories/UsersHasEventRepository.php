<?php

namespace App\Repositories;

use App\Models\UsersHasEvent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UsersHasEventRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:33 am -03
 *
 * @method UsersHasEvent findWithoutFail($id, $columns = ['*'])
 * @method UsersHasEvent find($id, $columns = ['*'])
 * @method UsersHasEvent first($columns = ['*'])
*/
class UsersHasEventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'student_id',
        'event_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UsersHasEvent::class;
    }
}
