<?php

namespace App\Repositories;

use App\Models\StudentHasCourseInstance;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StudentHasCourseInstanceRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:28 am -03
 *
 * @method StudentHasCourseInstance findWithoutFail($id, $columns = ['*'])
 * @method StudentHasCourseInstance find($id, $columns = ['*'])
 * @method StudentHasCourseInstance first($columns = ['*'])
*/
class StudentHasCourseInstanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'student_id',
        'course_instance_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StudentHasCourseInstance::class;
    }
}
