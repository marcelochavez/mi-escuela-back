<?php

namespace App\Repositories;

use App\Models\UsersHasDevice;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UsersHasDeviceRepository
 * @package App\Repositories
 * @version November 26, 2018, 6:30 pm -03
 *
 * @method UsersHasDevice findWithoutFail($id, $columns = ['*'])
 * @method UsersHasDevice find($id, $columns = ['*'])
 * @method UsersHasDevice first($columns = ['*'])
*/
class UsersHasDeviceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'device',
        'users_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UsersHasDevice::class;
    }
}
