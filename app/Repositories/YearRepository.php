<?php

namespace App\Repositories;

use App\Models\Year;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class YearRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:35 am -03
 *
 * @method Year findWithoutFail($id, $columns = ['*'])
 * @method Year find($id, $columns = ['*'])
 * @method Year first($columns = ['*'])
*/
class YearRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Year::class;
    }
}
