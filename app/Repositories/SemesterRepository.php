<?php

namespace App\Repositories;

use App\Models\Semester;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SemesterRepository
 * @package App\Repositories
 * @version November 14, 2018, 3:33 am -03
 *
 * @method Semester findWithoutFail($id, $columns = ['*'])
 * @method Semester find($id, $columns = ['*'])
 * @method Semester first($columns = ['*'])
*/
class SemesterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Semester::class;
    }
}
