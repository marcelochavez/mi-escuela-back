<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSystemMenuRequest;
use App\Http\Requests\UpdateSystemMenuRequest;
use App\Repositories\SystemMenuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SystemMenuController extends AppBaseController
{
    /** @var  SystemMenuRepository */
    private $systemMenuRepository;

    public function __construct(SystemMenuRepository $systemMenuRepo)
    {
        $this->systemMenuRepository = $systemMenuRepo;
    }

    /**
     * Display a listing of the SystemMenu.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->systemMenuRepository->pushCriteria(new RequestCriteria($request));
        $systemMenus = $this->systemMenuRepository->all();

        return view('system_menus.index')
            ->with('systemMenus', $systemMenus);
    }

    /**
     * Show the form for creating a new SystemMenu.
     *
     * @return Response
     */
    public function create()
    {
        return view('system_menus.create');
    }

    /**
     * Store a newly created SystemMenu in storage.
     *
     * @param CreateSystemMenuRequest $request
     *
     * @return Response
     */
    public function store(CreateSystemMenuRequest $request)
    {
        $input = $request->all();

        $systemMenu = $this->systemMenuRepository->create($input);

        Flash::success('System Menu saved successfully.');

        return redirect(route('systemMenus.index'));
    }

    /**
     * Display the specified SystemMenu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            Flash::error('System Menu not found');

            return redirect(route('systemMenus.index'));
        }

        return view('system_menus.show')->with('systemMenu', $systemMenu);
    }

    /**
     * Show the form for editing the specified SystemMenu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            Flash::error('System Menu not found');

            return redirect(route('systemMenus.index'));
        }

        return view('system_menus.edit')->with('systemMenu', $systemMenu);
    }

    /**
     * Update the specified SystemMenu in storage.
     *
     * @param  int              $id
     * @param UpdateSystemMenuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSystemMenuRequest $request)
    {
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            Flash::error('System Menu not found');

            return redirect(route('systemMenus.index'));
        }

        $systemMenu = $this->systemMenuRepository->update($request->all(), $id);

        Flash::success('System Menu updated successfully.');

        return redirect(route('systemMenus.index'));
    }

    /**
     * Remove the specified SystemMenu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            Flash::error('System Menu not found');

            return redirect(route('systemMenus.index'));
        }

        $this->systemMenuRepository->delete($id);

        Flash::success('System Menu deleted successfully.');

        return redirect(route('systemMenus.index'));
    }
}
