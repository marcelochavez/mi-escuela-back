<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGlobalEventRequest;
use App\Http\Requests\UpdateGlobalEventRequest;
use App\Repositories\GlobalEventRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GlobalEventController extends AppBaseController
{
    /** @var  GlobalEventRepository */
    private $globalEventRepository;

    public function __construct(GlobalEventRepository $globalEventRepo)
    {
        $this->globalEventRepository = $globalEventRepo;
    }

    /**
     * Display a listing of the GlobalEvent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->globalEventRepository->pushCriteria(new RequestCriteria($request));
        $globalEvents = $this->globalEventRepository->all();

        return view('global_events.index')
            ->with('globalEvents', $globalEvents);
    }

    /**
     * Show the form for creating a new GlobalEvent.
     *
     * @return Response
     */
    public function create()
    {
        return view('global_events.create');
    }

    /**
     * Store a newly created GlobalEvent in storage.
     *
     * @param CreateGlobalEventRequest $request
     *
     * @return Response
     */
    public function store(CreateGlobalEventRequest $request)
    {
        $input = $request->all();

        $globalEvent = $this->globalEventRepository->create($input);

        Flash::success('Global Event saved successfully.');

        return redirect(route('globalEvents.index'));
    }

    /**
     * Display the specified GlobalEvent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $globalEvent = $this->globalEventRepository->findWithoutFail($id);

        if (empty($globalEvent)) {
            Flash::error('Global Event not found');

            return redirect(route('globalEvents.index'));
        }

        return view('global_events.show')->with('globalEvent', $globalEvent);
    }

    /**
     * Show the form for editing the specified GlobalEvent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $globalEvent = $this->globalEventRepository->findWithoutFail($id);

        if (empty($globalEvent)) {
            Flash::error('Global Event not found');

            return redirect(route('globalEvents.index'));
        }

        return view('global_events.edit')->with('globalEvent', $globalEvent);
    }

    /**
     * Update the specified GlobalEvent in storage.
     *
     * @param  int              $id
     * @param UpdateGlobalEventRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGlobalEventRequest $request)
    {
        $globalEvent = $this->globalEventRepository->findWithoutFail($id);

        if (empty($globalEvent)) {
            Flash::error('Global Event not found');

            return redirect(route('globalEvents.index'));
        }

        $globalEvent = $this->globalEventRepository->update($request->all(), $id);

        Flash::success('Global Event updated successfully.');

        return redirect(route('globalEvents.index'));
    }

    /**
     * Remove the specified GlobalEvent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $globalEvent = $this->globalEventRepository->findWithoutFail($id);

        if (empty($globalEvent)) {
            Flash::error('Global Event not found');

            return redirect(route('globalEvents.index'));
        }

        $this->globalEventRepository->delete($id);

        Flash::success('Global Event deleted successfully.');

        return redirect(route('globalEvents.index'));
    }
}
