<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCourseInstanceRequest;
use App\Http\Requests\UpdateCourseInstanceRequest;
use App\Repositories\CourseInstanceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CourseInstanceController extends AppBaseController
{
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;

    public function __construct(CourseInstanceRepository $courseInstanceRepo)
    {
        $this->courseInstanceRepository = $courseInstanceRepo;
    }

    /**
     * Display a listing of the CourseInstance.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->courseInstanceRepository->pushCriteria(new RequestCriteria($request));
        $courseInstances = $this->courseInstanceRepository->all();

        return view('course_instances.index')
            ->with('courseInstances', $courseInstances);
    }

    /**
     * Show the form for creating a new CourseInstance.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_instances.create');
    }

    /**
     * Store a newly created CourseInstance in storage.
     *
     * @param CreateCourseInstanceRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseInstanceRequest $request)
    {
        $input = $request->all();

        $courseInstance = $this->courseInstanceRepository->create($input);

        Flash::success('Course Instance saved successfully.');

        return redirect(route('courseInstances.index'));
    }

    /**
     * Display the specified CourseInstance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            Flash::error('Course Instance not found');

            return redirect(route('courseInstances.index'));
        }

        return view('course_instances.show')->with('courseInstance', $courseInstance);
    }

    /**
     * Show the form for editing the specified CourseInstance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            Flash::error('Course Instance not found');

            return redirect(route('courseInstances.index'));
        }

        return view('course_instances.edit')->with('courseInstance', $courseInstance);
    }

    /**
     * Update the specified CourseInstance in storage.
     *
     * @param  int              $id
     * @param UpdateCourseInstanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseInstanceRequest $request)
    {
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            Flash::error('Course Instance not found');

            return redirect(route('courseInstances.index'));
        }

        $courseInstance = $this->courseInstanceRepository->update($request->all(), $id);

        Flash::success('Course Instance updated successfully.');

        return redirect(route('courseInstances.index'));
    }

    /**
     * Remove the specified CourseInstance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            Flash::error('Course Instance not found');

            return redirect(route('courseInstances.index'));
        }

        $this->courseInstanceRepository->delete($id);

        Flash::success('Course Instance deleted successfully.');

        return redirect(route('courseInstances.index'));
    }
}
