<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStudentHasCourseInstanceRequest;
use App\Http\Requests\UpdateStudentHasCourseInstanceRequest;
use App\Repositories\StudentHasCourseInstanceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StudentHasCourseInstanceController extends AppBaseController
{
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;

    public function __construct(StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo)
    {
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
    }

    /**
     * Display a listing of the StudentHasCourseInstance.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->studentHasCourseInstanceRepository->pushCriteria(new RequestCriteria($request));
        $studentHasCourseInstances = $this->studentHasCourseInstanceRepository->all();

        return view('student_has_course_instances.index')
            ->with('studentHasCourseInstances', $studentHasCourseInstances);
    }

    /**
     * Show the form for creating a new StudentHasCourseInstance.
     *
     * @return Response
     */
    public function create()
    {
        return view('student_has_course_instances.create');
    }

    /**
     * Store a newly created StudentHasCourseInstance in storage.
     *
     * @param CreateStudentHasCourseInstanceRequest $request
     *
     * @return Response
     */
    public function store(CreateStudentHasCourseInstanceRequest $request)
    {
        $input = $request->all();

        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->create($input);

        Flash::success('Student Has Course Instance saved successfully.');

        return redirect(route('studentHasCourseInstances.index'));
    }

    /**
     * Display the specified StudentHasCourseInstance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            Flash::error('Student Has Course Instance not found');

            return redirect(route('studentHasCourseInstances.index'));
        }

        return view('student_has_course_instances.show')->with('studentHasCourseInstance', $studentHasCourseInstance);
    }

    /**
     * Show the form for editing the specified StudentHasCourseInstance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            Flash::error('Student Has Course Instance not found');

            return redirect(route('studentHasCourseInstances.index'));
        }

        return view('student_has_course_instances.edit')->with('studentHasCourseInstance', $studentHasCourseInstance);
    }

    /**
     * Update the specified StudentHasCourseInstance in storage.
     *
     * @param  int              $id
     * @param UpdateStudentHasCourseInstanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStudentHasCourseInstanceRequest $request)
    {
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            Flash::error('Student Has Course Instance not found');

            return redirect(route('studentHasCourseInstances.index'));
        }

        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->update($request->all(), $id);

        Flash::success('Student Has Course Instance updated successfully.');

        return redirect(route('studentHasCourseInstances.index'));
    }

    /**
     * Remove the specified StudentHasCourseInstance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            Flash::error('Student Has Course Instance not found');

            return redirect(route('studentHasCourseInstances.index'));
        }

        $this->studentHasCourseInstanceRepository->delete($id);

        Flash::success('Student Has Course Instance deleted successfully.');

        return redirect(route('studentHasCourseInstances.index'));
    }
}
