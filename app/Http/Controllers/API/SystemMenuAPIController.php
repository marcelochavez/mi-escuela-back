<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSystemMenuAPIRequest;
use App\Http\Requests\API\UpdateSystemMenuAPIRequest;
use App\Models\SystemMenu;
use App\Repositories\SystemMenuRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SystemMenuController
 * @package App\Http\Controllers\API
 */

class SystemMenuAPIController extends AppBaseController
{
    /** @var  SystemMenuRepository */
    private $systemMenuRepository;

    public function __construct(SystemMenuRepository $systemMenuRepo)
    {
        $this->systemMenuRepository = $systemMenuRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/systemMenus",
     *      summary="Get a listing of the SystemMenus.",
     *      tags={"SystemMenu"},
     *      description="Get all SystemMenus",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SystemMenu")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->systemMenuRepository->pushCriteria(new RequestCriteria($request));
        $this->systemMenuRepository->pushCriteria(new LimitOffsetCriteria($request));
        $systemMenus = $this->systemMenuRepository->all();

        return $this->sendResponse($systemMenus->toArray(), 'System Menus retrieved successfully');
    }

    /**
     * @param CreateSystemMenuAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/systemMenus",
     *      summary="Store a newly created SystemMenu in storage",
     *      tags={"SystemMenu"},
     *      description="Store SystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SystemMenu that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SystemMenu")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SystemMenu"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSystemMenuAPIRequest $request)
    {
        $input = $request->all();

        $systemMenus = $this->systemMenuRepository->create($input);

        return $this->sendResponse($systemMenus->toArray(), 'System Menu saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/systemMenus/{id}",
     *      summary="Display the specified SystemMenu",
     *      tags={"SystemMenu"},
     *      description="Get SystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SystemMenu",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SystemMenu"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SystemMenu $systemMenu */
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            return $this->sendError('System Menu not found');
        }

        return $this->sendResponse($systemMenu->toArray(), 'System Menu retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSystemMenuAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/systemMenus/{id}",
     *      summary="Update the specified SystemMenu in storage",
     *      tags={"SystemMenu"},
     *      description="Update SystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SystemMenu",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SystemMenu that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SystemMenu")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SystemMenu"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSystemMenuAPIRequest $request)
    {
        $input = $request->all();

        /** @var SystemMenu $systemMenu */
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            return $this->sendError('System Menu not found');
        }

        $systemMenu = $this->systemMenuRepository->update($input, $id);

        return $this->sendResponse($systemMenu->toArray(), 'SystemMenu updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/systemMenus/{id}",
     *      summary="Remove the specified SystemMenu from storage",
     *      tags={"SystemMenu"},
     *      description="Delete SystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SystemMenu",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var SystemMenu $systemMenu */
        $systemMenu = $this->systemMenuRepository->findWithoutFail($id);

        if (empty($systemMenu)) {
            return $this->sendError('System Menu not found');
        }

        $systemMenu->delete();

        return $this->sendResponse($id, 'System Menu deleted successfully');
    }
}
