<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Constants\SemesterEnum;
use App\Facades\IdentityFacade;
use App\Http\Requests\API\CreateStudentHasCourseInstanceAPIRequest;
use App\Http\Requests\API\UpdateStudentHasCourseInstanceAPIRequest;
use App\Models\StudentHasCourseInstance;
use App\Repositories\StudentHasCourseInstanceRepository;
use App\Repositories\CourseInstanceRepository;
use App\Repositories\YearRepository;
use App\Repositories\AcademicYearRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StudentHasCourseInstanceController
 * @package App\Http\Controllers\API
 */

class StudentHasCourseInstanceAPIController extends AppBaseController
{
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;
    /** @var  YearRepository */
    private $yearRepository;
    /** @var  AcademicYearRepository */
    private $academicYearRepository;

    public function __construct(StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo,
                                CourseInstanceRepository $courseInstanceRepo,
                                YearRepository $yearRepo,
                                AcademicYearRepository $academicYearRepo)
    {
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
        $this->courseInstanceRepository = $courseInstanceRepo;
        $this->yearRepository = $yearRepo;
        $this->academicYearRepository = $academicYearRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/studentHasCourseInstances",
     *      summary="Get a listing of the StudentHasCourseInstances.",
     *      tags={"StudentHasCourseInstance"},
     *      description="Get all StudentHasCourseInstances",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StudentHasCourseInstance")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->studentHasCourseInstanceRepository->pushCriteria(new RequestCriteria($request));
        $this->studentHasCourseInstanceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $studentHasCourseInstances = $this->studentHasCourseInstanceRepository->all();

        return $this->sendResponse($studentHasCourseInstances->toArray(), 'Student Has Course Instances retrieved successfully');
    }

    /**
     * @param CreateStudentHasCourseInstanceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/studentHasCourseInstances",
     *      summary="Store a newly created StudentHasCourseInstance in storage",
     *      tags={"StudentHasCourseInstance"},
     *      description="Store StudentHasCourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StudentHasCourseInstance that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StudentHasCourseInstance")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StudentHasCourseInstance"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStudentHasCourseInstanceAPIRequest $request)
    {
        $input = $request->all();

        $studentHasCourseInstances = $this->studentHasCourseInstanceRepository->create($input);

        return $this->sendResponse($studentHasCourseInstances->toArray(), 'Student Has Course Instance saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/studentHasCourseInstances/{id}",
     *      summary="Display the specified StudentHasCourseInstance",
     *      tags={"StudentHasCourseInstance"},
     *      description="Get StudentHasCourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StudentHasCourseInstance",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StudentHasCourseInstance"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StudentHasCourseInstance $studentHasCourseInstance */
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            return $this->sendError('Student Has Course Instance not found');
        }

        return $this->sendResponse($studentHasCourseInstance->toArray(), 'Student Has Course Instance retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStudentHasCourseInstanceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/studentHasCourseInstances/{id}",
     *      summary="Update the specified StudentHasCourseInstance in storage",
     *      tags={"StudentHasCourseInstance"},
     *      description="Update StudentHasCourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StudentHasCourseInstance",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StudentHasCourseInstance that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StudentHasCourseInstance")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StudentHasCourseInstance"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStudentHasCourseInstanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var StudentHasCourseInstance $studentHasCourseInstance */
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            return $this->sendError('Student Has Course Instance not found');
        }

        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->update($input, $id);

        return $this->sendResponse($studentHasCourseInstance->toArray(), 'StudentHasCourseInstance updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/studentHasCourseInstances/{id}",
     *      summary="Remove the specified StudentHasCourseInstance from storage",
     *      tags={"StudentHasCourseInstance"},
     *      description="Delete StudentHasCourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StudentHasCourseInstance",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var StudentHasCourseInstance $studentHasCourseInstance */
        $studentHasCourseInstance = $this->studentHasCourseInstanceRepository->findWithoutFail($id);

        if (empty($studentHasCourseInstance)) {
            return $this->sendError('Student Has Course Instance not found');
        }

        $studentHasCourseInstance->delete();

        return $this->sendResponse($id, 'Student Has Course Instance deleted successfully');
    }


    public function getIntanceCourseByStudent(){
        $identity = IdentityFacade::instance();
        $userId = $identity->user->id;

        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();


        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id
            ],['id']);

        $studentInstance = $this->studentHasCourseInstanceRepository
            ->findWhereIn('course_instance_id', $courseInstance->toArray());


        $studentCourse = collect();
        $i = 0;
        while ($i < count($studentInstance)){
            if ($studentInstance[$i]->student_id== $userId){
                $studentCourse = $studentCourse->concat([$studentInstance[$i]->course_instance_id]);
            }
            $i++;
        }

        $courseInstance = $this->courseInstanceRepository
            ->with('course')
            ->findWhereIn('id', $studentCourse->toArray());

        return $this->sendResponse($courseInstance, Messages::SUCCESS);
    }
}
