<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Constants\SemesterEnum;
use App\Http\Requests\API\CreateCourseInstanceHasEventAPIRequest;
use App\Http\Requests\API\UpdateCourseInstanceHasEventAPIRequest;
use App\Models\CourseInstanceHasEvent;
use App\Repositories\EventRepository;
use App\Repositories\CourseInstanceHasEventRepository;
use App\Repositories\CourseInstanceRepository;
use App\Repositories\AcademicYearRepository;
use App\Repositories\StudentHasCourseInstanceRepository;
use App\Repositories\UsersHasDeviceRepository;
use App\Repositories\YearRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

/**
 * Class CourseInstanceHasEventController
 * @package App\Http\Controllers\API
 */

class CourseInstanceHasEventAPIController extends AppBaseController
{
    /** @var  CourseInstanceHasEventRepository */
    private $courseInstanceHasEventRepository;
    /** @var  EventRepository */
    private $eventRepository;
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;
    /** @var  AcademicYearRepository */
    private $academicYearRepository;
    /** @var  YearRepository */
    private $yearRepository;
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;
    /** @var  UsersHasDeviceRepository */
    private $usersHasDeviceRepository;

    public function __construct(CourseInstanceHasEventRepository $courseInstanceHasEventRepo,
                                EventRepository $eventRepo,
                                CourseInstanceRepository $courseInstanceRepo,
                                AcademicYearRepository $academicYearRepo,
                                YearRepository $yearRepo,
                                StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo,
                                UsersHasDeviceRepository $usersHasDeviceRepo)
    {
        $this->courseInstanceHasEventRepository = $courseInstanceHasEventRepo;
        $this->eventRepository = $eventRepo;
        $this->courseInstanceRepository = $courseInstanceRepo;
        $this->academicYearRepository = $academicYearRepo;
        $this->yearRepository = $yearRepo;
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
        $this->usersHasDeviceRepository = $usersHasDeviceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/courseInstanceHasEvents",
     *      summary="Get a listing of the CourseInstanceHasEvents.",
     *      tags={"CourseInstanceHasEvent"},
     *      description="Get all CourseInstanceHasEvents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CourseInstanceHasEvent")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->courseInstanceHasEventRepository->pushCriteria(new RequestCriteria($request));
        $this->courseInstanceHasEventRepository->pushCriteria(new LimitOffsetCriteria($request));
        $courseInstanceHasEvents = $this->courseInstanceHasEventRepository
            ->with(['courseInstance.course.typeCourse','event.priority','event.eventsType.category'])->all();

        return $this->sendResponse($courseInstanceHasEvents->toArray(), 'Course Instance Has Events retrieved successfully');
    }

    /**
     * @param CreateCourseInstanceHasEventAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/courseInstanceHasEvents",
     *      summary="Store a newly created CourseInstanceHasEvent in storage",
     *      tags={"CourseInstanceHasEvent"},
     *      description="Store CourseInstanceHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CourseInstanceHasEvent that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CourseInstanceHasEvent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CourseInstanceHasEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCourseInstanceHasEventAPIRequest $request)
    {
        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();

        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id,
                'course_id'=>$request->get('course_id')
            ])->first();

        $event = $this->eventRepository->create([
            'priority_id'=>$request->get('priority_id'),
            'event_type_id'=>$request->get('event_type_id'),
            'event_date'=>$request->get('event_date')
        ]);

        $courseInstanceHasEvents = $this->courseInstanceHasEventRepository->create([
            'course_instance_id'=>$courseInstance->id,
            'event_id'=>$event->id,
            'description'=>$request->get('description')
        ]);


        $event = $this->courseInstanceHasEventRepository ->with(['courseInstance.course.typeCourse','event.priority','event.eventsType.category'])
            ->findWhere([
            'id'=>$courseInstanceHasEvents->id
        ])->first();
        //return $event->courseInstance->course->code.' '.$event->courseInstance->course->name;

        $findStudent = $this->studentHasCourseInstanceRepository->findWhere([
            'course_instance_id'=>$courseInstance->id
        ],['student_id']);


        $this->sendNotificacion($findStudent, $event);
        return $this->sendResponse($findStudent, Messages::SUCCESS);
        //return $this->sendResponse($courseInstanceHasEvents->toArray(), 'Course Instance Has Event saved successfully');
    }

    public function sendNotificacion($users, $event){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($event->event->eventsType->name);
        $notificationBuilder->setBody($event->courseInstance->course->code.' '.$event->courseInstance->course->name)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder
            ->addData(['a_data' => $event->event->eventsType->name.'<br>'.$event->courseInstance->course->code.'-'.$event->courseInstance->course->name]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();


        $allUser = $this->usersHasDeviceRepository->findWhereIn('users_id', $users->toArray());

        $i = 0;
        while ($i < count($allUser)){
            $token = $allUser[$i]->device;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $i++;
        }
        return $notificationBuilder->getBody();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/courseInstanceHasEvents/{id}",
     *      summary="Display the specified CourseInstanceHasEvent",
     *      tags={"CourseInstanceHasEvent"},
     *      description="Get CourseInstanceHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CourseInstanceHasEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CourseInstanceHasEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CourseInstanceHasEvent $courseInstanceHasEvent */
        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->findWithoutFail($id);

        if (empty($courseInstanceHasEvent)) {
            return $this->sendError('Course Instance Has Event not found');
        }

        return $this->sendResponse($courseInstanceHasEvent->toArray(), 'Course Instance Has Event retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCourseInstanceHasEventAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/courseInstanceHasEvents/{id}",
     *      summary="Update the specified CourseInstanceHasEvent in storage",
     *      tags={"CourseInstanceHasEvent"},
     *      description="Update CourseInstanceHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CourseInstanceHasEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CourseInstanceHasEvent that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CourseInstanceHasEvent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CourseInstanceHasEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCourseInstanceHasEventAPIRequest $request)
    {

        $inputEvent = [
          'priority_id'=>$request->get('priority_id'),
          'event_type_id'=>$request->get('event_type_id'),
          'event_date'=>$request->get('event_date')
        ];

        $event = $this->eventRepository->update($inputEvent, $id);


        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();

        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id,
                'course_id'=>$request->get('course_id')
            ])->first();


        $input = [
            'course_instance_id'=>$courseInstance->id,
            'description'=>$request->get('description')
        ];

        $idEvent = $this->courseInstanceHasEventRepository->findWhere([
            'event_id'=>$id
        ])->first();


        if (empty($idEvent)) {
            return $this->sendError('Course Instance Has Event not found');
        }

        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->update($input, $idEvent->id);

        return $this->sendResponse($courseInstanceHasEvent->toArray(), 'CourseInstanceHasEvent updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/courseInstanceHasEvents/{id}",
     *      summary="Remove the specified CourseInstanceHasEvent from storage",
     *      tags={"CourseInstanceHasEvent"},
     *      description="Delete CourseInstanceHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CourseInstanceHasEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        $idEvent = $this->eventRepository->findWhere([
            'id'=>$id
        ])->first();

        if (empty($idEvent)) {
            return $this->sendError('Event not found');
        }

        $idCourseEvent = $this->courseInstanceHasEventRepository->findWhere([
            'event_id'=>$id
        ],['id']);


        $i = 0;
        while ($i < count($idCourseEvent)){
            $this->courseInstanceHasEventRepository->delete($idCourseEvent[$i]->id);
            $i++;
        }

        $this->eventRepository->delete($idEvent->id);

        return $this->sendResponse($id, 'Course Instance Has Event deleted successfully');
    }
}
