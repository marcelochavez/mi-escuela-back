<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Http\Requests\API\CreateUsersHasEventAPIRequest;
use App\Http\Requests\API\UpdateUsersHasEventAPIRequest;
use App\Models\UsersHasEvent;
use App\Repositories\UsersHasEventRepository;
use App\Repositories\EventRepository;
use App\Repositories\UsersHasDeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

/**
 * Class UsersHasEventController
 * @package App\Http\Controllers\API
 */

class UsersHasEventAPIController extends AppBaseController
{
    /** @var  UsersHasEventRepository */
    private $usersHasEventRepository;
    /** @var  EventRepository */
    private $eventRepository;
    /** @var  UsersHasDeviceRepository */
    private $usersHasDeviceRepository;

    public function __construct(UsersHasEventRepository $usersHasEventRepo,
                                EventRepository $eventRepo,
                                UsersHasDeviceRepository $usersHasDeviceRepo)
    {
        $this->usersHasEventRepository = $usersHasEventRepo;
        $this->eventRepository = $eventRepo;
        $this->usersHasDeviceRepository = $usersHasDeviceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/usersHasEvents",
     *      summary="Get a listing of the UsersHasEvents.",
     *      tags={"UsersHasEvent"},
     *      description="Get all UsersHasEvents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UsersHasEvent")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->usersHasEventRepository->pushCriteria(new RequestCriteria($request));
        $this->usersHasEventRepository->pushCriteria(new LimitOffsetCriteria($request));
        $usersHasEvents = $this->usersHasEventRepository->with(['event.priority','event.eventsType.category'])->all();

        return $this->sendResponse($usersHasEvents->toArray(), 'Users Has Events retrieved successfully');
    }

    /**
     * @param CreateUsersHasEventAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/usersHasEvents",
     *      summary="Store a newly created UsersHasEvent in storage",
     *      tags={"UsersHasEvent"},
     *      description="Store UsersHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UsersHasEvent that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UsersHasEvent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsersHasEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUsersHasEventAPIRequest $request)
    {
        $event = $this->eventRepository->create([
            'priority_id'=>$request->get('priority_id'),
            'event_type_id'=>$request->get('event_type_id'),
            'event_date'=>$request->get('event_date')
        ]);
        $i = 0;
        while ($i < count($request->get('student_id'))){
            $usersHasEvents = $this->usersHasEventRepository->create([
                'student_id'=>$request->get('student_id')[$i],
                'event_id'=>$event->id,
                'description'=>$request->get('description')
            ]);
            $i++;
        }
        $events = $this->eventRepository->with([
            'eventsType.category',
            'priority',
            'usersHasEvents.user'])
            ->findWhere([
            'id'=>$event->id
        ])->first();


      $this->sendNotificacion($events, $request->get('student_id'));

        return $this->sendResponse($usersHasEvents->toArray(), 'Users Has Event saved successfully');
    }

    public function sendNotificacion($events, $students){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Estudiantes: '.$events->usersHasEvents[0]->description);
        $notificationBuilder->setBody('Fecha de Evento '.$events->event_date)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => $events->eventsType->name.'<br>Para Siguientes Estudiantes...<br><br> Fecha de Evento: '.$events->event_date]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();


        $allUser = $this->usersHasDeviceRepository->findWhereIn('users_id', $students);

        $i = 0;
        while ($i < count($allUser)){
            $token = $allUser[$i]->device;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $i++;
        }
        return $notificationBuilder->getBody();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/usersHasEvents/{id}",
     *      summary="Display the specified UsersHasEvent",
     *      tags={"UsersHasEvent"},
     *      description="Get UsersHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsersHasEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsersHasEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UsersHasEvent $usersHasEvent */
        $usersHasEvent = $this->usersHasEventRepository->findWithoutFail($id);

        if (empty($usersHasEvent)) {
            return $this->sendError('Users Has Event not found');
        }

        return $this->sendResponse($usersHasEvent->toArray(), 'Users Has Event retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUsersHasEventAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/usersHasEvents/{id}",
     *      summary="Update the specified UsersHasEvent in storage",
     *      tags={"UsersHasEvent"},
     *      description="Update UsersHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsersHasEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UsersHasEvent that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UsersHasEvent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsersHasEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUsersHasEventAPIRequest $request)
    {
        $inputEvent = [
            'priority_id'=>$request->get('priority_id'),
            'event_type_id'=>$request->get('event_type_id'),
            'event_date'=>$request->get('event_date')
        ];

        $event = $this->eventRepository->update($inputEvent, $id);

        $buscarUserEvent = $this->usersHasEventRepository->findWhere([
            'event_id'=>$id
        ]);

        $arrayStudent = $request->get('student_id');
        // Eliminar
        $i =0;
        while ($i < count($buscarUserEvent)){
            $j=0;
            $eliminar = true;
            while ($j < count($arrayStudent)){
                if ($buscarUserEvent[$i]->student_id == $arrayStudent[$j]){
                    $eliminar = false;
                }
                $j++;
            }
            if ($eliminar){
                $this->usersHasEventRepository->delete($buscarUserEvent[$i]->id);
            }
            $i++;
        }

        //Editar/Crear
        $i=0;

        while ($i < count($arrayStudent)){
            $j = 0;
            $crear = true;
            while ($j < count($buscarUserEvent)){
                if ($arrayStudent[$i] == $buscarUserEvent[$j]->student_id){
                    $this->usersHasEventRepository
                        ->update(['description'=>$request->get('description')], $buscarUserEvent[$j]->id);
                    $crear = false;
                }
                $j++;
            }
            if ($crear){
                $this->usersHasEventRepository->create([
                    'student_id'=>$arrayStudent[$i],
                    'event_id'=>$id,
                    'description'=>$request->get('description')
                ]);
            }
            $i++;
        }



        return $this->sendResponse('Eliminado Correctamente', 'UsersHasEvent updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/usersHasEvents/{id}",
     *      summary="Remove the specified UsersHasEvent from storage",
     *      tags={"UsersHasEvent"},
     *      description="Delete UsersHasEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsersHasEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {

        $idEvent = $this->eventRepository->findWhere([
            'id'=>$id
        ])->first();

        if (empty($idEvent)) {
            return $this->sendError('Event not found');
        }

        $idUserEvent = $this->usersHasEventRepository->findWhere([
            'event_id'=>$id
        ],['id']);


        $i = 0;
        while ($i < count($idUserEvent)){
            $this->usersHasEventRepository->delete($idUserEvent[$i]->id);
            $i++;
        }

        $this->eventRepository->delete($idEvent->id);

        return $this->sendResponse($id, 'Users Has Event deleted successfully');
    }
}
