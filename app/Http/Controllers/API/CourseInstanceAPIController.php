<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
USE App\Constants\SemesterEnum;
use App\Http\Requests\API\CreateCourseInstanceAPIRequest;
use App\Http\Requests\API\UpdateCourseInstanceAPIRequest;
use App\Models\CourseInstance;
use App\Repositories\CourseInstanceRepository;
use App\Repositories\StudentHasCourseInstanceRepository;
use App\Repositories\SemesterRepository;
use App\Repositories\YearRepository;
use App\Repositories\AcademicYearRepository;
use App\Repositories\CourseRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CourseInstanceController
 * @package App\Http\Controllers\API
 */

class CourseInstanceAPIController extends AppBaseController
{
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;
    /** @var  YearRepository */
    private $yearRepository;
    /** @var  SemesterRepository */
    private $semesterRepository;
    /** @var  AcademicYearRepository */
    private $academicYearRepository;
    /** @var  CourseRepository */
    private $courseRepository;
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;

    public function __construct(CourseInstanceRepository $courseInstanceRepo,
                                SemesterRepository $semesterRepo,
                                YearRepository $yearRepo,
                                AcademicYearRepository $academicYearRepo,
                                CourseRepository $courseRepo,
                                StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo)
    {
        $this->courseInstanceRepository = $courseInstanceRepo;
        $this->semesterRepository = $semesterRepo;
        $this->yearRepository = $yearRepo;
        $this->academicYearRepository = $academicYearRepo;
        $this->courseRepository = $courseRepo;
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/courseInstances",
     *      summary="Get a listing of the CourseInstances.",
     *      tags={"CourseInstance"},
     *      description="Get all CourseInstances",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CourseInstance")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->courseInstanceRepository->pushCriteria(new RequestCriteria($request));
        $this->courseInstanceRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$courseInstances = $this->courseInstanceRepository->all();

        $courseInstances = $this->courseInstanceRepository->with(['course.typeCourse','semester','year'])->all();

        return $this->sendResponse($courseInstances->toArray(), 'Course Instances retrieved successfully');
    }

    /**
     * @param CreateCourseInstanceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/courseInstances",
     *      summary="Store a newly created CourseInstance in storage",
     *      tags={"CourseInstance"},
     *      description="Store CourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CourseInstance that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CourseInstance")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CourseInstance"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    //TODO: terminar instancia de curso: OK
    public function store(CreateCourseInstanceAPIRequest $request)
    {
        $input = $request->all();
        // Buscar Semestre
        $findSemester = $this->semesterRepository->findWhere([
            'id'=>$request->get('semester_id')
        ])->first();
        // Buscar año
        $findYear = $this->yearRepository->findWhere([
            'name'=>$request->get('year_name')
        ])->first();
        // Si no exite crear año
        if (!empty($findYear)){
            $idYear = $findYear->id;

            $findAllCourse = $this->courseInstanceRepository->findWhere([
                'year_id'=>$idYear,
                'semester_id'=>$findSemester->id
            ]);

            //return $findAllCourse;

            // Eliminar instancias
                $courseId = $request->get('course_id');
                $i = 0;
                while ($i < count($findAllCourse)){
                    $eliminar = true;
                    $j = 0;
                    while ($j < count($courseId)){
                        if ($courseId[$j] == $findAllCourse[$i]->course_id){
                            $eliminar = false;
                        }
                        $j++;
                    }
                    //return $this->sendResponse($eliminar, 'sjfe');
                    if ($eliminar){

                        $studentInstance = $this->studentHasCourseInstanceRepository->findWhere([
                            'course_instance_id'=>$findAllCourse[$i]->id
                        ]);

                        $k = 0;
                        while ($k<count($studentInstance)){
                            $this->studentHasCourseInstanceRepository->delete($studentInstance[$k]->id);
                            $k++;
                        }
                        $this->courseInstanceRepository->delete($findAllCourse[$i]->id);
                    }
                    $i++;
                }

                $i =0;
            $courseId = $request->get('course_id');
                while ($i < count($courseId)){
                    $j = 0;
                    $crear = true;
                    while ($j < count($findAllCourse)){
                        if ($courseId[$i] == $findAllCourse[$j]->course_id){
                            $crear = false;
                        }
                        $j++;
                    }

                    if ($crear){
                        $courseInstances = $this->courseInstanceRepository->create([
                            'semester_id'=>$findSemester->id,
                            'year_id'=>$idYear,
                            'course_id'=>$courseId[$i]
                        ]);
                    }
                    $i++;
                }
        }else {
            $newYear = $this->yearRepository->create([
                'name'=>$request->get('year_name')
            ]);
            $idYear = $newYear->id;

            // Crear instancia
            $i = 0;
            while ($i < count($request->get('course_id'))){
                $courseInstances = $this->courseInstanceRepository->create([
                    'semester_id'=>$findSemester->id,
                    'year_id'=>$idYear,
                    'course_id'=>$request->get('course_id')[$i]
                ]);
                $i++;
            }

        }

        return $this->sendResponse('ok', 'Se crearon las instancias correctamente');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/courseInstances/{id}",
     *      summary="Display the specified CourseInstance",
     *      tags={"CourseInstance"},
     *      description="Get CourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CourseInstance",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CourseInstance"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CourseInstance $courseInstance */
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            return $this->sendError('Course Instance not found');
        }

        return $this->sendResponse($courseInstance->toArray(), 'Course Instance retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCourseInstanceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/courseInstances/{id}",
     *      summary="Update the specified CourseInstance in storage",
     *      tags={"CourseInstance"},
     *      description="Update CourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CourseInstance",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CourseInstance that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CourseInstance")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CourseInstance"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCourseInstanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var CourseInstance $courseInstance */
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            return $this->sendError('Course Instance not found');
        }

        $courseInstance = $this->courseInstanceRepository->update($input, $id);

        return $this->sendResponse($courseInstance->toArray(), 'CourseInstance updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/courseInstances/{id}",
     *      summary="Remove the specified CourseInstance from storage",
     *      tags={"CourseInstance"},
     *      description="Delete CourseInstance",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CourseInstance",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CourseInstance $courseInstance */
        $courseInstance = $this->courseInstanceRepository->findWithoutFail($id);

        if (empty($courseInstance)) {
            return $this->sendError('Course Instance not found');
        }

        $courseInstance->delete();

        return $this->sendResponse($id, 'Course Instance deleted successfully');
    }

    public function findCourseInstanceByInstance(Request $request){

        $semesterId = $request->get('semester_id');
        $yearId = $request->get('year_id');
        $courseInstance = $this->courseInstanceRepository
            ->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semesterId,
                'year_id'=>$yearId
            ]);
        $courseInstanceFinal = collect();
        $i = $request->get('initial');
        while ($i <= $request->get('final') && $i<= (count($courseInstance)-1)) {
            $courseInstanceFinal = $courseInstanceFinal->concat([$courseInstance[$i]]);
            $i++;
        }

        return $this->sendResponseInstanceCourse($courseInstanceFinal,Messages::SUCCESS, count($courseInstance));
        //return $this->sendResponse($courseInstance, Messages::SUCCESS);
    }
    public function findCourseInstanceByInstancex(Request $request){
        $semesterId = $request->get('semester_id');
        $yearId = $request->get('year_id');

        $courseInstance = $this->courseInstanceRepository
            ->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semesterId,
                'year_id'=>$yearId
            ]);

        return $this->sendResponse($courseInstance,Messages::SUCCESS);
    }
    public function currentInstancesCourses() {
        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();


        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id
            ],['course_id']);

        //return $courseInstance;
        $course = $this->courseRepository->with('typeCourse')->findWhereIn('id', $courseInstance->toArray());

        return $this->sendResponse($course, Messages::SUCCESS);
        return $this->sendResponse($courseInstance,Messages::SUCCESS);


    }
}
