<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Constants\RolEnum;
use App\Http\Requests\API\CreateCourseAPIRequest;
use App\Http\Requests\API\UpdateCourseAPIRequest;
use App\Models\Course;
use App\Repositories\CourseRepository;
use App\Repositories\CourseInstanceRepository;
use App\Repositories\CourseInstanceHasEventRepository;
use App\Repositories\StudentHasCourseInstanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Facades\IdentityFacade;
use Response;

/**
 * Class CourseController
 * @package App\Http\Controllers\API
 */

class CourseAPIController extends AppBaseController
{
    /** @var  CourseRepository */
    private $courseRepository;
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;
    /** @var  CourseInstanceHasEventRepository */
    private $courseInstanceHasEventRepository;
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;

    public function __construct(CourseRepository $courseRepo,
                                CourseInstanceRepository $courseInstanceRepo,
                                CourseInstanceHasEventRepository $courseInstanceHasEventRepo,
                                StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo)
    {
        $this->courseRepository = $courseRepo;
        $this->courseInstanceRepository = $courseInstanceRepo;
        $this->courseInstanceHasEventRepository = $courseInstanceHasEventRepo;
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/courses",
     *      summary="Get a listing of the Courses.",
     *      tags={"Course"},
     *      description="Get all Courses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Course")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();

        if($role != RolEnum::ADMIN){
            $this->sendError(Messages::ERROR_UNAUTHORIZED,404);
        }
        $this->courseRepository->pushCriteria(new RequestCriteria($request));
        $this->courseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $courses = $this->courseRepository->with('typeCourse')->all();

        return $this->sendResponse($courses->toArray(), 'Courses retrieved successfully');
    }

    /**
     * @param CreateCourseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/courses",
     *      summary="Store a newly created Course in storage",
     *      tags={"Course"},
     *      description="Store Course",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Course that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Course")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Course"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCourseAPIRequest $request)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();
        if($role != RolEnum::ADMIN){
            return $this->sendError(Messages::ERROR_UNAUTHORIZED,404);
        }
        $findCourse = $this->courseRepository->findWhere([
            'code'=>$request->get('code')
        ])->first();
        if (!empty($findCourse)){
            return $this->sendError(Messages::ERROR_CODE_REGISTERED);
        }

        $input = $request->all();
        $courses = $this->courseRepository->create($input);
        return $this->sendResponse($courses->toArray(), 'Course saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/courses/{id}",
     *      summary="Display the specified Course",
     *      tags={"Course"},
     *      description="Get Course",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Course",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Course"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Course $course */
        $course = $this->courseRepository->with('typeCourse')->findWithoutFail($id);

        if (empty($course)) {
            return $this->sendError('Course not found');
        }

        return $this->sendResponse($course->toArray(), 'Course retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCourseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/courses/{id}",
     *      summary="Update the specified Course in storage",
     *      tags={"Course"},
     *      description="Update Course",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Course",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Course that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Course")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Course"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCourseAPIRequest $request)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();

        if($role != RolEnum::ADMIN){
            $this->sendError(Messages::ERROR_UNAUTHORIZED,404);
        }

        $input = $request->all();

        /** @var Course $course */
        $course = $this->courseRepository->findWithoutFail($id);

        if (empty($course)) {
            return $this->sendError('Course not found');
        }

        $findCode = $this->courseRepository->findWhere([
            'code'=>$request->get('code')
        ])->first();


        if (!empty($findCode) ) {
            if ($course->id != $findCode->id) {
                return $this->sendError(Messages::ERROR_CODE_REGISTERED);
            }
        }

        $course = $this->courseRepository->update($input, $id);

        return $this->sendResponse($course->toArray(), 'Course updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/courses/{id}",
     *      summary="Remove the specified Course from storage",
     *      tags={"Course"},
     *      description="Delete Course",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Course",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    //TODO: Falta eliminar instancias: OK
    public function destroy($id)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();

        if($role != RolEnum::ADMIN){
            $this->sendError(Messages::ERROR_UNAUTHORIZED,404);
        }
        /** @var Course $course */
        $course = $this->courseRepository->findWithoutFail($id);

        if (empty($course)) {
            return $this->sendError('Course not found');
        }
        //Buscar instancias de cursos
        $allInstances = $this->courseInstanceRepository->findWhere([
            'course_id'=>$id
        ]);

        if (count($allInstances) > 0){
            $i = 0;
            while ($i < count($allInstances)){
                $studentInstance = $this->studentHasCourseInstanceRepository->findWhere([
                   'course_instance_id'=>$allInstances[$i]->id
                ]);
                if (count($studentInstance) >0){
                    $j = 0;
                    while ($j < count($studentInstance)){
                        $this->studentHasCourseInstanceRepository->delete($studentInstance[$j]->id);
                        $j++;
                    }
                }
                $eventInstance = $this->courseInstanceHasEventRepository->findWhere([
                    'course_instance_id'=>$allInstances[$i]->id
                ]);
                if (count($eventInstance) >0){
                    $k =0;
                    while ($k < count($eventInstance)){
                        $this->courseInstanceHasEventRepository->delete($eventInstance[$k]->id);
                        $k++;
                    }
                }
                $this->courseInstanceRepository->delete($allInstances[$i]->id);
                $i++;
            }
        }
        $course->delete();

        return $this->sendResponse($id, 'Course deleted successfully');
    }
}
