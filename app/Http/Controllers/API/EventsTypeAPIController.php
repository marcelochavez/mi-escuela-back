<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEventsTypeAPIRequest;
use App\Http\Requests\API\UpdateEventsTypeAPIRequest;
use App\Models\EventsType;
use App\Repositories\EventsTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EventsTypeController
 * @package App\Http\Controllers\API
 */

class EventsTypeAPIController extends AppBaseController
{
    /** @var  EventsTypeRepository */
    private $eventsTypeRepository;

    public function __construct(EventsTypeRepository $eventsTypeRepo)
    {
        $this->eventsTypeRepository = $eventsTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/eventsTypes",
     *      summary="Get a listing of the EventsTypes.",
     *      tags={"EventsType"},
     *      description="Get all EventsTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/EventsType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->eventsTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->eventsTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $eventsTypes = $this->eventsTypeRepository->with('category')->all();

        return $this->sendResponse($eventsTypes->toArray(), 'Events Types retrieved successfully');
    }

    /**
     * @param CreateEventsTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/eventsTypes",
     *      summary="Store a newly created EventsType in storage",
     *      tags={"EventsType"},
     *      description="Store EventsType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EventsType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EventsType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EventsType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEventsTypeAPIRequest $request)
    {
        $input = $request->all();

        $eventsTypes = $this->eventsTypeRepository->create($input);

        return $this->sendResponse($eventsTypes->toArray(), 'Events Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/eventsTypes/{id}",
     *      summary="Display the specified EventsType",
     *      tags={"EventsType"},
     *      description="Get EventsType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EventsType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EventsType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var EventsType $eventsType */
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            return $this->sendError('Events Type not found');
        }

        return $this->sendResponse($eventsType->toArray(), 'Events Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEventsTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/eventsTypes/{id}",
     *      summary="Update the specified EventsType in storage",
     *      tags={"EventsType"},
     *      description="Update EventsType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EventsType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EventsType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EventsType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EventsType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEventsTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var EventsType $eventsType */
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            return $this->sendError('Events Type not found');
        }

        $eventsType = $this->eventsTypeRepository->update($input, $id);

        return $this->sendResponse($eventsType->toArray(), 'EventsType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/eventsTypes/{id}",
     *      summary="Remove the specified EventsType from storage",
     *      tags={"EventsType"},
     *      description="Delete EventsType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EventsType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var EventsType $eventsType */
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            return $this->sendError('Events Type not found');
        }

        $eventsType->delete();

        return $this->sendResponse($id, 'Events Type deleted successfully');
    }
}
