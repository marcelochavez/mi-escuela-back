<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Http\Requests\API\CreateGlobalEventAPIRequest;
use App\Http\Requests\API\UpdateGlobalEventAPIRequest;
use App\Models\GlobalEvent;
use App\Repositories\GlobalEventRepository;
use App\Repositories\EventRepository;
use App\Repositories\UsersHasDeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

/**
 * Class GlobalEventController
 * @package App\Http\Controllers\API
 */

class GlobalEventAPIController extends AppBaseController
{
    /** @var  GlobalEventRepository */
    private $globalEventRepository;
    /** @var  EventRepository */
    private $eventRepository;
    /** @var  UsersHasDeviceRepository */
    private $usersHasDeviceRepository;

    public function __construct(GlobalEventRepository $globalEventRepo,
                                EventRepository $eventRepo,
                                UsersHasDeviceRepository $usersHasDeviceRepo)
    {
        $this->globalEventRepository = $globalEventRepo;
        $this->eventRepository = $eventRepo;
        $this->usersHasDeviceRepository = $usersHasDeviceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/globalEvents",
     *      summary="Get a listing of the GlobalEvents.",
     *      tags={"GlobalEvent"},
     *      description="Get all GlobalEvents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/GlobalEvent")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->globalEventRepository->pushCriteria(new RequestCriteria($request));
        $this->globalEventRepository->pushCriteria(new LimitOffsetCriteria($request));
        $globalEvents = $this->globalEventRepository->with(['event.priority','event.eventsType.category'])->all();

        return $this->sendResponse($globalEvents->toArray(), 'Global Events retrieved successfully');
    }

    /**
     * @param CreateGlobalEventAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/globalEvents",
     *      summary="Store a newly created GlobalEvent in storage",
     *      tags={"GlobalEvent"},
     *      description="Store GlobalEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="GlobalEvent that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/GlobalEvent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GlobalEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateGlobalEventAPIRequest $request)
    {
        $event = $this->eventRepository->create([
            'priority_id'=>$request->get('priority_id'),
            'event_type_id'=>$request->get('event_type_id'),
            'event_date'=>$request->get('event_date')
        ]);
        $globalEvents = $this->globalEventRepository->create([
            'event_id'=>$event->id,
            'description'=>$request->get('description')
        ]);

        $events = $this->globalEventRepository
            ->with(['event.priority','event.eventsType.category'])
            ->findWhere([
           'id'=>$globalEvents->id
        ])->first();

        $this->sendNotificacion($events);
        return $this->sendResponse(Messages::SUCCESS, Messages::SUCCESS);

       // return $this->sendResponse($globalEvents->toArray(), 'Global Event saved successfully');
    }

    public function sendNotificacion($events){
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($events->event->eventsType->name);
        $notificationBuilder->setBody('Para: '.$events->event->event_date)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => $events->event->eventsType->name.'<br> Para: '.$events->event->event_date]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();


        $allUser = $this->usersHasDeviceRepository->all();
        $i = 0;
        while ($i < count($allUser)){
            $token = $allUser[$i]->device;
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $i++;
        }
        return $notificationBuilder->getBody();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/globalEvents/{id}",
     *      summary="Display the specified GlobalEvent",
     *      tags={"GlobalEvent"},
     *      description="Get GlobalEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GlobalEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GlobalEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var GlobalEvent $globalEvent */
        $globalEvent = $this->globalEventRepository->findWithoutFail($id);

        if (empty($globalEvent)) {
            return $this->sendError('Global Event not found');
        }

        return $this->sendResponse($globalEvent->toArray(), 'Global Event retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateGlobalEventAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/globalEvents/{id}",
     *      summary="Update the specified GlobalEvent in storage",
     *      tags={"GlobalEvent"},
     *      description="Update GlobalEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GlobalEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="GlobalEvent that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/GlobalEvent")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GlobalEvent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateGlobalEventAPIRequest $request)
    {
        $inputEvent = [
            'priority_id'=>$request->get('priority_id'),
            'event_type_id'=>$request->get('event_type_id'),
            'event_date'=>$request->get('event_date')
        ];

        $event = $this->eventRepository->update($inputEvent, $id);


        $input = ['description'=>$request->get('description')];

        /** @var GlobalEvent $globalEvent */
        $globalEvent = $this->globalEventRepository->findWhere([
            'event_id'=>$id
        ])->first();


        if (empty($globalEvent)) {
            return $this->sendError('Global Event not found');
        }

        $globalEvent = $this->globalEventRepository->update($input, $globalEvent->id);

        return $this->sendResponse($globalEvent->toArray(), 'GlobalEvent updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/globalEvents/{id}",
     *      summary="Remove the specified GlobalEvent from storage",
     *      tags={"GlobalEvent"},
     *      description="Delete GlobalEvent",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GlobalEvent",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        $idEvent = $this->eventRepository->findWhere([
            'id'=>$id
        ])->first();

        if (empty($idEvent)) {
            return $this->sendError('Event not found');
        }

        $idGlobalEvent = $this->globalEventRepository->findWhere([
            'event_id'=>$id
        ],['id']);

        $i = 0;
        while ($i < count($idGlobalEvent)){
            $this->globalEventRepository->delete($idGlobalEvent[$i]->id);
            $i++;
        }

        $this->eventRepository->delete($idEvent->id);

        return $this->sendResponse($id, 'Global Event deleted successfully');
    }
}
