<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Http\Requests\API\CreateUsersHasDeviceAPIRequest;
use App\Http\Requests\API\UpdateUsersHasDeviceAPIRequest;
use App\Models\UsersHasDevice;
use App\Repositories\UsersHasDeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UsersHasDeviceController
 * @package App\Http\Controllers\API
 */

class UsersHasDeviceAPIController extends AppBaseController
{
    /** @var  UsersHasDeviceRepository */
    private $usersHasDeviceRepository;

    public function __construct(UsersHasDeviceRepository $usersHasDeviceRepo)
    {
        $this->usersHasDeviceRepository = $usersHasDeviceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/usersHasDevices",
     *      summary="Get a listing of the UsersHasDevices.",
     *      tags={"UsersHasDevice"},
     *      description="Get all UsersHasDevices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UsersHasDevice")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->usersHasDeviceRepository->pushCriteria(new RequestCriteria($request));
        $this->usersHasDeviceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $usersHasDevices = $this->usersHasDeviceRepository->all();

        return $this->sendResponse($usersHasDevices->toArray(), 'Users Has Devices retrieved successfully');
    }

    /**
     * @param CreateUsersHasDeviceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/usersHasDevices",
     *      summary="Store a newly created UsersHasDevice in storage",
     *      tags={"UsersHasDevice"},
     *      description="Store UsersHasDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UsersHasDevice that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UsersHasDevice")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsersHasDevice"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUsersHasDeviceAPIRequest $request)
    {
        $input = $request->all();

        $validation = $this->usersHasDeviceRepository->findWhere([
            'users_id'=>$request->get('users_id'),
            'device'=>$request->get('device')
        ]);

        if (count($validation) <= 0){
            // return "entre";
            $usersHasDevices = $this->usersHasDeviceRepository->create($input);
            return $this->sendResponse($usersHasDevices->toArray(), 'Users Has Device saved successfully');
        }else{
            return $this->sendResponse("token", Messages::SUCCESS);
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/usersHasDevices/{id}",
     *      summary="Display the specified UsersHasDevice",
     *      tags={"UsersHasDevice"},
     *      description="Get UsersHasDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsersHasDevice",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsersHasDevice"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UsersHasDevice $usersHasDevice */
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            return $this->sendError('Users Has Device not found');
        }

        return $this->sendResponse($usersHasDevice->toArray(), 'Users Has Device retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUsersHasDeviceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/usersHasDevices/{id}",
     *      summary="Update the specified UsersHasDevice in storage",
     *      tags={"UsersHasDevice"},
     *      description="Update UsersHasDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsersHasDevice",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UsersHasDevice that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UsersHasDevice")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsersHasDevice"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUsersHasDeviceAPIRequest $request)
    {
        $input = $request->all();

        /** @var UsersHasDevice $usersHasDevice */
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            return $this->sendError('Users Has Device not found');
        }

        $usersHasDevice = $this->usersHasDeviceRepository->update($input, $id);

        return $this->sendResponse($usersHasDevice->toArray(), 'UsersHasDevice updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/usersHasDevices/{id}",
     *      summary="Remove the specified UsersHasDevice from storage",
     *      tags={"UsersHasDevice"},
     *      description="Delete UsersHasDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsersHasDevice",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UsersHasDevice $usersHasDevice */
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            return $this->sendError('Users Has Device not found');
        }

        $usersHasDevice->delete();

        return $this->sendResponse($id, 'Users Has Device deleted successfully');
    }
}
