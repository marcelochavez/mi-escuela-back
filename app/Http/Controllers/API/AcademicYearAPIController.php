<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAcademicYearAPIRequest;
use App\Http\Requests\API\UpdateAcademicYearAPIRequest;
use App\Models\AcademicYear;
use App\Repositories\AcademicYearRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AcademicYearController
 * @package App\Http\Controllers\API
 */

class AcademicYearAPIController extends AppBaseController
{
    /** @var  AcademicYearRepository */
    private $academicYearRepository;

    public function __construct(AcademicYearRepository $academicYearRepo)
    {
        $this->academicYearRepository = $academicYearRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/academicYears",
     *      summary="Get a listing of the AcademicYears.",
     *      tags={"AcademicYear"},
     *      description="Get all AcademicYears",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AcademicYear")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->academicYearRepository->pushCriteria(new RequestCriteria($request));
        $this->academicYearRepository->pushCriteria(new LimitOffsetCriteria($request));
        $academicYears = $this->academicYearRepository->all();


        $aux = collect();
        //Ordenar
        $i = 0;
        while ($i < count($academicYears)){
            $j = 0;
            while ($j < count($academicYears)-1){
                if ($academicYears[$j]->year < $academicYears[$j+1]->year){
                    $aux = $academicYears[$j];
                    $academicYears[$j] = $academicYears[$j+1];
                    $academicYears[$j+1] = $aux;
                }
                $j++;
            }
            $i++;
        }


        return $this->sendResponse($academicYears->toArray(), 'Academic Years retrieved successfully');
    }

    /**
     * @param CreateAcademicYearAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/academicYears",
     *      summary="Store a newly created AcademicYear in storage",
     *      tags={"AcademicYear"},
     *      description="Store AcademicYear",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AcademicYear that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AcademicYear")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AcademicYear"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAcademicYearAPIRequest $request)
    {
        $find = $this->academicYearRepository->findWhere([
            'year'=> $request->get('year')
        ])->first();

        if (!empty($find)){
            return $this->sendError('Ya existe este año académico');
        }
        $input = $request->all();

        $academicYears = $this->academicYearRepository->create($input);

        return $this->sendResponse($academicYears->toArray(), 'Academic Year saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/academicYears/{id}",
     *      summary="Display the specified AcademicYear",
     *      tags={"AcademicYear"},
     *      description="Get AcademicYear",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AcademicYear",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AcademicYear"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var AcademicYear $academicYear */
        $academicYear = $this->academicYearRepository->findWithoutFail($id);

        if (empty($academicYear)) {
            return $this->sendError('Academic Year not found');
        }

        return $this->sendResponse($academicYear->toArray(), 'Academic Year retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAcademicYearAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/academicYears/{id}",
     *      summary="Update the specified AcademicYear in storage",
     *      tags={"AcademicYear"},
     *      description="Update AcademicYear",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AcademicYear",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AcademicYear that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AcademicYear")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AcademicYear"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAcademicYearAPIRequest $request)
    {
        $input = $request->all();

        /** @var AcademicYear $academicYear */
        $academicYear = $this->academicYearRepository->findWithoutFail($id);

        if (empty($academicYear)) {
            return $this->sendError('Academic Year not found');
        }

        $academicYear = $this->academicYearRepository->update($input, $id);

        return $this->sendResponse($academicYear->toArray(), 'AcademicYear updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/academicYears/{id}",
     *      summary="Remove the specified AcademicYear from storage",
     *      tags={"AcademicYear"},
     *      description="Delete AcademicYear",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AcademicYear",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var AcademicYear $academicYear */
        $academicYear = $this->academicYearRepository->findWithoutFail($id);

        if (empty($academicYear)) {
            return $this->sendError('Academic Year not found');
        }

        $academicYear->delete();

        return $this->sendResponse($id, 'Academic Year deleted successfully');
    }
}
