<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Facades\IdentityFacade;
use App\Http\Requests\API\CreateRoleHasSystemMenuAPIRequest;
use App\Http\Requests\API\UpdateRoleHasSystemMenuAPIRequest;
use App\Models\RoleHasSystemMenu;
use App\Repositories\RoleHasSystemMenuRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SystemMenuRepository;
use Response;

/**
 * Class RoleHasSystemMenuController
 * @package App\Http\Controllers\API
 */

class RoleHasSystemMenuAPIController extends AppBaseController
{
    /** @var  RoleHasSystemMenuRepository */
    private $roleHasSystemMenuRepository;

    /** @var  SystemMenuRepository */
    private $systemMenuRepository;

    public function __construct(RoleHasSystemMenuRepository $roleHasSystemMenuRepo,
                                SystemMenuRepository $systemMenuRepo)
    {
        $this->roleHasSystemMenuRepository = $roleHasSystemMenuRepo;
        $this->systemMenuRepository = $systemMenuRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/roleHasSystemMenus",
     *      summary="Get a listing of the RoleHasSystemMenus.",
     *      tags={"RoleHasSystemMenu"},
     *      description="Get all RoleHasSystemMenus",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/RoleHasSystemMenu")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->roleHasSystemMenuRepository->pushCriteria(new RequestCriteria($request));
        $this->roleHasSystemMenuRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roleHasSystemMenus = $this->roleHasSystemMenuRepository->all();

        return $this->sendResponse($roleHasSystemMenus->toArray(), 'Role Has System Menus retrieved successfully');
    }

    /**
     * @param CreateRoleHasSystemMenuAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/roleHasSystemMenus",
     *      summary="Store a newly created RoleHasSystemMenu in storage",
     *      tags={"RoleHasSystemMenu"},
     *      description="Store RoleHasSystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RoleHasSystemMenu that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RoleHasSystemMenu")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RoleHasSystemMenu"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRoleHasSystemMenuAPIRequest $request)
    {
        $input = $request->all();

        $roleHasSystemMenus = $this->roleHasSystemMenuRepository->create($input);

        return $this->sendResponse($roleHasSystemMenus->toArray(), 'Role Has System Menu saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/roleHasSystemMenus/{id}",
     *      summary="Display the specified RoleHasSystemMenu",
     *      tags={"RoleHasSystemMenu"},
     *      description="Get RoleHasSystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RoleHasSystemMenu",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RoleHasSystemMenu"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var RoleHasSystemMenu $roleHasSystemMenu */
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            return $this->sendError('Role Has System Menu not found');
        }

        return $this->sendResponse($roleHasSystemMenu->toArray(), 'Role Has System Menu retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRoleHasSystemMenuAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/roleHasSystemMenus/{id}",
     *      summary="Update the specified RoleHasSystemMenu in storage",
     *      tags={"RoleHasSystemMenu"},
     *      description="Update RoleHasSystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RoleHasSystemMenu",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RoleHasSystemMenu that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RoleHasSystemMenu")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RoleHasSystemMenu"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRoleHasSystemMenuAPIRequest $request)
    {
        $input = $request->all();

        /** @var RoleHasSystemMenu $roleHasSystemMenu */
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            return $this->sendError('Role Has System Menu not found');
        }

        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->update($input, $id);

        return $this->sendResponse($roleHasSystemMenu->toArray(), 'RoleHasSystemMenu updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/roleHasSystemMenus/{id}",
     *      summary="Remove the specified RoleHasSystemMenu from storage",
     *      tags={"RoleHasSystemMenu"},
     *      description="Delete RoleHasSystemMenu",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RoleHasSystemMenu",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var RoleHasSystemMenu $roleHasSystemMenu */
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            return $this->sendError('Role Has System Menu not found');
        }

        $roleHasSystemMenu->delete();

        return $this->sendResponse($id, 'Role Has System Menu deleted successfully');
    }

    public function findSystemMenu()
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();

        $menuId = $this->roleHasSystemMenuRepository->findWhere([
            'role_id'=>$role
        ],['system_menu_id']);

        $menu = $this->systemMenuRepository->findWhereIn('id',$menuId->toArray());

        return $this->sendResponse($menu,Messages::SUCCESS);
    }
}
