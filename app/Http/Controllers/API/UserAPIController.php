<?php

namespace App\Http\Controllers\API;

use App\Constants\SemesterEnum;
use App\Facades\IdentityFacade;
use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Repositories\StudentHasCourseInstanceRepository;
use App\Repositories\CourseInstanceRepository;
use App\Repositories\UsersHasEventRepository;
use App\Repositories\YearRepository;
use App\Repositories\AcademicYearRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Constants\RolEnum;
use App\Constants\Messages;
use App\Constants\MailConstants;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Helpers\MailHelper;
use Response;
use JWTAuth;
use JWTAuthException;
use Hash;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    /** @var  RoleRepository */
    private $roleRepository;
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;
    /** @var  UsersHasEventRepository */
    private $usersHasEventRepository;
    /** @var  YearRepository */
    private $yearRepository;
    /** @var  AcademicYearRepository */
    private $academicYearRepository;

    public function __construct(UserRepository $userRepo,
                                RoleRepository $roleRepo,
                                StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo,
                                CourseInstanceRepository $courseInstanceRepo,
                                UsersHasEventRepository $usersHasEventRepo,
                                YearRepository $yearRepo,
                                AcademicYearRepository $academicYearRepo)
    {
        $this->userRepository = $userRepo;
        $this->roleRepository = $roleRepo;
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
        $this->courseInstanceRepository = $courseInstanceRepo;
        $this->usersHasEventRepository = $usersHasEventRepo;
        $this->yearRepository = $yearRepo;
        $this->academicYearRepository = $academicYearRepo;
    }

    public function studentRegister(Request $request){
        try {
            $email = $request->get('email');
            $newPass = str_random(5);
            $role = RolEnum::ESTUDIANTE;
            if (!$request->has(['name','last_name','email', 'phone'])){
                return $this->sendError(Messages::ERROR_MISSING_INPUTS,400);
            }
            $findEmail = $this->userRepository->findWhere([
                'email'=>$email
            ])->first();
            if($findEmail){
                return $this->sendError(Messages::ERROR_MAIL_REGISTERED,400);
            }
            $user = $this->userRepository->create([
                'name' => $request->get('name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'role_id' => $role,
                'password' => bcrypt($newPass)
            ]);

            $response = [];
            if(MailHelper::isSendMailActive()){
                $body = MailConstants::recoveryPasswordMail($user->name, $newPass);
                MailHelper::sendMail($email, 'MiEscuelaApp- Contraseña temporal para activar cuenta', $body);
                $resetFinishDate = Carbon::now()->addDay();
                $input = ['reset_finish_date' => $resetFinishDate];
                $usr = $this->userRepository->findWhere(['email' => $request->email])->first();
                $this->userRepository->update($input, $usr->id);
                $msg = Messages::REGISTER_SUCCESS_ACTIVATE_MAIL;
            } else {
                $response['password'] = $newPass;
                $msg = Messages::REGISTER_SUCCESS_WITHOUT_MAIL;
            }

           if ( (!empty($request->get('course_id')[0]))  && ((count($request->get('course_id'))) > 0)   ){
                $course = $request->get('course_id');
                $this->createInstance($course, $user->id);
           }
            return $this->sendResponse($response, $msg);

        }catch (\Exception $e){

            return $e;
        }
    }

    public function changePassword(UpdateUserAPIRequest $request){

        $identity = IdentityFacade::instance();
        $identity = $identity->getUser();

        $findUser = $this->userRepository->findWhere(['id'=>$identity->id])->first();
        if(!Hash::check($request->password, $findUser->password )){
            return $this->sendError(Messages::ERROR_IVALID_CREDENTIALS);
        }
        $input = ['password'=>bcrypt($request->get('newPassword'))];
        $user = $this->userRepository->update($input,$identity->id);
        if(!empty($findUser->reset_finish_date)){
            $resetFinishDate = ['reset_finish_date'=>null];
            $user = $this->userRepository->update($resetFinishDate, $identity->id);
        }
        return $this->sendResponse($user,Messages::SUCCESS);

    }


    public function createInstance($course, $userId){

        //buscar instancia de cursos
        $newInstance = $this->courseInstanceRepository
            ->findWhereIn('course_id',$course);

        $i=0;
        while ($i < count($course)){
            $this->studentHasCourseInstanceRepository->create([
                'student_id'=>$userId,
                'course_instance_id'=>$newInstance[$i]->id
            ]);
            $i++;
        }
    }

    //login app movil
    public function studentLogin(Request $request)
    {
        $validation = $this->userRepository->findWhere([
            'email'=>$request->email,
            'role_id'=>RolEnum::ESTUDIANTE
        ])->first();

        if($validation==null){
            return $this->sendError(Messages::ERROR_IVALID_CREDENTIALS, 401);
        }
        $changePass = false;
        if(!empty($validation->reset_finish_date)){
            if($validation->reset_finish_date > Carbon::now()){
                $changePass = true;
            }
            elseif ($validation->reset_finish_date < Carbon::now()){
                return $this->sendError("Caduco la contraseña", 422);
            }
        }
        $credentials = $request->only('email', 'password');
        $token = null;

        $token = JWTAuth::attempt(['email'=>$request->get('email'),'password'=>$request->get('password'), 'deleted_at'=>null]);

        //return $this->sendResponse($token,Messages::SUCCESS);
        try {
            if (!$token =  JWTAuth::attempt(['email'=>$request->get('email'),'password'=>$request->get('password'), 'deleted_at'=>null])) {
                return $this->sendError(Messages::ERROR_IVALID_CREDENTIALS, 422);
            }
        } catch (JWTAuthException $e) {
            return $this->sendError(Messages::ERROR_GENERAL_TOKEN, 500);
        }
        $value = compact('token');
        $value = array_add($value,"changePass",$changePass);
        $value = array_add($value, 'id', $validation->id);
        return $this->sendResponse($value, Messages::SUCCESS);
    }

    // Login app Web
    public function generalLogin(Request $request){
        $validation = $this->userRepository->findWhere(['email'=>$request->email])->first();


        if($validation==null || $validation->role_id == RolEnum::ESTUDIANTE){
            return $this->sendError(Messages::ERROR_IVALID_CREDENTIALS, 401);
        }
        $changePass = false;
        if(!empty($validation->reset_finish_date)){
            if($validation->reset_finish_date > Carbon::now()){
                $changePass = true;
            }
            elseif ($validation->reset_finish_date < Carbon::now()){
                return $this->sendError("Caduco la contraseña", 422);
            }
        }
        $credentials = $request->only('email', 'password');
        $token = null;

        $token = JWTAuth::attempt(['email'=>$request->get('email'),'password'=>$request->get('password'), 'deleted_at'=>null]);


        try {
            if (!$token = JWTAuth::attempt(['email'=>$request->get('email'),'password'=>$request->get('password'), 'deleted_at'=>null])) {
                return $this->sendError(Messages::ERROR_IVALID_CREDENTIALS, 422);
            }
        } catch (JWTAuthException $e) {
            return $this->sendError(Messages::ERROR_GENERAL_TOKEN, 500);
        }
        $value = compact('token');
        $value = array_add($value,"changePass",$changePass);
        return $this->sendResponse($value, Messages::SUCCESS);

    }



    public function generalRegister(Request $request){
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();
        if($role != RolEnum::ADMIN){
            return $this->sendError(Messages::ERROR_UNAUTHORIZED);
        }

        try {
            $email = $request->get('email');
            $newPass = str_random(5);
            $role = $request->get('role_id');
            if (!$request->has(['name','last_name','email', 'phone','role_id'])){
                return $this->sendError(Messages::ERROR_MISSING_INPUTS,400);
            }
            $findRole = $this->roleRepository->findWhere(['id'=>$role])->first();
            if(empty($findRole)){
                return $this->sendError('Role not found');
            }

            $findEmail = $this->userRepository->findWhere([
                'email'=>$email
            ])->first();
            if($findEmail){
                $this->sendError(Messages::ERROR_MAIL_REGISTERED,400);
            }
            $user = $this->userRepository->create([
                'name'=>$request->get('name'),
                'last_name'=>$request->get('last_name'),
                'email'=>$request->get('email'),
                'phone'=> $request->get('phone'),
                'role_id'=>$role,
                'password'=>bcrypt($newPass)
            ]);

            $response = [];
            if(MailHelper::isSendMailActive()){
                $body = MailConstants::recoveryPasswordMail($user->name, $newPass);
                MailHelper::sendMail($email, 'MiEscuelaApp- Contraseña temporal para activar cuenta', $body);
                $resetFinishDate = Carbon::now()->addDay();
                $input = ['reset_finish_date' => $resetFinishDate];
                $usr = $this->userRepository->findWhere(['email' => $request->email])->first();
                $this->userRepository->update($input, $usr->id);
                $msg = Messages::REGISTER_SUCCESS_ACTIVATE_MAIL;
            } else {
                $response['password'] = $newPass;
                $msg = Messages::REGISTER_SUCCESS_WITHOUT_MAIL;
            }
            return $this->sendResponse($response, $msg);

        }catch (\Exception $e){

            return $e;
        }
    }



    public function reset(UpdateUserAPIRequest $request){

        $verificar = $this->userRepository->findWhere(['email'=>$request->email])->first();

        if(empty($verificar)){
            return $this->sendError(Messages::ERROR_NOT_FOUND,404);
        }
        $newPassword = str_random(5);
        $input = ['password' => bcrypt($newPassword)];

        $body = MailConstants::recoveryPasswordMail($verificar->name, $newPassword);

        MailHelper::sendMail($request['email'],'Mi Escuela - Restauración de contraseña', $body);

        $resetFinishDate = Carbon::now()->addDay();
        $input = array_add($input,'reset_finish_date',$resetFinishDate);

        $user = $this->userRepository->update($input, $verificar->id);

        return $this->sendResponse("",Messages::MAIL_MESSAGE);

    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/users",
     *      summary="Get a listing of the Users.",
     *      tags={"User"},
     *      description="Get all Users",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        $users = $this->userRepository->all();

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * @param CreateUserAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/users",
     *      summary="Store a newly created User in storage",
     *      tags={"User"},
     *      description="Store User",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/User")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $users = $this->userRepository->create($input);

        return $this->sendResponse($users->toArray(), 'User saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/users/{id}",
     *      summary="Display the specified User",
     *      tags={"User"},
     *      description="Get User",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of User",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->with('role')->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUserAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/users/{id}",
     *      summary="Update the specified User in storage",
     *      tags={"User"},
     *      description="Update User",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of User",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/User")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();
        if ($role != RolEnum::ADMIN){
            return $this->sendError(Messages::ERROR_UNAUTHORIZED);
        }
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $findEmail = $this->userRepository->findWhere([
            'email'=>$request->get('email')
        ])->first();

        if (!empty($findEmail)){
            if($user->id != $findEmail->id){
                return $this->sendError(Messages::ERROR_MAIL_REGISTERED);
            }
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/users/{id}",
     *      summary="Remove the specified User from storage",
     *      tags={"User"},
     *      description="Delete User",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of User",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();

        if ($role != RolEnum::ADMIN){
            return $this->sendError(Messages::ERROR_UNAUTHORIZED);
        }

        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }


    public function dataUser(){
        $identity = IdentityFacade::instance();
        $user = $identity->getUser();

        $user = $this->userRepository->with('role')->findWhere([
            'id'=>$user->id
        ])->first();

        return $this->sendResponse($user,Messages::SUCCESS);

    }

    public function usersByRole(Request $request)
    {
        $role = $request->get('role_id');

        $users = $this->userRepository->findWhere([
            'role_id'=>$role
        ]);

        return $this->sendResponse($users, Messages::SUCCESS);
    }

    public function studentsList()
    {
            $students = $this->userRepository
                ->with(['studentHasCourseInstances.courseInstance.course',
                    'studentHasCourseInstances.courseInstance.semester',
                    'studentHasCourseInstances.courseInstance.year'])
                ->findWhere([
                'role_id'=>RolEnum::ESTUDIANTE
            ]);

            return $this->sendResponse($students, Messages::SUCCESS);
    }

    public function studentById($id){
        $students = $this->userRepository
            ->with(['studentHasCourseInstances.courseInstance.course',
                'studentHasCourseInstances.courseInstance.semester',
                'studentHasCourseInstances.courseInstance.year'])
            ->findWhere([
                'id'=>$id,
                'role_id'=>RolEnum::ESTUDIANTE
            ])->first();

        $count = count($students->studentHasCourseInstances);
        $students = array_add($students,'courseCount',$count );

        return $this->sendResponse($students, Messages::SUCCESS);
    }
//TODO: terminar: OK
    public function studentById2($id){
        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();

        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id
            ],['id']);

        //return $courseInstance;

        $instanciasEstudiante = $this->studentHasCourseInstanceRepository
            ->with(['courseInstance.course',
            'courseInstance.semester',
            'courseInstance.year'])
            ->findWhereIn('course_instance_id',$courseInstance->toArray());

        //return $instanciasEstudiante[0];

        $i = 0;
        $instanciasEstudianteFinal = collect();
        while ($i < count($instanciasEstudiante)){
            if ($instanciasEstudiante[$i]->student_id == $id){
                $instanciasEstudianteFinal = $instanciasEstudianteFinal->concat(
                    [$instanciasEstudiante[$i]
                ]);
            }
            $i++;
        }

        $students = $this->userRepository->findWhere([
                'id'=>$id,
                'role_id'=>RolEnum::ESTUDIANTE
            ])->first();

        $count = count($instanciasEstudianteFinal);
        $students = array_add($students,'courseCount',$count );
        $students = array_add($students,'student_has_course_instances',$instanciasEstudianteFinal->toArray());

        //return $students->student_has_course_instances[0];

        return $this->sendResponse($students, Messages::SUCCESS);
    }



/**

    public function currentInstancesCourses() {
        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();


        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id
            ],['course_id']);

        //return $courseInstance;
        $course = $this->courseRepository->with('typeCourse')->findWhereIn('id', $courseInstance->toArray());

        return $this->sendResponse($course, Messages::SUCCESS);
        return $this->sendResponse($courseInstance,Messages::SUCCESS);


    }


    **/

    public function studentUpdate($id,UpdateUserAPIRequest $request){


        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        $yearId = $this->yearRepository->findWhere([
            'name'=>$currentYear
        ])->first();

        $courseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id
            ],['id']);

       // return $courseInstance;

        $instanciasEstudiante = $this->studentHasCourseInstanceRepository
            ->with(['courseInstance.course',
                'courseInstance.semester',
                'courseInstance.year'])
            ->findWhereIn('course_instance_id',$courseInstance->toArray());


        $i = 0;
        $instanciasEstudianteFinal = collect();
        while ($i < count($instanciasEstudiante)){
            if ($instanciasEstudiante[$i]->student_id == $id){
                $instanciasEstudianteFinal = $instanciasEstudianteFinal->concat([$instanciasEstudiante[$i]]);
            }
            $i++;
        }

        //Todas las instancias actuales
        $allCourseInstance = $this->courseInstanceRepository
            //->with(['course.typeCourse','semester','year'])
            ->findWhere([
                'semester_id'=>$semester,
                'year_id'=>$yearId->id
            ]);

        $i = 0;
        $courseId = $request->get('course_id');
        //Para eliminar
        while ($i < count($instanciasEstudianteFinal)){
            $j = 0;
            $eliminar = true;
            while ($j< count($courseId)){
                if ($courseId[$j] == $instanciasEstudianteFinal[$i]->courseInstance->course_id){
                    $idIns =$instanciasEstudianteFinal[$j]->courseInstance->id;
                    $eliminar = false;
                }
                $j++;
            }
            //return $this->sendResponse($eliminar, "ef");
            if ($eliminar){
                $this->studentHasCourseInstanceRepository->delete($instanciasEstudianteFinal[$i]->id);
            }
            $i++;
        }

        //Para crear
        $i = 0;
        $courseId = $request->get('course_id');
        while ($i < count($courseId)){
            $j = 0;
            $crear= true;
            while ($j< count($instanciasEstudianteFinal)){
                //return $instanciasEstudianteFinal[]->courseInstance->course_id;
                if ($courseId[$i] == $instanciasEstudianteFinal[$j]->courseInstance->course_id){

                    $crear = false;
                }
                $j++;
            }
            //return $this->sendResponse($crear, "ef");
            if ($crear){
                $k=0;
                while ($k < count($allCourseInstance)){
                    if ($allCourseInstance[$k]->course_id == $courseId[$i]){
                        $this->studentHasCourseInstanceRepository->create([
                            'student_id'=>$id,
                            'course_instance_id'=>$allCourseInstance[$k]->id
                        ]);
                    }
                    $k++;
                }
            }
            $i++;
        }



        //Buscar usuario
        $student = $this->userRepository->findWhere([
            'id'=>$id,
            'role_id'=>RolEnum::ESTUDIANTE
        ])->first();
        if (empty($student)){
            $this->sendError("Estudiante no encontrado");
        }


        //Editar Usuario
        $inputUser = [
            'name'=>$request->get('name'),
            'last_name'=>$request->get('last_name'),
            'phone'=>$request->get('phone'),
            'email'=>$request->get('email')
        ];

        $user = $this->userRepository->update($inputUser, $id);

        return $this->sendResponse($user,Messages::SUCCESS);
    }


    public function studentEdit(UpdateUserAPIRequest $request){
        $identity = IdentityFacade::instance();

        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($identity->user->id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }
        $findEmail = $this->userRepository->findWhere([
            'email'=>$request->get('email')
        ])->first();

        if (!empty($findEmail)){
            if($user->id != $findEmail->id){
                return $this->sendError(Messages::ERROR_MAIL_REGISTERED);
            }
        }
        $user = $this->userRepository->update($input, $identity->user->id);

        return $this->sendResponse($user->toArray(), 'User updated successfully');

    }
    public function studentDestroy($id){
        $identity = IdentityFacade::instance();
//        $role = $identity->getRole();
//
//        if ($role != RolEnum::ADMIN){
//            return $this->sendError(Messages::ERROR_UNAUTHORIZED);
//        }
        //buscar instanias de curso del usuario
        $studentInstance = $this->studentHasCourseInstanceRepository->findWhere([
            'student_id'=>$id
        ]);
        if (count($studentInstance) > 0){
            $i = 0;
            while ($i < count($studentInstance)){
                $this->studentHasCourseInstanceRepository->delete($studentInstance[$i]->id);
                $i++;
            }

        }
        //buscar eventos de studiante
        $studentEvent = $this->usersHasEventRepository->findWhere([
            'student_id'=>$id
        ]);
        if (count($studentEvent) > 0){
            $i =0;
            while ($i < count($studentEvent)){
                $this->usersHasEventRepository->delete($studentEvent[$i]->id);
                $i++;
            }
        }

        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendResponse($id, 'User deleted successfully');
    }
}
