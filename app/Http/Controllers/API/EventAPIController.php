<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Constants\SemesterEnum;
use App\Facades\IdentityFacade;
use App\Http\Requests\API\CreateEventAPIRequest;
use App\Http\Requests\API\UpdateEventAPIRequest;
use App\Models\Event;
use App\Repositories\EventRepository;
use App\Repositories\GlobalEventRepository;
use App\Repositories\CourseInstanceHasEventRepository;
use App\Repositories\UsersHasEventRepository;
use App\Repositories\CourseInstanceRepository;
use App\Repositories\StudentHasCourseInstanceRepository;
use App\Repositories\YearRepository;
use App\Repositories\AcademicYearRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EventController
 * @package App\Http\Controllers\API
 */

class EventAPIController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;
    /** @var  GlobalEventRepository */
    private $globalEventRepository;
    /** @var  CourseInstanceHasEventRepository */
    private $courseInstanceHasEventRepository;
    /** @var  UsersHasEventRepository */
    private $usersHasEventRepository;
    /** @var  CourseInstanceRepository */
    private $courseInstanceRepository;
    /** @var  YearRepository */
    private $yearRepository;
    /** @var  AcademicYearRepository */
    private $academicYearRepository;
    /** @var  StudentHasCourseInstanceRepository */
    private $studentHasCourseInstanceRepository;

    public function __construct(EventRepository $eventRepo,
                                GlobalEventRepository $globalEventRepo,
                                CourseInstanceHasEventRepository $courseInstanceHasEventRepo,
                                UsersHasEventRepository $usersHasEventRepo,
                                CourseInstanceRepository $courseInstanceRepo,
                                YearRepository $yearRepo,
                                AcademicYearRepository $academicYearRepo,
                                StudentHasCourseInstanceRepository $studentHasCourseInstanceRepo)
    {
        $this->eventRepository = $eventRepo;
        $this->globalEventRepository = $globalEventRepo;
        $this->courseInstanceHasEventRepository = $courseInstanceHasEventRepo;
        $this->usersHasEventRepository = $usersHasEventRepo;
        $this->courseInstanceRepository = $courseInstanceRepo;
        $this->yearRepository = $yearRepo;
        $this->academicYearRepository = $academicYearRepo;
        $this->studentHasCourseInstanceRepository = $studentHasCourseInstanceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/events",
     *      summary="Get a listing of the Events.",
     *      tags={"Event"},
     *      description="Get all Events",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Event")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->eventRepository->pushCriteria(new RequestCriteria($request));
        $this->eventRepository->pushCriteria(new LimitOffsetCriteria($request));
        $events = $this->eventRepository
            ->with([
                'eventsType.category',
                'priority',
                'courseInstanceHasEvents.courseInstance.course.typeCourse',
                'globalEvents',
                'usersHasEvents.user'])->all();

        $eventList = $this->addTypeEvent($events);


        $aux = collect();
        //Ordenar
        $i = 0;
        while ($i < count($eventList)){
            $j = 0;
            while ($j < count($eventList)-1){
                if ($eventList[$j]->created_at < $eventList[$j+1]->created_at){
                    $aux = $eventList[$j];
                    $eventList[$j] = $eventList[$j+1];
                    $eventList[$j+1] = $aux;
                }
                $j++;
            }
            $i++;
        }


        return $this->sendResponse($eventList->toArray(), 'Events retrieved successfully');
    }

    public function addTypeEvent($events){
        $i = 0;
        while ($i < count($events)){
            if (count($events[$i]->courseInstanceHasEvents) > 0) {
                $events[$i] = array_add($events[$i],'eventoPara', 'Evento de Curso');
            }elseif (count($events[$i]->globalEvents) > 0){
                $events[$i] = array_add($events[$i],'eventoPara', 'Evento Global');
            }elseif (count($events[$i]->usersHasEvents) > 0){
                $events[$i] = array_add($events[$i],'eventoPara', 'Evento de Estudiante');
            }
            $i++;
        }
        return $events;
    }

    /**
     * @param CreateEventAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/events",
     *      summary="Store a newly created Event in storage",
     *      tags={"Event"},
     *      description="Store Event",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Event that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Event")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Event"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEventAPIRequest $request)
    {
        $input = $request->all();

        $events = $this->eventRepository->create($input);

        return $this->sendResponse($events->toArray(), 'Event saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/events/{id}",
     *      summary="Display the specified Event",
     *      tags={"Event"},
     *      description="Get Event",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Event",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Event"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->with([
            'eventsType.category',
            'priority',
            'courseInstanceHasEvents.courseInstance.course.typeCourse',
            'globalEvents',
            'usersHasEvents.user'])
            ->findWithoutFail($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }
        if (count($event->courseInstanceHasEvents) > 0) {
                $event = array_add($event,'eventoPara', 'Evento de Curso');
        }elseif (count($event->globalEvents) > 0){
                $event = array_add($event,'eventoPara', 'Evento Global');
        }elseif (count($event->usersHasEvents) > 0){
                $event = array_add($event,'eventoPara', 'Evento de Estudiante');
        }

        return $this->sendResponse($event->toArray(), 'Event retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEventAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/events/{id}",
     *      summary="Update the specified Event in storage",
     *      tags={"Event"},
     *      description="Update Event",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Event",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Event that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Event")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Event"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEventAPIRequest $request)
    {
        $input = $request->all();

        /** @var Event $event */
        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event = $this->eventRepository->update($input, $id);

        return $this->sendResponse($event->toArray(), 'Event updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/events/{id}",
     *      summary="Remove the specified Event from storage",
     *      tags={"Event"},
     *      description="Delete Event",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Event",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event->delete();

        return $this->sendResponse($id, 'Event deleted successfully');
    }
    public function obtenerEventosDeEstudiante(){
        $identity = IdentityFacade::instance();
        // estudiante logeado
        $userId = $identity->user->id;

        //Calcular instancia actual
        $currentDate = Carbon::now();
        $currentYear = $currentDate->year;

        $academicYears = $this->academicYearRepository->findWhere([
            'year'=> $currentYear
        ])->first();

        //Obtener Semestre Actual
        if($currentDate >= $academicYears->start_first_semester && $currentDate < $academicYears->start_second_semester){
            $semester = SemesterEnum::PRIMER;
        }elseif ($currentDate >=  $academicYears->start_second_semester) {
            $semester = SemesterEnum::SEGUNDO;
        }

        // Obtener Todos los eventos
        $allEvent = $this->eventRepository->with([
            'eventsType.category',
            'priority',
            'courseInstanceHasEvents.courseInstance.course.typeCourse',
            'globalEvents',
            'usersHasEvents.user'])->all();
        $allEventYear = collect();

        // Obtener Solo los eventos del año actual
        $i = 0;
        while ($i < count($allEvent)){
            if ($allEvent[$i]->created_at->year == $currentYear){
                $allEventYear = $allEventYear->concat([$allEvent[$i]]);
            }
            $i++;
        }

        // Obtener Eventos del año y semestre actual
        $allEventInstance = collect();
        if ($semester==SemesterEnum::PRIMER){
            $i = 0;
            while ($i < count($allEventYear)){
                $eventDate = $allEventYear[$i]->created_at;
                if ($eventDate >= $academicYears->start_first_semester && $eventDate < $academicYears->start_second_semester){
                    $allEventInstance = $allEventInstance->concat([$allEventYear[$i]]);
                }
                $i++;
            }
        }elseif ($semester==SemesterEnum::SEGUNDO){
            $i=0;
            while ($i < count($allEventYear)){
                $eventDate = $allEventYear[$i]->created_at;
                if ($eventDate >=  $academicYears->start_second_semester){
                    $allEventInstance = $allEventInstance->concat([$allEventYear[$i]]);
                }
                $i++;
            }
        }

        //Agregar tipo de evento a todos los eventos
        $allEventWithType = $this->addTypeEvent($allEventInstance);

        $studentInstance = $this->studentHasCourseInstanceRepository->findWhere([
            'student_id'=>$userId
        ]);
        //return $studentInstance[0];

        $finallyEvent = collect();

        // Clasificar eventos
        $i = 0;
        //return $allEventWithType[4]->usersHasEvents[0];
        while ($i < count($allEventWithType)){
            // Si es evento de curso
            if ($allEventWithType[$i]->eventoPara == 'Evento de Curso'){
                $j =0;
                while ($j < count($studentInstance)){
                    //return $allEventWithType[$i]->course_instance_has_events[0]->course_instance_id;
                    if ($allEventWithType[$i]->courseInstanceHasEvents[0]->course_instance_id ==
                        $studentInstance[$j]->course_instance_id){

                        $finallyEvent = $finallyEvent->concat([$allEventWithType[$i]]);

                    }
                    $j++;
                }
            }elseif ($allEventWithType[$i]->eventoPara == 'Evento Global'){
                $finallyEvent = $finallyEvent->concat([$allEventWithType[$i]]);
            }
            elseif ($allEventWithType[$i]->eventoPara == 'Evento de Estudiante'){
                //return $allEventWithType[2]->usersHasEvents[1]->student_id;
                $k = 0;
                while ($k < count($allEventWithType[$i]->usersHasEvents)){
                    if ($allEventWithType[$i]->usersHasEvents[$k]->student_id == $userId){
                        $finallyEvent = $finallyEvent->concat([$allEventWithType[$i]]);
                    }
                    $k++;
                }
            }
            $i++;
        }


        $aux = collect();
        //Ordenar
        $i = 0;
        while ($i < count($finallyEvent)){
            $j = 0;
            while ($j < count($finallyEvent)-1){
                if ($finallyEvent[$j]->created_at < $finallyEvent[$j+1]->created_at){
                    $aux = $finallyEvent[$j];
                    $finallyEvent[$j] = $finallyEvent[$j+1];
                    $finallyEvent[$j+1] = $aux;
                }
                $j++;
            }
            $i++;
        }

        return $this->sendResponse($finallyEvent, Messages::SUCCESS);
    }

}
