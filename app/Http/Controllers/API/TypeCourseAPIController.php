<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeCourseAPIRequest;
use App\Http\Requests\API\UpdateTypeCourseAPIRequest;
use App\Models\TypeCourse;
use App\Repositories\TypeCourseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypeCourseController
 * @package App\Http\Controllers\API
 */

class TypeCourseAPIController extends AppBaseController
{
    /** @var  TypeCourseRepository */
    private $typeCourseRepository;

    public function __construct(TypeCourseRepository $typeCourseRepo)
    {
        $this->typeCourseRepository = $typeCourseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeCourses",
     *      summary="Get a listing of the TypeCourses.",
     *      tags={"TypeCourse"},
     *      description="Get all TypeCourses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TypeCourse")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->typeCourseRepository->pushCriteria(new RequestCriteria($request));
        $this->typeCourseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $typeCourses = $this->typeCourseRepository->all();

        return $this->sendResponse($typeCourses->toArray(), 'Type Courses retrieved successfully');
    }

    /**
     * @param CreateTypeCourseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/typeCourses",
     *      summary="Store a newly created TypeCourse in storage",
     *      tags={"TypeCourse"},
     *      description="Store TypeCourse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeCourse that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeCourse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeCourse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTypeCourseAPIRequest $request)
    {
        $input = $request->all();

        $typeCourses = $this->typeCourseRepository->create($input);

        return $this->sendResponse($typeCourses->toArray(), 'Type Course saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/typeCourses/{id}",
     *      summary="Display the specified TypeCourse",
     *      tags={"TypeCourse"},
     *      description="Get TypeCourse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeCourse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeCourse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TypeCourse $typeCourse */
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            return $this->sendError('Type Course not found');
        }

        return $this->sendResponse($typeCourse->toArray(), 'Type Course retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTypeCourseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/typeCourses/{id}",
     *      summary="Update the specified TypeCourse in storage",
     *      tags={"TypeCourse"},
     *      description="Update TypeCourse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeCourse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TypeCourse that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TypeCourse")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TypeCourse"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTypeCourseAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeCourse $typeCourse */
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            return $this->sendError('Type Course not found');
        }

        $typeCourse = $this->typeCourseRepository->update($input, $id);

        return $this->sendResponse($typeCourse->toArray(), 'TypeCourse updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/typeCourses/{id}",
     *      summary="Remove the specified TypeCourse from storage",
     *      tags={"TypeCourse"},
     *      description="Delete TypeCourse",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TypeCourse",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TypeCourse $typeCourse */
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            return $this->sendError('Type Course not found');
        }

        $typeCourse->delete();

        return $this->sendResponse($id, 'Type Course deleted successfully');
    }
}
