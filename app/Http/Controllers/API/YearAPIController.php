<?php

namespace App\Http\Controllers\API;

use App\Constants\Messages;
use App\Constants\RolEnum;
use App\Facades\IdentityFacade;
use App\Http\Requests\API\CreateYearAPIRequest;
use App\Http\Requests\API\UpdateYearAPIRequest;
use App\Models\Year;
use App\Repositories\YearRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use function Sodium\library_version_major;

/**
 * Class YearController
 * @package App\Http\Controllers\API
 */

class YearAPIController extends AppBaseController
{
    /** @var  YearRepository */
    private $yearRepository;

    public function __construct(YearRepository $yearRepo)
    {
        $this->yearRepository = $yearRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/years",
     *      summary="Get a listing of the Years.",
     *      tags={"Year"},
     *      description="Get all Years",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Year")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->yearRepository->pushCriteria(new RequestCriteria($request));
        $this->yearRepository->pushCriteria(new LimitOffsetCriteria($request));
        $years = $this->yearRepository->all();

        $aux = collect();
        //Ordenar
        $i = 0;
        while ($i < count($years)){
            $j = 0;
            while ($j < count($years)-1){
                if ($years[$j]->name > $years[$j+1]->name){
                    $aux = $years[$j];
                    $years[$j] = $years[$j+1];
                    $years[$j+1] = $aux;
                }
                $j++;
            }
            $i++;
        }

        return $this->sendResponse($years->toArray(), 'Years retrieved successfully');
    }

    /**
     * @param CreateYearAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/years",
     *      summary="Store a newly created Year in storage",
     *      tags={"Year"},
     *      description="Store Year",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Year that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Year")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Year"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateYearAPIRequest $request)
    {
        $identity = IdentityFacade::instance();
        $role = $identity->getRole();

        if($role != RolEnum::ADMIN){
            $this->sendError(Messages::ERROR_UNAUTHORIZED,404);
        }
        $input = $request->all();

        $years = $this->yearRepository->create($input);

        return $this->sendResponse($years->toArray(), 'Year saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/years/{id}",
     *      summary="Display the specified Year",
     *      tags={"Year"},
     *      description="Get Year",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Year",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Year"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Year $year */
        $year = $this->yearRepository->findWithoutFail($id);

        if (empty($year)) {
            return $this->sendError('Year not found');
        }

        return $this->sendResponse($year->toArray(), 'Year retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateYearAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/years/{id}",
     *      summary="Update the specified Year in storage",
     *      tags={"Year"},
     *      description="Update Year",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Year",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Year that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Year")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Year"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateYearAPIRequest $request)
    {
        $input = $request->all();

        /** @var Year $year */
        $year = $this->yearRepository->findWithoutFail($id);

        if (empty($year)) {
            return $this->sendError('Year not found');
        }

        $year = $this->yearRepository->update($input, $id);

        return $this->sendResponse($year->toArray(), 'Year updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/years/{id}",
     *      summary="Remove the specified Year from storage",
     *      tags={"Year"},
     *      description="Delete Year",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Year",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Year $year */
        $year = $this->yearRepository->findWithoutFail($id);

        if (empty($year)) {
            return $this->sendError('Year not found');
        }

        $year->delete();

        return $this->sendResponse($id, 'Year deleted successfully');
    }
    public function getYearInstance()
    {
        $year = $this->yearRepository->all();
        $aux = collect();
        //Ordenar
        $i = 0;
        while ($i < count($year)){
            $j = 0;
            while ($j < count($year)-1){
                if ($year[$j]->name > $year[$j+1]->name){
                    $aux = $year[$j];
                    $year[$j] = $year[$j+1];
                    $year[$j+1] = $aux;
                }
                $j++;
            }
            $i++;
        }

        $newYear = collect([
            'id'=>null,
            'name'=>$year[count($year)-1]->name+1
        ]);

        $year = $year->concat([$newYear]);

        return $this->sendResponse($year, Messages::SUCCESS);
    }
}
