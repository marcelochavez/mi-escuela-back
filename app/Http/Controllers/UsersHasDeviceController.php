<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUsersHasDeviceRequest;
use App\Http\Requests\UpdateUsersHasDeviceRequest;
use App\Repositories\UsersHasDeviceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UsersHasDeviceController extends AppBaseController
{
    /** @var  UsersHasDeviceRepository */
    private $usersHasDeviceRepository;

    public function __construct(UsersHasDeviceRepository $usersHasDeviceRepo)
    {
        $this->usersHasDeviceRepository = $usersHasDeviceRepo;
    }

    /**
     * Display a listing of the UsersHasDevice.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->usersHasDeviceRepository->pushCriteria(new RequestCriteria($request));
        $usersHasDevices = $this->usersHasDeviceRepository->all();

        return view('users_has_devices.index')
            ->with('usersHasDevices', $usersHasDevices);
    }

    /**
     * Show the form for creating a new UsersHasDevice.
     *
     * @return Response
     */
    public function create()
    {
        return view('users_has_devices.create');
    }

    /**
     * Store a newly created UsersHasDevice in storage.
     *
     * @param CreateUsersHasDeviceRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersHasDeviceRequest $request)
    {
        $input = $request->all();

        $usersHasDevice = $this->usersHasDeviceRepository->create($input);

        Flash::success('Users Has Device saved successfully.');

        return redirect(route('usersHasDevices.index'));
    }

    /**
     * Display the specified UsersHasDevice.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            Flash::error('Users Has Device not found');

            return redirect(route('usersHasDevices.index'));
        }

        return view('users_has_devices.show')->with('usersHasDevice', $usersHasDevice);
    }

    /**
     * Show the form for editing the specified UsersHasDevice.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            Flash::error('Users Has Device not found');

            return redirect(route('usersHasDevices.index'));
        }

        return view('users_has_devices.edit')->with('usersHasDevice', $usersHasDevice);
    }

    /**
     * Update the specified UsersHasDevice in storage.
     *
     * @param  int              $id
     * @param UpdateUsersHasDeviceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersHasDeviceRequest $request)
    {
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            Flash::error('Users Has Device not found');

            return redirect(route('usersHasDevices.index'));
        }

        $usersHasDevice = $this->usersHasDeviceRepository->update($request->all(), $id);

        Flash::success('Users Has Device updated successfully.');

        return redirect(route('usersHasDevices.index'));
    }

    /**
     * Remove the specified UsersHasDevice from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $usersHasDevice = $this->usersHasDeviceRepository->findWithoutFail($id);

        if (empty($usersHasDevice)) {
            Flash::error('Users Has Device not found');

            return redirect(route('usersHasDevices.index'));
        }

        $this->usersHasDeviceRepository->delete($id);

        Flash::success('Users Has Device deleted successfully.');

        return redirect(route('usersHasDevices.index'));
    }
}
