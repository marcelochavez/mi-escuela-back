<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleHasSystemMenuRequest;
use App\Http\Requests\UpdateRoleHasSystemMenuRequest;
use App\Repositories\RoleHasSystemMenuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoleHasSystemMenuController extends AppBaseController
{
    /** @var  RoleHasSystemMenuRepository */
    private $roleHasSystemMenuRepository;

    public function __construct(RoleHasSystemMenuRepository $roleHasSystemMenuRepo)
    {
        $this->roleHasSystemMenuRepository = $roleHasSystemMenuRepo;
    }

    /**
     * Display a listing of the RoleHasSystemMenu.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roleHasSystemMenuRepository->pushCriteria(new RequestCriteria($request));
        $roleHasSystemMenus = $this->roleHasSystemMenuRepository->all();

        return view('role_has_system_menus.index')
            ->with('roleHasSystemMenus', $roleHasSystemMenus);
    }

    /**
     * Show the form for creating a new RoleHasSystemMenu.
     *
     * @return Response
     */
    public function create()
    {
        return view('role_has_system_menus.create');
    }

    /**
     * Store a newly created RoleHasSystemMenu in storage.
     *
     * @param CreateRoleHasSystemMenuRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleHasSystemMenuRequest $request)
    {
        $input = $request->all();

        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->create($input);

        Flash::success('Role Has System Menu saved successfully.');

        return redirect(route('roleHasSystemMenus.index'));
    }

    /**
     * Display the specified RoleHasSystemMenu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            Flash::error('Role Has System Menu not found');

            return redirect(route('roleHasSystemMenus.index'));
        }

        return view('role_has_system_menus.show')->with('roleHasSystemMenu', $roleHasSystemMenu);
    }

    /**
     * Show the form for editing the specified RoleHasSystemMenu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            Flash::error('Role Has System Menu not found');

            return redirect(route('roleHasSystemMenus.index'));
        }

        return view('role_has_system_menus.edit')->with('roleHasSystemMenu', $roleHasSystemMenu);
    }

    /**
     * Update the specified RoleHasSystemMenu in storage.
     *
     * @param  int              $id
     * @param UpdateRoleHasSystemMenuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleHasSystemMenuRequest $request)
    {
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            Flash::error('Role Has System Menu not found');

            return redirect(route('roleHasSystemMenus.index'));
        }

        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->update($request->all(), $id);

        Flash::success('Role Has System Menu updated successfully.');

        return redirect(route('roleHasSystemMenus.index'));
    }

    /**
     * Remove the specified RoleHasSystemMenu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roleHasSystemMenu = $this->roleHasSystemMenuRepository->findWithoutFail($id);

        if (empty($roleHasSystemMenu)) {
            Flash::error('Role Has System Menu not found');

            return redirect(route('roleHasSystemMenus.index'));
        }

        $this->roleHasSystemMenuRepository->delete($id);

        Flash::success('Role Has System Menu deleted successfully.');

        return redirect(route('roleHasSystemMenus.index'));
    }
}
