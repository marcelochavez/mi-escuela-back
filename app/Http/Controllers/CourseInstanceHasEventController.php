<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCourseInstanceHasEventRequest;
use App\Http\Requests\UpdateCourseInstanceHasEventRequest;
use App\Repositories\CourseInstanceHasEventRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CourseInstanceHasEventController extends AppBaseController
{
    /** @var  CourseInstanceHasEventRepository */
    private $courseInstanceHasEventRepository;

    public function __construct(CourseInstanceHasEventRepository $courseInstanceHasEventRepo)
    {
        $this->courseInstanceHasEventRepository = $courseInstanceHasEventRepo;
    }

    /**
     * Display a listing of the CourseInstanceHasEvent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->courseInstanceHasEventRepository->pushCriteria(new RequestCriteria($request));
        $courseInstanceHasEvents = $this->courseInstanceHasEventRepository->all();

        return view('course_instance_has_events.index')
            ->with('courseInstanceHasEvents', $courseInstanceHasEvents);
    }

    /**
     * Show the form for creating a new CourseInstanceHasEvent.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_instance_has_events.create');
    }

    /**
     * Store a newly created CourseInstanceHasEvent in storage.
     *
     * @param CreateCourseInstanceHasEventRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseInstanceHasEventRequest $request)
    {
        $input = $request->all();

        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->create($input);

        Flash::success('Course Instance Has Event saved successfully.');

        return redirect(route('courseInstanceHasEvents.index'));
    }

    /**
     * Display the specified CourseInstanceHasEvent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->findWithoutFail($id);

        if (empty($courseInstanceHasEvent)) {
            Flash::error('Course Instance Has Event not found');

            return redirect(route('courseInstanceHasEvents.index'));
        }

        return view('course_instance_has_events.show')->with('courseInstanceHasEvent', $courseInstanceHasEvent);
    }

    /**
     * Show the form for editing the specified CourseInstanceHasEvent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->findWithoutFail($id);

        if (empty($courseInstanceHasEvent)) {
            Flash::error('Course Instance Has Event not found');

            return redirect(route('courseInstanceHasEvents.index'));
        }

        return view('course_instance_has_events.edit')->with('courseInstanceHasEvent', $courseInstanceHasEvent);
    }

    /**
     * Update the specified CourseInstanceHasEvent in storage.
     *
     * @param  int              $id
     * @param UpdateCourseInstanceHasEventRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseInstanceHasEventRequest $request)
    {
        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->findWithoutFail($id);

        if (empty($courseInstanceHasEvent)) {
            Flash::error('Course Instance Has Event not found');

            return redirect(route('courseInstanceHasEvents.index'));
        }

        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->update($request->all(), $id);

        Flash::success('Course Instance Has Event updated successfully.');

        return redirect(route('courseInstanceHasEvents.index'));
    }

    /**
     * Remove the specified CourseInstanceHasEvent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseInstanceHasEvent = $this->courseInstanceHasEventRepository->findWithoutFail($id);

        if (empty($courseInstanceHasEvent)) {
            Flash::error('Course Instance Has Event not found');

            return redirect(route('courseInstanceHasEvents.index'));
        }

        $this->courseInstanceHasEventRepository->delete($id);

        Flash::success('Course Instance Has Event deleted successfully.');

        return redirect(route('courseInstanceHasEvents.index'));
    }
}
