<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypeCourseRequest;
use App\Http\Requests\UpdateTypeCourseRequest;
use App\Repositories\TypeCourseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TypeCourseController extends AppBaseController
{
    /** @var  TypeCourseRepository */
    private $typeCourseRepository;

    public function __construct(TypeCourseRepository $typeCourseRepo)
    {
        $this->typeCourseRepository = $typeCourseRepo;
    }

    /**
     * Display a listing of the TypeCourse.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->typeCourseRepository->pushCriteria(new RequestCriteria($request));
        $typeCourses = $this->typeCourseRepository->all();

        return view('type_courses.index')
            ->with('typeCourses', $typeCourses);
    }

    /**
     * Show the form for creating a new TypeCourse.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_courses.create');
    }

    /**
     * Store a newly created TypeCourse in storage.
     *
     * @param CreateTypeCourseRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeCourseRequest $request)
    {
        $input = $request->all();

        $typeCourse = $this->typeCourseRepository->create($input);

        Flash::success('Type Course saved successfully.');

        return redirect(route('typeCourses.index'));
    }

    /**
     * Display the specified TypeCourse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            Flash::error('Type Course not found');

            return redirect(route('typeCourses.index'));
        }

        return view('type_courses.show')->with('typeCourse', $typeCourse);
    }

    /**
     * Show the form for editing the specified TypeCourse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            Flash::error('Type Course not found');

            return redirect(route('typeCourses.index'));
        }

        return view('type_courses.edit')->with('typeCourse', $typeCourse);
    }

    /**
     * Update the specified TypeCourse in storage.
     *
     * @param  int              $id
     * @param UpdateTypeCourseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeCourseRequest $request)
    {
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            Flash::error('Type Course not found');

            return redirect(route('typeCourses.index'));
        }

        $typeCourse = $this->typeCourseRepository->update($request->all(), $id);

        Flash::success('Type Course updated successfully.');

        return redirect(route('typeCourses.index'));
    }

    /**
     * Remove the specified TypeCourse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeCourse = $this->typeCourseRepository->findWithoutFail($id);

        if (empty($typeCourse)) {
            Flash::error('Type Course not found');

            return redirect(route('typeCourses.index'));
        }

        $this->typeCourseRepository->delete($id);

        Flash::success('Type Course deleted successfully.');

        return redirect(route('typeCourses.index'));
    }
}
