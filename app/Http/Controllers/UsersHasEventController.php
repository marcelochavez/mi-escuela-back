<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUsersHasEventRequest;
use App\Http\Requests\UpdateUsersHasEventRequest;
use App\Repositories\UsersHasEventRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UsersHasEventController extends AppBaseController
{
    /** @var  UsersHasEventRepository */
    private $usersHasEventRepository;

    public function __construct(UsersHasEventRepository $usersHasEventRepo)
    {
        $this->usersHasEventRepository = $usersHasEventRepo;
    }

    /**
     * Display a listing of the UsersHasEvent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->usersHasEventRepository->pushCriteria(new RequestCriteria($request));
        $usersHasEvents = $this->usersHasEventRepository->all();

        return view('users_has_events.index')
            ->with('usersHasEvents', $usersHasEvents);
    }

    /**
     * Show the form for creating a new UsersHasEvent.
     *
     * @return Response
     */
    public function create()
    {
        return view('users_has_events.create');
    }

    /**
     * Store a newly created UsersHasEvent in storage.
     *
     * @param CreateUsersHasEventRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersHasEventRequest $request)
    {
        $input = $request->all();

        $usersHasEvent = $this->usersHasEventRepository->create($input);

        Flash::success('Users Has Event saved successfully.');

        return redirect(route('usersHasEvents.index'));
    }

    /**
     * Display the specified UsersHasEvent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $usersHasEvent = $this->usersHasEventRepository->findWithoutFail($id);

        if (empty($usersHasEvent)) {
            Flash::error('Users Has Event not found');

            return redirect(route('usersHasEvents.index'));
        }

        return view('users_has_events.show')->with('usersHasEvent', $usersHasEvent);
    }

    /**
     * Show the form for editing the specified UsersHasEvent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $usersHasEvent = $this->usersHasEventRepository->findWithoutFail($id);

        if (empty($usersHasEvent)) {
            Flash::error('Users Has Event not found');

            return redirect(route('usersHasEvents.index'));
        }

        return view('users_has_events.edit')->with('usersHasEvent', $usersHasEvent);
    }

    /**
     * Update the specified UsersHasEvent in storage.
     *
     * @param  int              $id
     * @param UpdateUsersHasEventRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersHasEventRequest $request)
    {
        $usersHasEvent = $this->usersHasEventRepository->findWithoutFail($id);

        if (empty($usersHasEvent)) {
            Flash::error('Users Has Event not found');

            return redirect(route('usersHasEvents.index'));
        }

        $usersHasEvent = $this->usersHasEventRepository->update($request->all(), $id);

        Flash::success('Users Has Event updated successfully.');

        return redirect(route('usersHasEvents.index'));
    }

    /**
     * Remove the specified UsersHasEvent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $usersHasEvent = $this->usersHasEventRepository->findWithoutFail($id);

        if (empty($usersHasEvent)) {
            Flash::error('Users Has Event not found');

            return redirect(route('usersHasEvents.index'));
        }

        $this->usersHasEventRepository->delete($id);

        Flash::success('Users Has Event deleted successfully.');

        return redirect(route('usersHasEvents.index'));
    }
}
