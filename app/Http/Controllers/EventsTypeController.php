<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEventsTypeRequest;
use App\Http\Requests\UpdateEventsTypeRequest;
use App\Repositories\EventsTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class EventsTypeController extends AppBaseController
{
    /** @var  EventsTypeRepository */
    private $eventsTypeRepository;

    public function __construct(EventsTypeRepository $eventsTypeRepo)
    {
        $this->eventsTypeRepository = $eventsTypeRepo;
    }

    /**
     * Display a listing of the EventsType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->eventsTypeRepository->pushCriteria(new RequestCriteria($request));
        $eventsTypes = $this->eventsTypeRepository->all();

        return view('events_types.index')
            ->with('eventsTypes', $eventsTypes);
    }

    /**
     * Show the form for creating a new EventsType.
     *
     * @return Response
     */
    public function create()
    {
        return view('events_types.create');
    }

    /**
     * Store a newly created EventsType in storage.
     *
     * @param CreateEventsTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateEventsTypeRequest $request)
    {
        $input = $request->all();

        $eventsType = $this->eventsTypeRepository->create($input);

        Flash::success('Events Type saved successfully.');

        return redirect(route('eventsTypes.index'));
    }

    /**
     * Display the specified EventsType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            Flash::error('Events Type not found');

            return redirect(route('eventsTypes.index'));
        }

        return view('events_types.show')->with('eventsType', $eventsType);
    }

    /**
     * Show the form for editing the specified EventsType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            Flash::error('Events Type not found');

            return redirect(route('eventsTypes.index'));
        }

        return view('events_types.edit')->with('eventsType', $eventsType);
    }

    /**
     * Update the specified EventsType in storage.
     *
     * @param  int              $id
     * @param UpdateEventsTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventsTypeRequest $request)
    {
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            Flash::error('Events Type not found');

            return redirect(route('eventsTypes.index'));
        }

        $eventsType = $this->eventsTypeRepository->update($request->all(), $id);

        Flash::success('Events Type updated successfully.');

        return redirect(route('eventsTypes.index'));
    }

    /**
     * Remove the specified EventsType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $eventsType = $this->eventsTypeRepository->findWithoutFail($id);

        if (empty($eventsType)) {
            Flash::error('Events Type not found');

            return redirect(route('eventsTypes.index'));
        }

        $this->eventsTypeRepository->delete($id);

        Flash::success('Events Type deleted successfully.');

        return redirect(route('eventsTypes.index'));
    }
}
