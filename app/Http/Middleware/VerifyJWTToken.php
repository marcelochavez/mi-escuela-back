<?php

namespace App\Http\Middleware;

use App\Constants\Messages;
use App\Facades\IdentityFacade;
use App\Helpers\Utils;
use App\Http\Controllers\API\IdentityAPIController;
use App\Models\Identity;
use Closure;
//use JWTAuth;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // caching the next action
        //$response = $next($request);

        $loggedUser = null;

        try
        {
            $loggedUser = JWTAuth::parseToken()->toUser();
        }
        catch (TokenExpiredException $e)
        {

            try
            {
                $refreshedToken = JWTAuth::refresh(JWTAuth::getToken());
                $loggedUser = JWTAuth::setToken($refreshedToken)->toUser();
                header('Authorization: Bearer ' . $refreshedToken);
            }
            catch (JWTException $e)
            {
                Utils::sendError(Messages::ERROR_REFRESH_TOKEN, 400);
//                return response()->json(['error' => Messages::ERROR_REFRESH_TOKEN]);
            }

//            try
//            {
//                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
//                $response->header('Authorization', 'Bearer ' . $refreshed);
//            }
//            catch (JWTException $e)
//            {
//                return ApiHelpers::ApiResponse(103, null);
//            }
//            $user = JWTAuth::setToken($refreshed)->toUser();
        }
        catch (TokenInvalidException $e)
        {
            return Utils::sendError(Messages::ERROR_INVALID_TOKEN, 401);
        }

        catch (JWTException $e)
        {
            return Utils::sendError(Messages::ERROR_MISSING_TOKEN, 401);
        }

        $identity = app()->make(IdentityFacade::class);
        $identity->user = $loggedUser;
        $identityFacade = app()->make(IdentityFacade::class);
        $identityFacade->user = $loggedUser;
        return $next($request);


//        return $response;



    }
}
