<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CourseInstance",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="course_id",
 *          description="course_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="semester_id",
 *          description="semester_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="year_id",
 *          description="year_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class CourseInstance extends Model
{
    use SoftDeletes;

    public $table = 'course_instance';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public $fillable = [
        'course_id',
        'semester_id',
        'year_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'course_id' => 'integer',
        'semester_id' => 'integer',
        'year_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function semester()
    {
        return $this->belongsTo(\App\Models\Semester::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function year()
    {
        return $this->belongsTo(\App\Models\Year::class,'year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function courseInstanceHasEvents()
    {
        return $this->hasMany('App\Models\CourseInstanceHasEvent','course_instance_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function studentHasCourseInstances()
    {
        return $this->hasMany('App\Models\StudentHasCourseInstance', 'course_instance_id');
    }
}
