<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="SystemMenu",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="label",
 *          description="label",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="component",
 *          description="component",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="classButton",
 *          description="classButton",
 *          type="string"
 *      )
 * )
 */
class SystemMenu extends Model
{
    use SoftDeletes;

    public $table = 'system_menu';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'label',
        'component',
        'classButton'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'label' => 'string',
        'component' => 'string',
        'classButton' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function roleHasSystemMenus()
    {
        return $this->hasMany(\App\Models\RoleHasSystemMenu::class);
    }
}
