<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Repositories\UserRepository;


class UpdateUserAccess extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:accessUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activar y desactiva el acceso al usuario';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo
                                )
    {
        $this->userRepository = $userRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */



    public function handle()
    {


    }
}
