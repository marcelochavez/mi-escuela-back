<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Mail;

class MailHelper
{
    public static function sendMail($email, $subject, $body){
        if(self::isSendMailActive() ) {
            Mail::raw($body, function($message) use ($subject, $email){
                $message->to($email)->subject($subject);
            });

        }
    }

    public static function isSendMailActive(){
//        return false;
        return (config('app.mail_notification_active') == 'true');

    }


}