<?php

namespace App\Helpers;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;



class Utils
{
    public static function sendResponse($result, $message = "")
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public static function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public static function isEmptyResultData($resultData){
        return ($resultData==[] || $resultData==null || empty($resultData) || count($resultData)==0);
    }

    public static function sendResponseReport($result, $message="", $year, $size)
    {
        return response()->json([
            'success' => true,
            'data'    => $result,
            'year'    => $year,
            'size'    => $size,
            'message' => $message,
        ]);
            //Response::json(ResponseUtil::makeResponseReport($message,$result, $year, $size));
    }

    public static function sendResponseInstanceCourse($result, $message="", $size)
    {
        return response()->json([
            'success' => true,
            'data'    => $result,
            'size'    => $size,
            'message' => $message,
        ]);
        //Response::json(ResponseUtil::makeResponseReport($message,$result, $year, $size));
    }

    public static function deleteDirectoryContent($directoryPath) {
        $files = glob($directoryPath.'/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }

    public static function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen( $output_file, 'wb' );
        $data = explode( ',', $base64_string );
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        fclose( $ifp );
        return $output_file;
    }


    public static function compress($source, $destination, $quality) {
        $info = getimagesize($source);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);
        imagejpeg($image, $destination, $quality);
        return $destination;
    }

    public static function generateImage($img, $userId)
    {
        $image_parts = explode("base64,", $img);
        $image_base64 = base64_decode($image_parts[1]);
        $imagePath = storage_path("app/user_profile_images/".$userId);
        file_put_contents($imagePath, $image_base64);
        self::compress($imagePath, $imagePath, 90);
    }

//     response()->json(['error' => Messages::ERROR_REFRESH_TOKEN]);
}