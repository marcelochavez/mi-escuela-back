<?php

namespace App\Helpers;


use Illuminate\Support\Facades\DB;

class DatabaseHelper
{
    public static function executeMultipleQueries($queries){

        if(!is_array($queries))
            $queries = explode(";", $queries);
        DB::beginTransaction();
        foreach($queries as $query){

            if(preg_match('~[A-Za-z]~', $query))
                DB::update($query);
        }
        DB::commit();
    }

    public static function executeMultipleQueriesUncheckFK($queries){
        DB::update('SET FOREIGN_KEY_CHECKS=0');
        self::executeMultipleQueries($queries);
        DB::update('SET FOREIGN_KEY_CHECKS=1');
    }
}