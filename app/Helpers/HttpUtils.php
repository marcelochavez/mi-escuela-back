<?php


namespace App\Helpers;


class HttpUtils
{
    public static function makeGetReq($url)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, config('app.deviceConnectionTimeout')); // 7 seconds to timeout
        // curl_setopt($ch, CURLOPT_PORT, 81);
        //  curl_setopt($ch,CURLOPT_HEADER, false);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}