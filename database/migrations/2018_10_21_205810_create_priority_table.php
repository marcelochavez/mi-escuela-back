<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class CreatePriorityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('priority', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        $q = "
        INSERT INTO priority (id, name, created_at, updated_at, deleted_at)
        VALUES (1, 'Normal','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO priority (id, name, created_at, updated_at, deleted_at)
        VALUES (2, 'Urgente', '2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
               
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('priority');
    }
}
