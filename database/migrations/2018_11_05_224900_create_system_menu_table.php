<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class CreateSystemMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('component');
            $table->string('classButton');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });


        $q = "
        INSERT INTO system_menu (id, label,component,classButton, created_at, updated_at, deleted_at)
        VALUES (1, 'Gestionar Docencia','/gestionarDocencia','colorButtonB styleButton','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO system_menu (id, label,component,classButton, created_at, updated_at, deleted_at)
        VALUES (2, 'Gestionar Extensión','/gestionarExtension','colorButtonA styleButton','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
      
        INSERT INTO system_menu (id, label,component,classButton, created_at, updated_at, deleted_at)
        VALUES (3, 'Gestionar Estudiantes','/gestionarEstudiantes','colorButtonB styleButton','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);

        
        INSERT INTO system_menu (id, label,component,classButton, created_at, updated_at, deleted_at)
        VALUES (4, 'Gestionar Cursos','/homeCurso','colorButtonA styleButton','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO system_menu (id, label,component,classButton, created_at, updated_at, deleted_at)
        VALUES (5, 'Gestionar Eventos','/gestionarEventos', 'colorButtonB styleButton','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_menu');
    }
}
