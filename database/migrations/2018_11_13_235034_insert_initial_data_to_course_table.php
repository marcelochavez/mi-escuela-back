<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class InsertInitialDataToCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course', function (Blueprint $table) {
            $table->string('code')->unique()->change();
        });
        $q = "
        INSERT INTO `course` VALUES 
(1,'INF 1240','Introducción a la Ingeniería Informática',2,NULL,2,'2018-10-24 06:34:50','2018-10-24 06:34:50',NULL),
(2,'INF 2240','Estructura de Datos',4,NULL,2,'2018-10-24 06:35:46','2018-10-24 06:35:46',NULL),
(3,'INF 2340','Arquitectura de Hardware',3,NULL,2,'2018-10-24 06:36:07','2018-10-24 06:36:07',NULL),
(4,'INF 2241','Programación Orientada a Objetos',4,NULL,2,'2018-10-24 06:36:33','2018-10-24 06:36:33',NULL),
(5,'INF 2341','Sistemas Operativos',3,NULL,2,'2018-10-24 06:37:05','2018-10-24 06:37:05',NULL),
(6,'INF 2243','Base de Datos',3,NULL,2,'2018-10-24 06:37:26','2018-10-24 06:37:26',NULL),
(7,'INF 3242','Modelamiento de Sistemas de Software',4,NULL,2,'2018-10-24 06:38:06','2018-10-24 06:38:06',NULL),
(8,'INF 3340','Redes de Computadores',3,NULL,2,'2018-10-24 06:38:28','2018-10-24 06:38:28',NULL),
(9,'INF 3144','Investigación de Operaciones',3,NULL,2,'2018-10-24 06:38:55','2018-10-24 06:38:55',NULL),
(10,'INF 3241','Ingeniería de Software',4,NULL,2,'2018-10-24 06:39:27','2018-10-24 06:39:27',NULL),
(11,'INF 3240','Ingeniería Web',3,NULL,2,'2018-10-24 06:39:51','2018-10-24 06:39:51',NULL),
(12,'INF 3540','Taller de Base de Datos',3,NULL,2,'2018-10-24 06:40:14','2018-10-24 06:40:14',NULL),
(13,'INF 4540','Taller de Ingeniería de Software',4,NULL,2,'2018-10-24 06:40:43','2018-10-24 06:40:43',NULL),
(14,'INF 4243','Ingeniería de Requerimientos',3,NULL,2,'2018-10-24 06:41:07','2018-10-24 06:41:07',NULL),
(15,'INF 1141','Fundamentos de Algoritmos',4,NULL,2,'2018-10-24 06:41:39','2018-10-24 06:41:39',NULL),
(16,'INF 1142','Fundamentos de Programación',4,NULL,2,'2018-10-24 06:41:57','2018-10-24 06:41:57',NULL),
(17,'INF 1140','Electrónica Digital',3,NULL,2,'2018-10-24 06:42:26','2018-10-24 06:42:26',NULL),
(18,'INF 3000','Practica Profesional',8,NULL,2,'2018-10-24 06:43:26','2018-10-24 06:43:26',NULL),
(19,'INF 4541','Proyecto de título I',9,NULL,2,'2018-10-24 06:43:50','2018-11-02 21:59:38',NULL),
(20,'ICI 3241','Programación Avanzada',4,NULL,1,'2018-11-02 17:41:37','2018-11-02 17:41:37',NULL);        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course', function (Blueprint $table) {
            //
        });
    }
}
