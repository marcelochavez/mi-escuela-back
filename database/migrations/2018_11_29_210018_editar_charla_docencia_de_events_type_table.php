<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class EditarCharlaDocenciaDeEventsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events_type', function (Blueprint $table) {
            //
        });

        $q = "
        
        UPDATE `events_type`
        SET `category_id` = 2
        WHERE `id` =11;
        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events_type', function (Blueprint $table) {
            //
        });
    }
}
