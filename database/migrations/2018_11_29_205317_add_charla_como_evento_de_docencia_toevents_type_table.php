<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class AddCharlaComoEventoDeDocenciaToeventsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events_type', function (Blueprint $table) {
            //
        });
        $q = "
        INSERT INTO `events_type` VALUES 
        (11,'Charla (Docencia)',1,'2018-10-24 06:54:49','2018-10-24 06:54:49',NULL);
        
        UPDATE `events_type`
        SET `name` = 'Charla (Extensión)'
        WHERE `id` = 1;
        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events_type', function (Blueprint $table) {
            //
        });
    }
}
