<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class CreateRoleHasSystemMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_has_system_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('role');
            $table->integer('system_menu_id')->unsigned();
            $table->foreign('system_menu_id')->references('id')->on('system_menu');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        $q = "
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (1, 1, 1,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (2, 1, 2,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (3, 1, 3,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (4, 1, 4,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (5, 1, 5,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (6, 2, 5,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (7, 3, 5,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
        
        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_has_system_menu');
    }
}
