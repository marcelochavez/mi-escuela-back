<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class CreateEventsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('category');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        $q = "
        INSERT INTO `events_type` VALUES 
        (1,'Charla',1,'2018-10-24 06:54:49','2018-10-24 06:54:49',NULL),
        (2,'Seminario',1,'2018-10-24 06:54:58','2018-10-24 06:54:58',NULL),
        (3,'Expo Software',1,'2018-10-24 06:55:09','2018-10-24 06:55:09',NULL),
        (4,'Suspensión de Clases',2,'2018-10-28 16:13:51','2018-10-28 16:13:51',NULL),
        (5,'Atención de Tutoría',2,'2018-10-28 16:14:26','2018-10-28 16:14:26',NULL),
        (6,'Inicio de Clases',2,'2018-10-28 16:17:04','2018-10-28 16:17:04',NULL),
        (7,'Atención Docencia',2,'2018-10-28 16:18:23','2018-10-28 16:18:23',NULL),
        (8,'Otro',2,'2018-10-28 16:18:57','2018-10-28 16:18:57',NULL),
        (9,'Otro',1,'2018-10-28 16:19:05','2018-10-28 16:19:05',NULL),
        (10,'Horarios de Clases',2,'2018-10-28 16:19:54','2018-10-28 16:19:54',NULL);

        
        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_type');
    }
}
