<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\DatabaseHelper;

class AddSystemMenuDataToSystemMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('system_menu', function (Blueprint $table) {
            //
        });

        $q = "
        INSERT INTO system_menu (id, label,component,classButton, created_at, updated_at, deleted_at)
        VALUES (6, 'Año Académico','/gestionarAnioAcademico','colorButtonA styleButton','2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
       
       INSERT INTO role_has_system_menu (id, role_id,system_menu_id, created_at, updated_at, deleted_at)
        VALUES (8, 1, 6,'2018-07-17 15:49:57', '2018-07-17 15:49:57', null);
       
        
";
        DatabaseHelper::executeMultipleQueries($q);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_menu', function (Blueprint $table) {
            //
        });
    }
}
