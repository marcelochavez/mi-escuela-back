<?php

use App\Helpers\DatabaseHelper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(NecesaryDataSeeder::class);
        $this->call(TestDataSeeder::class);
    }

}
