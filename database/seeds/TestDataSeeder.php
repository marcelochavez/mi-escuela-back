<?php

use App\Helpers\DatabaseHelper;
use Illuminate\Database\Seeder;


class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataFileContent = Storage::disk('database_sql')->get('db_test_data_1.sql');
        DatabaseHelper::executeMultipleQueriesUncheckFK($dataFileContent);
    }
}