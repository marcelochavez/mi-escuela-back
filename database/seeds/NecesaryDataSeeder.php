<?php

use App\Helpers\DatabaseHelper;
use Illuminate\Database\Seeder;


class NecesaryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataFileContent = Storage::disk('database_sql')->get('db_necesary_data.sql');
        DatabaseHelper::executeMultipleQueriesUncheckFK($dataFileContent);
    }
}